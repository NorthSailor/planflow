#include "drawingobject.h"
#include <qmath.h>
#include <QVector3D>
#include "settings.h"
#include "drawingview.h"


// Maybe this should be replaced by a macro
bool insideRect(QPoint p, QRect r)
{
    return ((p.x() > r.left()) && (p.x() < r.right())
            && (p.y() > r.top()) && (p.y() < r.bottom()));
}

QVector3D DrawingObject::transformPoint(float x, float y, float z) const
{
    return transformPoint(QVector3D(x, y, z));
}

QVector3D DrawingObject::transformPoint(QVector3D point) const
{
    QVector3D newPoint(point.x() - _position.x(), point.y() - _position.y(), point.z() - _position.z());

    // Rotate the vertex
    float radianValue = rotation.z()/180*PI;
    float sinTheta = sin(radianValue);
    float cosTheta = cos(radianValue);
    // Unfortunately, we can't store the scaled coordinates as a variable
    // and we have to do it this inefficient way.
    // If you solve this let me know
    float newX = newPoint.x() * scaling.x() * cosTheta - newPoint.y() * scaling.y() * sinTheta;
    float newY = newPoint.x() * scaling.x() * sinTheta + newPoint.y() * scaling.y() * cosTheta;
    float newZ = newPoint.z() * scaling.z();

    // Translate the vertex back
    newPoint.setX(newX + _position.x());
    newPoint.setY(newY + _position.y());
    newPoint.setZ(newZ + _position.z());
    // Return the vertex
    return newPoint;
}

QVector3D DrawingObject::detransformPoint(float x, float y, float z) const
{
    return detransformPoint(x, y, z);
}

QVector3D DrawingObject::detransformPoint(QVector3D point) const
{
    QVector3D newPoint(point.x() - _position.x(), point.y() - _position.y(), point.z() - _position.z());

    // Rotate the vertex
    float radianValue = (-rotation.z())/180*PI;
    float sinTheta = sin(radianValue);
    float cosTheta = cos(radianValue);
    // Unfortunately, we can't store the scaled coordinates as a variable
    // and we have to do it this inefficient way.
    // If you solve this let me know
    float scalingX = 1/scaling.x();
    float scalingY = 1/scaling.y();
    float scalingZ = 1/scaling.z();
    float newX = newPoint.x() * cosTheta - newPoint.y() * sinTheta;
    float newY = newPoint.x() * sinTheta + newPoint.y() * cosTheta;
    float newZ = newPoint.z();

    // Translate the vertex back
    newPoint.setX(newX * scalingX + _position.x());
    newPoint.setY(newY * scalingY + _position.y());
    newPoint.setZ(newZ * scalingZ + _position.z());
    // Return the vertex
    return newPoint;
}

void DrawingObject::paintDistanceDimension(DrawingView* drawingView, QPainter *painter, QVector3D point1, QVector3D point2, bool invertDirection)
{
    // Calculate the distance between the two points
    float distance =
            sqrt(
                pow(point2.x() - point1.x(), 2) +
                pow(point2.y() - point1.y(), 2) +
                pow(point2.z() - point1.z(), 2));
    // Find the last point in screen coordinates
    QPoint lastScreenPoint = drawingView->camera()->physicalPoint(
                point1.x(), point1.y(), point1.z());
    QPoint currentScreenPoint =
            drawingView->camera()->physicalPoint(point2.x(), point2.y(), point2.z());
    QPen dynamicInfoPen(settings.crosshairPromptForeground());
    dynamicInfoPen.setStyle(Qt::DotLine);
    painter->setPen(dynamicInfoPen);
    int t = 30;
    int d;
    int Dx = currentScreenPoint.x() - lastScreenPoint.x();
    int Dy = currentScreenPoint.y() - lastScreenPoint.y();
    d = sqrt(pow(currentScreenPoint.x() - lastScreenPoint.x(), 2) + pow(currentScreenPoint.y() - lastScreenPoint.y(), 2));
    if (d == 0) return;
    int invertingFactor = (invertDirection ? -1 : 1);
    int dy = (Dx * t)/d * ((currentScreenPoint.x() < lastScreenPoint.x() ? 1 * invertingFactor : -1 * invertingFactor));
    int dx = (Dy * t)/d * ((currentScreenPoint.x() < lastScreenPoint.x() ? 1 * invertingFactor : -1 * invertingFactor));

    QPoint dimStart(lastScreenPoint.x() - dx, lastScreenPoint.y() + dy);
    QPoint dimEnd(dimStart.x() + Dx, dimStart.y() + Dy);
    painter->drawLine(
                dimStart, dimEnd);
    painter->drawLine(
                dimStart, lastScreenPoint);
    painter->drawLine(dimEnd, currentScreenPoint);
    painter->setPen(QColor(255, 255, 255, 200));
    painter->drawText((dimStart.x() + dimEnd.x())/2, (dimStart.y() + dimEnd.y())/2,
                      tr("%1 %2").arg(distance).arg(drawingView->unitAbbreviation()));
    painter->drawEllipse(dimStart, 2, 2);
    painter->drawEllipse(dimEnd, 2, 2);
}
