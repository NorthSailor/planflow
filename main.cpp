#include <QtGui/QApplication>
#include "mainwindow.h"
#include <QSplashScreen>
#include <QPixmap>
#include <QTimer>
#include <QThread>

class I : public QThread
{
public:
       static void sleep(unsigned long secs) {
               QThread::sleep(secs);
       }
};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("PlanFlow");
    a.setApplicationVersion("0.0.1 - Alpha");

    // Display the splash screen
    QPixmap pixmap(":/Icons/Splash.png"); // Insert your splash page image here
    QSplashScreen splash(pixmap);
    splash.show();

    splash.showMessage(QObject::tr("Starting up..."),
                    Qt::AlignLeft | Qt::AlignBottom, Qt::white);  //This line represents the alignment of text, color and position
    /*splash.showMessage(QObject::tr("(C) 2012 Flex Computer - All rights reserved"),
                       Qt::AlignRight | Qt::AlignBottom, Qt::white);*/

    a.processEvents(); //This is used to accept a click on the screen so that user can cancel the screen

    MainWindow* mainWindow = new MainWindow( &a );
    // I::sleep(20);
    splash.finish(mainWindow);
    // MainWindow w;
    mainWindow->show();
    //splash.finish(mainWindow);

    return a.exec();
}
