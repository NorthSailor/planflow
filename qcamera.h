#ifndef QCAMERA_H
#define QCAMERA_H

#include <QGLWidget>
#include <QVector3D>

class QCamera
{
public:
    QCamera() { }
    QCamera(float centerX, float centerY, float centerZ,
            float eyeX, float eyeY, float eyeZ,
            float upX, float upY, float upZ) :
        _centerX(centerX),
        _centerY(centerY),
        _centerZ(centerZ),
        _eyeX(eyeX),
        _eyeY(eyeY),
        _eyeZ(eyeZ),
        _upX(upX),
        _upY(upY),
        _upZ(upZ)
    { }

    QCamera(QVector3D center, QVector3D eye, QVector3D up) :
        _centerX(center.x()),
        _centerY(center.y()),
        _centerZ(center.z()),
        _eyeX(eye.x()),
        _eyeY(eye.y()),
        _eyeZ(eye.z()),
        _upX(up.x()),
        _upY(up.y()),
        _upZ(up.z())
    { }

    float centerX() const { return _centerX; }
    float centerY() const { return _centerY; }
    float centerZ() const { return _centerZ; }
    QVector3D center() const
    {
        QVector3D _center(_centerX, _centerY, _centerZ);
        return _center;
    }

    float eyeX() const { return _eyeX; }
    float eyeY() const { return _eyeY; }
    float eyeZ() const { return _eyeZ; }
    QVector3D eye() const
    {
        QVector3D _eye(_eyeX, _eyeY, _eyeZ);
        return _eye;
    }

    float upX() const { return _upX; }
    float upY() const { return _upY; }
    float upZ() const { return _upZ; }
    QVector3D up() const
    {
        QVector3D _up(_upX, _upY, _upZ);
        return _up;
    }

    void setCenterX(float x) { _centerX = x; }
    void setCenterY(float y) { _centerY = y; }
    void setCenterZ(float z) { _centerZ = z; }
    void setCenter(QVector3D center)
    {
        _centerX = center.x();
        _centerY = center.y();
        _centerZ = center.z();
    }

    void setEyeX(float x) { _eyeX = x; }
    void setEyeY(float y) { _eyeY = y; }
    void setEyeZ(float z) { _eyeZ = z; }
    void setEye(QVector3D eye)
    {
        _eyeX = eye.x();
        _eyeY = eye.y();
        _eyeZ = eye.z();
    }

    void setUpX(float x) { _upX = x; }
    void setUpY(float y) { _upY = y; }
    void setUpZ(float z) { _upZ = z; }
    void setUp(QVector3D up)
    {
        _upX = up.x();
        _upY = up.y();
        _upZ = up.z();
    }

    // API to be reimplemented by children classes
    virtual void resizeScene(int width, int height)
    {
        Q_UNUSED(width);
        Q_UNUSED(height);
        // Nothing for the base class
    }

    virtual void zoom(int times) { Q_UNUSED(times); }

protected:
    float _centerX, _centerY, _centerZ;
    float _eyeX, _eyeY, _eyeZ;
    float _upX, _upY, _upZ;
};

#endif // QCAMERA_H
