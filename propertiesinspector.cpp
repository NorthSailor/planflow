#define PROPS_INSPECTOR // Necessary to define the Properties Inspector classes
#include "propertiesinspector.h"
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QIcon>
#include <QToolBox>
#include <QComboBox>
#include <QColorDialog>
#include "drawingview.h"
#include "propwidgets/distance.h"
#include "propwidgets/checkproperty.h"
#include "propwidgets/button.h"
#include "propwidgets/list.h"

#include <QLabel>

HoverButton::HoverButton(QString text, QWidget *parent)
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    _label = new QLabel(text, this);
    layout->addWidget(_label);
}

void HoverButton::setText(QString text)
{
    _label->setText(text);
}

QString HoverButton::text() const
{
    return _label->text();
}

void HoverButton::enterEvent(QEvent *)
{
    emit enter();
}

void HoverButton::leaveEvent(QEvent *)
{
    emit leave();
}

PropertyWidget::PropertyWidget()
{

}

PropertyGroupWidget::PropertyGroupWidget()
{
    // Create the layouts
    _propertiesLayout = new QVBoxLayout(this);
}

void PropertyGroupWidget::addWidget(PropertyWidget *widget)
{
    // Add the widget to the layout
    _propertiesLayout->addWidget(widget);
}

void PropertyGroupWidget::clearWidgets()
{
    // Warning : this function will delete the widgets from memory
    // Get the widget layout
    if (_propertiesLayout != NULL)
    {
        QLayoutItem* item;
        while ( ( item = _propertiesLayout->takeAt( 0 ) ) != NULL )
        {
            delete item->widget();
            delete item;
        }
    }
}

void PropertyGroupWidget::endWidgets()
{
    // Just add a stretch to the layout
    _propertiesLayout->addStretch();
}

InspectorWidget::InspectorWidget(QWidget *parent)
{
    // Create the layouts
    QVBoxLayout* vBox = new QVBoxLayout(this);
    _selectionDescription = new QLabel(tr("<b>No objects selected</b>"), this);
    vBox->addWidget(_selectionDescription);

    _showDims = new HoverButton(tr("Show Dimensions"));

    // See http://doc.qt.digia.com/4.7/signalsandslots.html
    connect(_showDims, SIGNAL(enter()), SIGNAL(showDims()));
    connect(_showDims, SIGNAL(leave()), SIGNAL(hideDims()));

    vBox->addWidget(_showDims);

    /*widget->setCaption(tr("Essential"));
    vBox->addWidget(widget);
    vBox->addStretch();*/
    _toolBox = new QToolBox(this);
    vBox->addWidget(_toolBox);

    /*PropertyGroupWidget* essentialGroup = new PropertyGroupWidget();
    _toolBox->addItem(essentialGroup, QIcon(tr("")), tr("Essential"));

    essentialGroup->addWidget(new CheckProperty(tr("Inherit by Layer"), Qt::Checked));
    ListProperty* colorProperty = new ListProperty(tr("Line Color"), this);
    essentialGroup->addWidget(colorProperty);
    colorProperty->comboBox()->addItem(tr("Black/White"));
    colorProperty->comboBox()->addItem(tr("Red"));
    colorProperty->comboBox()->addItem(tr("Green"));
    colorProperty->comboBox()->addItem(tr("Blue"));
    colorProperty->comboBox()->addItem(tr("Magenta"));
    ListProperty* lineStyleProperty = new ListProperty(tr("Line Style"), this);
    essentialGroup->addWidget(lineStyleProperty);
    lineStyleProperty->comboBox()->addItem(tr("Solid"));
    lineStyleProperty->comboBox()->addItem(tr("Dash"));
    lineStyleProperty->comboBox()->addItem(tr("Dot"));
    ListProperty* hatchColorProperty = new ListProperty(tr("Hatch Color"), this);
    essentialGroup->addWidget(hatchColorProperty);
    hatchColorProperty->comboBox()->addItem(tr("Black/White"));
    hatchColorProperty->comboBox()->addItem(tr("Red"));
    hatchColorProperty->comboBox()->addItem(tr("Green"));
    hatchColorProperty->comboBox()->addItem(tr("Blue"));
    hatchColorProperty->comboBox()->addItem(tr("Magenta"));
    ListProperty* hatchProperty = new ListProperty(tr("Hatch"));
    essentialGroup->addWidget(hatchProperty);
    hatchProperty->comboBox()->addItem(tr("None"));
    hatchProperty->comboBox()->addItem(tr("Grid"));
    hatchProperty->comboBox()->addItem(tr("Horizontal Lines"));
    hatchProperty->comboBox()->addItem(tr("Vertical Lines"));
    hatchProperty->comboBox()->addItem(tr("Positive Diagonal Lines"));
    hatchProperty->comboBox()->addItem(tr("Negative Diagonal Lines"));
    hatchProperty->comboBox()->addItem(tr("Waves 1"));
    hatchProperty->comboBox()->addItem(tr("Waves 2"));
    hatchProperty->comboBox()->addItem(tr("Grass"));
    hatchProperty->comboBox()->addItem(tr("Trees"));
    essentialGroup->endWidgets();

    PropertyGroupWidget* rectangleWidget = new PropertyGroupWidget();
    _toolBox->addItem(rectangleWidget, QIcon(tr("")), tr("Rectangle"));

    rectangleWidget->addWidget(new CheckProperty(tr("Visible"), Qt::Checked));
    rectangleWidget->addWidget(new CheckProperty(tr("Locked"), Qt::Checked));
    rectangleWidget->addWidget(new DistanceProperty(tr("Rectangle Area"), 4.5f));
    rectangleWidget->addWidget(new DistanceProperty(tr("Value"), 33.2f));
    rectangleWidget->addWidget(new ButtonProperty(tr("Switch Orientation")));
    rectangleWidget->endWidgets();

    PropertyGroupWidget* geometryGroup = new PropertyGroupWidget();
    _toolBox->addItem(geometryGroup, QIcon(""), tr("Geometry"));*/
    clearProperties();
    // vBox->addStretch();
}

void InspectorWidget::addWidget(QString label, QWidget *widget, PropertyType type)
{
}

void InspectorWidget::clearProperties()
{
    int pageCount = _toolBox->count() - 1;
    for (int i = 0; i < pageCount; i++) // i is 1 so that we skip the "Essential" page
    {
        // Remove the widget
        PropertyGroupWidget* group = dynamic_cast<PropertyGroupWidget*>
                (_toolBox->widget(1));
        _toolBox->removeItem(1);
        if (group)
        {
            group->clearWidgets();
            delete group;
        }
    }
}

void InspectorWidget::setSelectionDescription(QString text)
{
    _selectionDescription->setText(text);
}

PropertiesInspector::PropertiesInspector(DrawingView *view, ObjectList list, QWidget *parent) :
    objectList(list),
    _view(view)
{
    // Create the dock widget
    _dockWidget = new QDockWidget(tr("Properties Inspector"), parent);
    _dockWidget->setAllowedAreas(Qt::BottomDockWidgetArea |
                                Qt::TopDockWidgetArea |
                                Qt::LeftDockWidgetArea |
                                Qt::RightDockWidgetArea);
    _widget = new InspectorWidget(parent);
    createEssentialProperties();
    _widget->clearProperties();
    _dockWidget->setWidget(propsWidget());
    _defaultColor = Qt::white;
    _defaultLineStyle = Continuous;
    _defaultLineWidth = 1;
    _showDimensions = false;
    // Connect the "Show Dimensions" slots
    connect(_widget, SIGNAL(showDims()), SLOT(showDims()));
    connect(_widget, SIGNAL(hideDims()), SLOT(hideDims()));
    selectionChanged();
}

void PropertiesInspector::selectionChanged()
{
    // Count the selected objects
    int selectionCount = 0;
    Q_FOREACH(DrawingObject* object, *objectList)
    {
        if (object->selected())
            selectionCount++;
    }
    // Set the selection description label
    QString selectionText = tr("");
    if (selectionCount == 0)
        selectionText = tr("<b>No objects selected</b>");
    else
    {
        selectionText = tr("<b>%n objects selected</b>", "", selectionCount);
    }
    _widget->setSelectionDescription(selectionText);
    // Update the essential properties
    // Find the color of the selection
    QColor color = defaultColor();
    bool colorVaries = false;
    LineStyle lineStyle = defaultLineStyle();
    bool lineStyleVaries = false;
    int lineWidth = defaultLineWidth();
    bool lineWidthVaries = false;
    bool firstObject = true; // True for now
    Q_FOREACH(DrawingObject* object, *objectList)
    {
        if (object->selected())
        {
            // Check if we are the first object
            if (firstObject)
            {
                firstObject = false;
                color = object->color();
                lineStyle = object->lineStyle();
                lineWidth = object->lineWidth();
            }
            else
            {
                if (object->color() != color)
                    // The color varies among the selection
                    // Display *VARIES*
                    colorVaries = true;
                if (object->lineStyle() != lineStyle)
                    lineStyleVaries = true;
                if (object->lineWidth() != lineWidth)
                    lineWidthVaries = true;
            }
        }
    }
    if (colorVaries)
    {
        lineColorList->comboBox()->addItem(tr("*VARIES*"));
        lineColorList->comboBox()->setCurrentIndex(lineColorList->comboBox()->findText(tr("*VARIES*")));
    }
    else
    {
        lineColorList->comboBox()->setCurrentIndex(lineColorList->comboBox()->findText(colorText(color)));
        lineColorList->comboBox()->removeItem(lineColorList->comboBox()->findText(tr("*VARIES*")));
    }
    if (lineStyleVaries)
    {
        lineStyleList->comboBox()->addItem(tr("*VARIES*"));
        lineStyleList->comboBox()->setCurrentIndex(lineStyleList->comboBox()->findText(tr("*VARIES*")));
    }
    else
    {
        lineStyleList->comboBox()->setCurrentIndex(lineStyleList->comboBox()->findText(lineStyleText(lineStyle)));
        lineStyleList->comboBox()->removeItem(lineStyleList->comboBox()->findText(tr("*VARIES*")));
    }
    if (lineWidthVaries)
    {
        lineWidthList->comboBox()->addItem(tr("*VARIES*"));
        lineWidthList->comboBox()->setCurrentIndex(lineWidthList->comboBox()->findText(tr("*VARIES*")));
    }
    else
    {
        lineWidthList->comboBox()->setCurrentIndex(lineWidthList->comboBox()->findText(tr("%1").arg(lineWidth)));
        lineWidthList->comboBox()->removeItem(lineWidthList->comboBox()->findText(tr("*VARIES*")));
    }
    createProperties();
}

void PropertiesInspector::createProperties()
{
    // Clear the old properties
    _widget->clearProperties();
    // Parse every selected object and let it create it's own property panel
    Q_FOREACH(DrawingObject* object, *objectList)
    {
        if (object->selected())
            object->createProperties(_widget->toolBox());
    }
}

void PropertiesInspector::createEssentialProperties()
{
    // Create the toolbox page
    PropertyGroupWidget* essentialGroup = new PropertyGroupWidget();
    _widget->toolBox()->addItem(essentialGroup, QIcon(""), tr("Essential"));
    lineColorList = new ListProperty(tr("Color"));
    essentialGroup->addWidget(lineColorList);
    // Add the basic colors
    lineColorList->addItem(tr("Black/White"));
    lineColorList->addItem(tr("Red"));
    lineColorList->addItem(tr("Yellow"));
    lineColorList->addItem(tr("Green"));
    lineColorList->addItem(tr("Blue"));
    lineColorList->addItem(tr("Magenta"));
    lineColorList->addItem(tr("Other..."));
    lineStyleList = new ListProperty(tr("Line Style"));
    essentialGroup->addWidget(lineStyleList);
    lineStyleList->addItem(tr("Solid"));
    lineStyleList->addItem(tr("Dash"));
    lineStyleList->addItem(tr("Dense Dash"));
    lineStyleList->addItem(tr("Dot"));
    lineStyleList->addItem(tr("Dash Dot Dash"));
    lineWidthList = new ListProperty(tr("Line Width"));
    essentialGroup->addWidget(lineWidthList);
    for (int i = 1; i <= 16; i++)
    {
        lineWidthList->comboBox()->addItem(tr("%1").arg(i));
    }
    essentialGroup->endWidgets();
    // Connect the slots
    connect(lineColorList->comboBox(), SIGNAL(currentIndexChanged(QString)), SLOT(colorChanged(QString)));
    connect(lineStyleList->comboBox(), SIGNAL(currentIndexChanged(QString)), SLOT(lineStyleChanged(QString)));
    connect(lineWidthList->comboBox(), SIGNAL(currentIndexChanged(int)), SLOT(lineWidthChanged(int)));
}

void PropertiesInspector::colorChanged(QString newColor)
{
    if (newColor == tr("Black/White"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(Qt::white);
        }
        _defaultColor = Qt::white;
    }
    if (newColor == tr("Red"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(Qt::red);
        }
        _defaultColor = Qt::red;
    }
    if (newColor == tr("Yellow"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(Qt::yellow);
        }
        _defaultColor = Qt::yellow;
    }
    if (newColor == tr("Green"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(Qt::green);
        }
        _defaultColor = Qt::green;
    }
    if (newColor == tr("Blue"))
    {
        QColor color(50, 200, 255);
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(color);
        }
        _defaultColor = color;
    }
    if (newColor == tr("Magenta"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(Qt::magenta);
        }
        _defaultColor = Qt::magenta;
    }
    if (newColor == tr("Other..."))
    {
        QColor newColor = QColorDialog::getColor(defaultColor());
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setColor(newColor);
            object->setSelected(false);
        }
        _defaultColor = newColor;
    }
    // Repaint the scene
    _view->repaint();
}

void PropertiesInspector::lineWidthChanged(int newWidth)
{
    int width = lineWidthList->comboBox()->itemText(newWidth).toInt();
    Q_FOREACH(DrawingObject* object, *objectList)
    {
        if (object->selected()) object->setLineWidth(width);
    }
    _defaultLineWidth = width;
    // Repaint the scene
    _view->repaint();
}

void PropertiesInspector::lineStyleChanged(QString newLineStyle)
{
    if (newLineStyle == tr("Solid"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setLineStyle(Continuous);
        }
        _defaultLineStyle = Continuous;
    }
    if (newLineStyle == tr("Dash"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setLineStyle(Dashed);
        }
        _defaultLineStyle = Dashed;
    }
    if (newLineStyle == tr("Dense Dash"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setLineStyle(DashedDense);
        }
        _defaultLineStyle = DashedDense;
    }
    if (newLineStyle == tr("Dot"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setLineStyle(Dotted);
        }
        _defaultLineStyle = Dotted;
    }
    if (newLineStyle == tr("Dash Dot Dash"))
    {
        Q_FOREACH(DrawingObject* object, *objectList)
        {
            if (object->selected()) object->setLineStyle(DashDotDash);
        }
        _defaultLineStyle = DashDotDash;
    }
    // Repaint the scene
    _view->repaint();
}

QString PropertiesInspector::colorText(QColor color)
{
    if (color == Qt::white)
        return tr("Black/White");
    if (color == Qt::red)
        return tr("Red");
    if (color == Qt::yellow)
        return tr("Yellow");
    if (color == Qt::green)
        return tr("Green");
    if (color == QColor(50, 200, 255))
        return tr("Blue");
    if (color == Qt::magenta)
        return tr("Magenta");
    else
    {
        // Form a custom string
        return tr("(%1, %1, %1)").arg(
                    color.red()).arg(color.green()).arg(color.blue());
    }
}

QString PropertiesInspector::lineStyleText(LineStyle lineStyle)
{
    if (lineStyle == Continuous)
        return tr("Solid");
    if (lineStyle == Dashed)
        return tr("Dash");
    if (lineStyle == DashedDense)
        return tr("Dense Dash");
    if (lineStyle == Dotted)
        return tr("Dot");
    if (lineStyle == DashDotDash)
        return tr("Dash Dot Dash");
    return "";
}

QString PropertiesInspector::lineWidthText(int width)
{
    return tr("%1").arg(width);
}

void PropertiesInspector::showDims()
{
    _showDimensions = true;
    _view->repaint();
}

void PropertiesInspector::hideDims()
{
    _showDimensions = false;
    _view->repaint();
}
