#include "mainwindow.h"
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    // Initialize the window
    // Set the window title
    setWindowTitle(tr("PlanFlow"));
    // Make the window 640 by 480
    resize(640, 640);
    // Create the main view
    drawingView = new DrawingView(this);
    setCentralWidget(drawingView);
}

MainWindow::MainWindow(QApplication* app, QWidget* parent) :
    QMainWindow(parent),
    parentApp(app)
{
    // Initialize the window
    // Set the window title
    setWindowTitle(tr("PlanFlow"));
    setWindowIcon(QIcon(tr(":/Icons/Logo.png")));
    // Move the window to the center of the screen
    int screenWidth = parentApp->desktop()->width();
    int screenHeight = parentApp->desktop()->height();
    move(
                distanceFromEdges,
                distanceFromEdges);
    resize(
                screenWidth - distanceFromEdges*2,
                screenHeight - distanceFromEdges*2);
    // Create the main view
    drawingView = new DrawingView(this);
    setCentralWidget(drawingView);
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    drawingView->keyPressEvent(e);
}

void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
    drawingView->keyReleaseEvent(e);
}

MainWindow::~MainWindow()
{
}
