#include "qorthocamera.h"
#include <GL/glu.h>

QOrthoCamera::QOrthoCamera()
{
    top = 5.0;
    bottom = -5.0;
    left = -5.0;
    right = 5.0;
}

void QOrthoCamera::resizeScene(int width, int height)
{
    _width = width;
    _height = height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(left, right, bottom, top, 1.0, 100000000000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(_eyeX, _eyeY, _eyeZ, _centerX, _centerY, _centerZ, _upX, _upY, _upZ);
}

void QOrthoCamera::zoom(int times)
{
    // Add times to the dimensions
    left += times;
    right -= times;
    bottom += times;
    top -= times;
}

void QOrthoCamera::setViewport2D(float _left, float _top, float _right, float _bottom)
{
    // Change the variables
    left = _left;
    top = _top;
    right = _right;
    bottom = _bottom;
}

QPoint QOrthoCamera::physicalPoint(float x, float y, float z)
{
    // Ignore the Z-parameter for now
    Q_UNUSED(z);

    // Calculate the viewport width and height in grid dimensions
    float viewWidth = right - left;
    float viewHeight = bottom - top;
    // Find the ratio of the grid and the window
    float widthRatio = (float)_width / (float)viewWidth;
    float heightRatio = (float) _height / (float)viewHeight;
    // Translate the points to match the physical coordinate system
    float newX = x - left;
    float newY = y - top; // Invert the Y ordinate
    // Find the physical dimensions
    QPoint returnPoint(  // Add 1 to match the exact opengl pixels
                (newX * widthRatio) + 1,
                (newY * heightRatio) + 1);
    return returnPoint;
}

void QOrthoCamera::logicalPoint2D(QPoint point, float &x, float &y, float &z)
{
    // Set z to 0 (Bad comment)
    z = 0;
    // Calculate the viewport width and height in grid dimensions
    float viewWidth = right - left;
    float viewHeight = bottom - top;
    // Find the ratio of the grid and the window
    float widthRatio = (float)_width / (float)viewWidth;
    float heightRatio = (float)_height / (float)viewHeight;
    float newX = point.x() / widthRatio;
    float newY = point.y() / heightRatio;
    // Find the original x and y
    x = newX + left;
    y = newY + top;
}
