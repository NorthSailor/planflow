#include "checkproperty.h"
#include <QCheckBox>
#include <QHBoxLayout>

CheckProperty::CheckProperty(QString caption,
                             int defaultState,
                             bool tristate,
                             QWidget*) :
    _caption(caption),
    _checkState(defaultState),
    _tristate(tristate),
    PropertyWidget()
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    _checkBox = new QCheckBox(_caption, this);
    _checkBox->setTristate(tristate);
    _checkBox->setCheckState((Qt::CheckState)defaultState);
    connect(_checkBox, SIGNAL(stateChanged(int)), SLOT(stateChangedSlot(int)));
    layout->addWidget(_checkBox);
}

void CheckProperty::stateChangedSlot(int state)
{
    // Emit the signals
    emit stateChanged(state);
    // Update the internal variable
    _checkState = state;
    emit stateChanged(checked());
}

void CheckProperty::setChecked(bool checked)
{
    // Change the internal variable
    _checkState = (checked ? Qt::Checked : Qt::Unchecked);
    // Update the check box
    _checkBox->setCheckState((Qt::CheckState)_checkState);
}

bool CheckProperty::checked() const
{
    return _checkState == Qt::Checked;
}

void CheckProperty::setCheckState(int state)
{
    // Update the variable
    _checkState = state;
    // Update the check box
    _checkBox->setCheckState((Qt::CheckState)state);
}

void CheckProperty::setTriState(bool tristate)
{
    // Update the variable
    _tristate = tristate;
    // Set the check box
    _checkBox->setTristate(_tristate);
}
