#ifndef DISTANCE_H
#define DISTANCE_H

#include <QObject>
#include <QWidget>
#include "../propertiesinspector.h"

class QLabel;
class QLineEdit;
class QDoubleValidator;
class QString;


class DistanceProperty : public PropertyWidget
{
    Q_OBJECT
public:
    DistanceProperty(QString caption, float value = 0);
    
    void setCaption(QString caption);
    QString caption() const;

protected:
    QLabel* _captionLabel;
    QLineEdit* _lineEdit;
    QDoubleValidator* _validator;

signals:
    void valueChanged(float newValue);

public slots:
    void textChanged(QString text);
};

#endif // DISTANCE_H
