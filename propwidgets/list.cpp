#include "list.h"

#include <QLabel>
#include <QComboBox>
#include <QHBoxLayout>

ListProperty::ListProperty(QString caption, QWidget *parent) :
    PropertyWidget()
{
    Q_UNUSED(parent);
    // Create the layout
    QHBoxLayout* layout = new QHBoxLayout(this);
    // Create the label
    _captionLabel = new QLabel(caption, this);
    layout->addWidget(_captionLabel);
    // Create the combo box
    _comboBox = new QComboBox(this);
    layout->addWidget(_comboBox);
    // Set the size policy for the combo box
    // _comboBox->setSizePolicy(QComboBox::AdjustToContents);
    // Set the insert policy for the combo box
    _comboBox->setInsertPolicy(QComboBox::InsertAtBottom);
}

void ListProperty::setCaption(QString caption)
{
    // Change the text of the label
    _captionLabel->setText(caption);
}

QString ListProperty::caption() const
{
    // Return the text of the label
    return _captionLabel->text();
}

void ListProperty::addItem(QString text)
{
    _comboBox->addItem(text);
}
