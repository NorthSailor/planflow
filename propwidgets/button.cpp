#include "button.h"
#include <QHBoxLayout>
#include <QPushButton>

ButtonProperty::ButtonProperty(QString caption, QWidget *) :
    _caption(caption),
    PropertyWidget()
{
    // For some reason we need to create a layout first
    QHBoxLayout* layout = new QHBoxLayout(this);
    // Create the button
    _button = new QPushButton(_caption, this);
    // Connect the slots
    connect(_button, SIGNAL(clicked()), SLOT(buttonClicked()));
    // Add the button to the layout
    layout->addWidget(_button);
}

void ButtonProperty::buttonClicked()
{
    emit clicked();
}

void ButtonProperty::setCaption(QString caption)
{
    _caption = caption;
    _button->setText(_caption);
}
