#ifndef CHECKPROPERTY_H
#define CHECKPROPERTY_H

class QCheckBox;
class QString;

#include <QObject>
#include <QWidget>

#include "../propertiesinspector.h"

class CheckProperty : public PropertyWidget
{
    Q_OBJECT
public:
    explicit CheckProperty(QString caption,
                           int defaultState = 0,
                           bool tristate = false,
                           QWidget * = 0);

    int checkState() const { return _checkState; }
    bool checked() const;
    bool tristate() const { return _tristate; }
    QString caption() const { return _caption; }

    void setChecked(bool checked = true);
    void setCheckState(int state);
    void setCaption(QString caption);
    void setTriState(bool tristate = true);
    
signals:
    void stateChanged(int currentState);
    void stateChanged(bool state);
    
public slots:
    void stateChangedSlot(int state);

protected:
    bool _tristate;
    QString _caption;
    int _checkState;
    QCheckBox* _checkBox;
    
};

#endif // CHECKPROPERTY_H
