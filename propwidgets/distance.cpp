#include "distance.h"
#include <QLabel>
#include <QLineEdit>
#include <QDoubleValidator>
#include "../propertiesinspector.h"
#include <QHBoxLayout>

DistanceProperty::DistanceProperty(QString caption, float value)
{
    // Create the layout
    QHBoxLayout* layout = new QHBoxLayout(this);
    _captionLabel = new QLabel(caption, this);
    layout->addWidget(_captionLabel);
    _lineEdit = new QLineEdit(this);
    layout->addWidget(_lineEdit);
    _validator = new QDoubleValidator(this);
    _validator->setNotation(QDoubleValidator::ScientificNotation);
    _lineEdit->setValidator(_validator);
    _lineEdit->setText(QString("%1").arg(value));
    connect(_lineEdit, SIGNAL(textChanged(QString)), SLOT(textChanged(QString)));
}

QString DistanceProperty::caption() const
{
    return _captionLabel->text();
}

void DistanceProperty::setCaption(QString caption)
{
    _captionLabel->setText(caption);
}

void DistanceProperty::textChanged(QString text)
{
    // Convert the text to float and emit the signal
    float val = text.toFloat();
    // No need to check for validity as we use QDoubleValidator
    // Emit the signal
    emit valueChanged(val);
}
