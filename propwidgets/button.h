#ifndef BUTTON_H
#define BUTTON_H

#include "../propertiesinspector.h"

class QPushButton;

class ButtonProperty : public PropertyWidget
{
    Q_OBJECT
public:
    explicit ButtonProperty(QString caption, QWidget * = 0);
    
    void setCaption(QString caption);

    QString caption() const { return _caption; }

signals:
    void clicked();

public slots:
    void buttonClicked();

protected:
    QPushButton* _button;
    QString _caption;
    
};

#endif // BUTTON_H
