#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include "../propertiesinspector.h"

class QLabel;
class QComboBox;

class ListProperty : public PropertyWidget
{
    Q_OBJECT
public:
    explicit ListProperty(QString caption, QWidget *parent = 0);

    QComboBox* comboBox() const { return _comboBox; }
    QString caption() const;
    void setCaption(QString caption);
    void addItem(QString text);

signals:
    
public slots:

protected:
    QComboBox* _comboBox;
    QLabel* _captionLabel;
    
};

#endif // LISTWIDGET_H
