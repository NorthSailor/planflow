#ifndef LABELPROPERTY_H
#define LABELPROPERTY_H

#include <QObject>
#include <QWidget>

class QLabel;

#include "../propertymodel.h"

class LabelProperty : public PropertyWidget
{
    Q_OBJECT
public:
    explicit LabelProperty(QString caption, QWidget *parent = 0);
    
    void setCaption(QString caption);
    QString caption() const;

protected:
    QLabel* _label;

signals:
    
public slots:
    
};

#endif // LABELPROPERTY_H
