#include "labelproperty.h"
#include <QHBoxLayout>
#include <QLabel>

LabelProperty::LabelProperty(QString caption, QWidget *parent) :
    PropertyWidget()
{
    // Create the layout
    QHBoxLayout* layout = new QHBoxLayout(this);
    // Create the label
    _label = new QLabel(caption, this);
    _label->setAlignment(Qt::AlignCenter); // All property labels should be center-aligned
    // Add the label to the layout
    layout->addWidget(_label);
}

void LabelProperty::setCaption(QString caption)
{
    _label->setText(caption);
}

QString LabelProperty::caption() const
{
    return _label->text();
}
