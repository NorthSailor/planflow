#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QAction>
#include <QMenu>
#include <QLinkedList>
#include <QDebug>
#include "drawingview.h"
const int distanceFromEdges = 100;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    explicit MainWindow(QApplication* app, QWidget *parent = 0);
    ~MainWindow();

    virtual void keyPressEvent(QKeyEvent *e);
    virtual void keyReleaseEvent(QKeyEvent *e);
    
private:
    QApplication* parentApp;

    // SDI stuff
    DrawingView* drawingView;
    void closeEvent(QCloseEvent* e) { drawingView->closeEvent(e); }
};

#endif // MAINWINDOW_H
