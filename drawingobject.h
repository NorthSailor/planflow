#ifndef DRAWINGOBJECT_H
#define DRAWINGOBJECT_H


#include <QDataStream>
#include <QTextStream>
#include <QObject>
#include <QMenu>
#include <QMessageBox>
#include <QVector3D>

#include "propertymodel.h"
#include "qframeview.h"
// The base class for every object in a drawing

// On the header type of every object it's id should be defined as a macro
#define DWG_OBJECT_BASE 0x0

enum HandleType
{
    PointHandle,
    ArrowHandle
};

// Helper function
bool insideRect(QPoint p, QRect r);

class Handle
{
public:
    HandleType type() const { return _type; }

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }

    void setX(float x) { _x = x; }
    void setY(float y) { _y = y; }
    void setZ(float z) { _z = z; }

    void move(float x, float y, float z, HandleType type)
    { _x = x; _y = y; _z = z; _type = type; }
    void move(QVector3D pos, HandleType type)
    { _x = pos.x(); _y = pos.y(); _z = pos.z(); _type = type; }

    QVector3D vertex() { return QVector3D(x(), y(), z()); }

protected:
    HandleType _type;
    float _x, _y, _z;
};

class SnapPoint
{
public:
    enum SnapPointType
    {
        Endpoint, Midpoint,
        Center, Intersection
    };

    SnapPointType type() const { return _type; }

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }

    void setX(float x) { _x = x; }
    void setY(float y) { _y = y; }
    void setZ(float z) { _z = z; }

    void move(float x, float y, float z, SnapPointType type)
    {
        _x = x; _y = y; _z = z; _type = type;
    }
    void move(QVector3D pos, SnapPointType type)
    { _x = pos.x(); _y = pos.y(); _z = pos.z(); _type = type; }

    void setType(SnapPointType type) { _type = type; }
protected:
    float _x, _y, _z;
    SnapPointType _type;
};

class LineSegment
{
public:
    LineSegment(float x1, float y1, float z1,
                float x2, float y2, float z2) :
        _x1(x1),
        _y1(y1),
        _z1(z1),
        _x2(x2),
        _y2(y2),
        _z2(z2)
    {

    }

    float x1() const { return _x1; }
    float y1() const { return _y1; }
    float z1() const { return _z1; }
    float x2() const { return _x2; }
    float y2() const { return _y2; }
    float z2() const { return _z2; }

    void setX1(float x1) { _x1 = x1; }
    void setY1(float y1) { _y1 = y1; }
    void setZ1(float z1) { _z1 = z1; }
    void setX2(float x2) { _x2 = x2; }
    void setY2(float y2) { _y2 = y2; }
    void setZ2(float z2) { _z2 = z2; }

protected:
    float _x1, _y1, _z1;
    float _x2, _y2, _z2;

};

class DrawingView;

class DrawingObject : public QObject
{
    Q_OBJECT
public:
    DrawingObject()
    {
        handleArray = new Handle[0];
        snapArray = new SnapPoint[0];
        lineArray = new LineSegment[0];
        _handleCount = 0;
        _lineCount = 0;
        _snapCount = 0;
        _selected = false;
        scaling.setX(1); scaling.setY(1); scaling.setZ(1);
    }

    virtual void render(QFrameView* view, bool highlighted)
    {
        Q_UNUSED(view);
        Q_UNUSED(highlighted);
    }

    virtual void paintOverlay(DrawingView* view, QPainter* painter)
    { Q_UNUSED(view); Q_UNUSED(painter); }

    virtual int typeId() const { return DWG_OBJECT_BASE; }
    virtual QString typeName() const { return tr("Base Object"); }

    virtual void createGeometry() { }

    // Save/Load functions
    virtual void loadFromText(QTextStream* stream)
    {
        Q_UNUSED(stream);
    }
    virtual void loadFromBinary(QDataStream* stream)
    {
        // Load the common attributes
        // Linestyle
        int linestyle = 0;
        (*stream) >> linestyle;
        setLineStyle((LineStyle)linestyle);
        // Linewidth
        (*stream) >> _lineWidth;
        // Color
        (*stream) >> _color;
        // Transformations
        (*stream) >> rotation;
        (*stream) >> scaling;
    }
    virtual void saveToText(QTextStream* stream)
    {
        Q_UNUSED(stream);
    }
    virtual void saveToBinary(QDataStream* stream)
    {
        // Save the common attributes
        (*stream) << (int)lineStyle();
        (*stream) << _lineWidth;
        (*stream) << _color;
        (*stream) << rotation;
        (*stream) << scaling;
    }

    virtual DrawingObject* cloneObject() const
    {
        DrawingObject* clone = cloneCustomObject();
        clone->setColor(_color);
        clone->setLineStyle(lineStyle());
        clone->setLineWidth(_lineWidth);
        return clone;
    }

    virtual DrawingObject* cloneCustomObject() const { return new DrawingObject(); }

    virtual void createContextMenu(QMenu* menu) { Q_UNUSED(menu); }

    QColor color() const { return _color; }
    Handle* handles(int& n) const { n = _handleCount; return handleArray; }
    SnapPoint* snapPoints(int &n) const { n = _snapCount; return snapArray; }
    LineSegment* lines(int &n) const { n = _lineCount; return lineArray; }
    bool selected() const { return _selected; }
    int lineWidth() const { return _lineWidth; }
    LineStyle lineStyle() const { return _linestyle; }
    virtual bool collides(QPoint point, QOrthoCamera* camera)
    {
        // Convert the point to logical coords
        float x, y, z;
        camera->logicalPoint2D(point, x, y, z);
        // Transform it
        QVector3D transformed = detransformPoint(QVector3D(x, y, z));
        // Convert it back to screen coordinates
        QPoint newPoint = camera->physicalPoint(transformed.x(), transformed.y(), transformed.z());
        return hitTest(newPoint, camera);
    }

    virtual bool hitTest(QPoint point, QOrthoCamera* camera) { Q_UNUSED(point); Q_UNUSED(camera); }

    virtual void handleMoved(int handle, float x, float y, float z)
    {
        Q_UNUSED(handle);
        Q_UNUSED(x);
        Q_UNUSED(y);
        Q_UNUSED(z);
    }

    virtual void move(float xStart, float yStart, float zStart, float xDest, float yDest, float zDest)
    {
        Q_UNUSED(xStart); Q_UNUSED(yStart); Q_UNUSED(zStart);
        Q_UNUSED(xDest); Q_UNUSED(yDest); Q_UNUSED(zDest);
    }

    virtual void rotate(float xRot, float yRot, float zRot, float xOrigin, float yOrigin, float zOrigin)
    {
        Q_UNUSED(xRot);  Q_UNUSED(yRot); Q_UNUSED(zRot);
        Q_UNUSED(xOrigin); Q_UNUSED(yOrigin); Q_UNUSED(zOrigin);
    }

    virtual void scale(float xScale, float yScale, float zScale, float xOrigin, float yOrigin, float zOrigin)
    {
        Q_UNUSED(xScale); Q_UNUSED(yScale); Q_UNUSED(zScale);
        Q_UNUSED(xOrigin); Q_UNUSED(yOrigin); Q_UNUSED(zOrigin);
    }

    void setColor(QColor c) { _color = c; }
    void setSelected(bool selected = true) { _selected = selected; }
    void setLineWidth(int width) { if (width) _lineWidth = width; }
    void setLineStyle(LineStyle style) { _linestyle = style; }

    void setColorByLayer(bool bylayer) { _colorByLayer = bylayer; }
    void setLineStyleByLayer(bool bylayer) { _lineStyleByLayer = bylayer; }

    bool colorByLayer() const { return _colorByLayer; }
    bool lineStyleByLayer() const { return _lineStyleByLayer; }

    virtual QRect screenRect() const { return QRect(); }

    virtual QVector3D center() const { return _position; }

    // Helper functions
    QVector3D transformPoint(QVector3D point) const;
    QVector3D transformPoint(float x = 0, float y = 0, float z = 0) const;

    QVector3D detransformPoint(QVector3D point) const;
    QVector3D detransformPoint(float x = 0, float y = 0, float z = 0) const;

    // Properties
    virtual void createProperties(QToolBox* toolBox) { Q_UNUSED(toolBox); }

    // Helper functions
    void paintDistanceDimension(DrawingView *drawingView, QPainter *painter, QVector3D point1, QVector3D point2, bool invertDirection = false);

protected:
    QColor _color;
    int _lineWidth;
    LineStyle _linestyle;
    Handle* handleArray;
    SnapPoint* snapArray;
    LineSegment* lineArray;
    int _handleCount;
    int _snapCount;
    int _lineCount;
    bool _selected;

    bool _colorByLayer;
    bool _lineStyleByLayer;
public: // They are public so that MainDesignTool can access them during the rendering
    // Tranformations
    QVector3D rotation;
    QVector3D scaling;

    QVector3D _position;

};

typedef QList<DrawingObject*>* ObjectList;

#endif // DRAWINGOBJECT_H
