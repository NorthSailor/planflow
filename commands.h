#ifndef COMMANDS_H
#define COMMANDS_H

#include "commands/erase.h"
#include "commands/move.h"
#include "commands/scale.h"
#include "commands/rotate.h"
#include "commands/new.h"
#include "commands/dwgprops.h"
#include "commands/saveas.h"
#include "commands/save.h"
#include "commands/open.h"
#include "commands/quit.h"
#include "commands/about.h"
#include "commands/grid.h"
#include "commands/osnap.h"
#include "commands/ortho.h"
#include "commands/snaptogrid.h"
#include "commands/dynamicsnap.h"
#include "commands/dynamicinfo.h"
#include "commands/line.h"
#include "commands/rect.h"
#include "commands/ellipse.h"
#include "commands/undo.h"
#include "commands/selectall.h"
#include "commands/ucs.h"

#endif // COMMANDS_H
