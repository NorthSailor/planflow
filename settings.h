#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QColor>

#define PI 3.1415926535897 // This should be precise enough

// Distance Units
// This should be enough for any
// drawing in the galaxy
enum DistanceUnit
{
    Foot = 0,       // For Everyone
    Inch = 1,       // For Engineers
    Meter = 2,      // For Everyone
    Decimeter = 3,
    Centimeter = 4,
    Millimeter = 5, // For Engineers
    Micrometer = 6,
    Nanometer = 7,
    Picometer = 8,
    Femtometer = 9,
    Attometer = 10,
    Zeptometer = 11,
    Yoctometer = 12, // For nano technologists
    Dekameter = 13,
    Hectometer = 14,
    Kilometer = 15,
    Megameter = 16,
    Gigameter = 17, // For map creators
    Terameter = 18,
    Petameter = 19,
    Exameter = 20,
    Zettameter = 21,
    Yottameter = 22,
    Mile = 23,
    NauticalMile = 24,  // For sea map designers and limenologists
    Lightyear = 25 // For astronomists
};

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);

    QColor backgroundColor() const { return _backgroundColor; }
    QColor crosshairColor() const { return _crosshairColor; }
    bool fullLengthCrosshair() const { return _fullLengthCrosshair; }
    int crossRectSize() const { return _crossRectSize; }
    int crosshairLength() const { return _crosshairLength; }
    int handleRectSize() const { return _handleRectSize; }
    int snapRectSize() const { return _snapRectSize; }
    int hitTestTolerance() const { return _hitTestTolerance; }
    QColor crosshairPromptBackground() const { return _crosshairPromptBackground; }
    QColor crosshairPromptForeground() const { return _crosshairPromptForeground; }
    bool antialiasing() const { return _antialiasing; }
    bool dynamicAntialiasing() const { return _dynamicAntialiasing; }
    bool showGrid() const { return _showGrid; }
    bool showAxes() const { return _showAxes; }
    float gridSpacing() const { return _gridSpacing; }
    bool showGridAsDots() const { return _showAsDots; }
    QColor baseGridColor() const { return _baseGridColor; }
    QColor foreGridColor() const { return _foreGridColor; }
    QColor gridDotColor() const { return _dotGridColor; }
    QColor xAxisColor() const { return _xAxisColor; }
    QColor yAxisColor() const { return _yAxisColor; }
    QColor csColor() const { return _csColor; }
    bool roundCoordinates() const { return _roundCoordinates; }
    bool alignCSIconWithTheCS() const { return _alignCSIconWithTheCS; }
    QColor handleColor() const { return _handleColor; }
    bool saveAsBinary() const { return _saveAsBinary; }
    bool showDrawingPropertiesAtStartup() const { return _showDrawingPropertiesOnStartup; }

    DistanceUnit defaultDistanceUnit() const { return _defaultDistanceUnit; }

    bool snapEnabled() const { return _snapEnabled; }
    bool snapEndpoints() const { return _snapEndpoints; }
    bool snapMidpoints() const { return _snapMidpoints; }
    bool snapCenters() const { return _snapCenters; }
    bool snapToGrid() const { return _snapToGrid; }
    bool dynamicSnap() const { return _dynamicSnap; }
    bool dynamicInfo() const { return _dynamicInfo; }
    bool orthoEnabled() const { return _orthoEnabled; }
    bool dynamicInfoDetailed() const { return _dynamicInfoDetailed; }

    void setBackgroundColor(QColor c) { _backgroundColor = c; }
    void setCrosshairColor(QColor c) { _crosshairColor = c; }
    void setFullLengthCrosshair(bool b) { _fullLengthCrosshair = b; }
    void setCrossRectSize(int n) { if (n) _crossRectSize = n; }
    void setSnapRectSize(int n) { if (n) _snapRectSize = n; }
    void setCrosshairLength(int n) { if (n) _crosshairLength = n; }
    void setHandleRectSize(int n) { if (n) _handleRectSize = n; }
    void setHitTestTolerance(int n) { if (n) _hitTestTolerance = n; }
    void setCrosshairPromptBackground(QColor c) { _crosshairPromptBackground = c; }
    void setCrosshairPromptForeground(QColor c) { _crosshairPromptForeground = c; }
    void setAntialiasing(bool antialias = true) { _antialiasing = antialias; }
    void setDynamicAntialiasing(bool dynamic = true) { _dynamicAntialiasing = dynamic; }
    void setShowGrid(bool show = true) { _showGrid = show; }
    void setShowAxes(bool show = true) { _showAxes = show; }
    void setGridSpacing(float spacing) { _gridSpacing = spacing; }
    void setShowGridAsDots(bool showAsDots = true) { _showAsDots = showAsDots; }
    void setBaseGridColor(QColor c) { _baseGridColor = c; }
    void setForeGridColor(QColor c) { _foreGridColor = c; }
    void setGridDotColor(QColor c) { _dotGridColor = c; }
    void setXAxisColor(QColor c) { _xAxisColor = c; }
    void setYAxisColor(QColor c) { _yAxisColor = c; }
    void setCSColor(QColor c) { _csColor = c; }
    void setRoundCoordinates(bool round = true) { _roundCoordinates = round; }
    void setAlignCSIconWithTheCS(bool align = true) { _alignCSIconWithTheCS = align; }
    void setHandleColor(QColor c) { _handleColor = c; }
    void setSaveAsBinary(bool binary = true) { _saveAsBinary = binary; }
    void setShowDrawingPropertiesAtStartup(bool show = true) { _showDrawingPropertiesOnStartup = show; }

    void setDefaultDistanceUnit(DistanceUnit defaultUnit) { _defaultDistanceUnit = defaultUnit; }

    void setSnapEnabled(bool enabled = true) { _snapEnabled = enabled; }
    void setSnapEndpoints(bool enabled = true) { _snapEndpoints = enabled; }
    void setSnapMidpoints(bool enabled = true) { _snapMidpoints = enabled; }
    void setSnapCenters(bool enabled = true) { _snapCenters = enabled; }
    void setSnapToGrid(bool enabled = true) { _snapToGrid = enabled; }
    void setDynamicSnap(bool enabled = true) { _dynamicSnap = enabled; }

    void setDynamicInfo(bool enabled = true) { _dynamicInfo = enabled; }

    void setOrthoEnabled(bool enabled = true) { _orthoEnabled = enabled; }
    void setDynamicInfoDetailed(bool enabled = true) { _dynamicInfoDetailed = enabled; }

protected:
    // Settings
    QColor _backgroundColor;
    QColor _crosshairColor;
    bool _fullLengthCrosshair;
    int _crosshairLength;
    int _crossRectSize;
    int _handleRectSize;
    int _snapRectSize;
    int _hitTestTolerance; // The size of the hit test rectangle
    QColor _crosshairPromptBackground;
    QColor _crosshairPromptForeground;
    bool _antialiasing; // Self-explanatory
    bool _dynamicAntialiasing; // Antialiasing off for vertical or horizontal lines
    bool _alignCSIconWithTheCS; // Align the CS Icon with the coordinate system (default is false)

    // Grid
    bool _showGrid; // The GRID flag
    bool _showAxes; // Only with PlanFlow
    float _gridSpacing; // Should be one
    bool _showAsDots; // Many people prefer the grid to be drawn as dots
    QColor _baseGridColor; // The color of the unnoticable lines
    QColor _foreGridColor; // The color of the important lines
    QColor _dotGridColor; // The color of the grid dots
    QColor _xAxisColor; // The color of the X axis (usually red)
    QColor _yAxisColor; // The color of the Y axis (usually green)
    QColor _csColor; // The color of the Coordinate system illustration (usually white)
    QColor _handleColor; // The color of the handles

    // Defaults
    DistanceUnit _defaultDistanceUnit;

    // Viewport
    bool _roundCoordinates; // If true, coordinates are rounded before displayed

    bool _saveAsBinary; // Saves the drawing using binary files, otherwise text files are used (Default)
    bool _showDrawingPropertiesOnStartup; // If true, the drawing properties dialog is shown at startup

    // Snap settings
    bool _snapEnabled;
    bool _snapEndpoints;
    bool _snapMidpoints;
    bool _snapCenters;
    bool _snapToGrid;
    bool _dynamicSnap; // Similar to AutoCAD's AutoSnap(R)(C)(TM)

    bool _dynamicInfo; // Shows dynamic info like objects selected or distance
                       // and angle from the previous point

    // Restriction settings
    bool _orthoEnabled;
    bool _dynamicInfoDetailed;
signals:
    
public slots:
    
};

// Extern
#ifndef _NO_EXTERN
extern Settings settings;
#endif

#endif // SETTINGS_H
