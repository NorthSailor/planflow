#ifndef DWGLINE_H
#define DWGLINE_H

#include "../drawingobject.h"

#define DWG_OBJECT_LINE 0x1

class DwgLine : public DrawingObject
{
    Q_OBJECT
public:
    DwgLine() { }
    DwgLine(float x1, float y1, float z1, float x2, float y2, float z2,
            QColor c) :
        _x1(x1),
        _x2(x2),
        _y1(y1),
        _y2(y2),
        _z1(z1),
        _z2(z2)
    {
        setColor(c);
        createGeometry();
        setLineStyle(Continuous);
        setLineWidth(1);
        _doubleLength = new QAction(tr("Double Length"), this);
        _halfLength = new QAction(tr("Half Length"), this);
        connect(_doubleLength, SIGNAL(triggered()), SLOT(doubleLength()));
        connect(_halfLength, SIGNAL(triggered()), SLOT(halfLength()));
        scaling.setX(1); scaling.setY(1); scaling.setZ(1);
    }

    float x1() const { return _x1; }
    float x2() const { return _x2; }
    float y1() const { return _y1; }
    float y2() const { return _y2; }
    float z1() const { return _z1; }
    float z2() const { return _z2; }

    QVector3D point1() const;
    QVector3D point2() const;

    void setX1(float x) { _x1 = x; }
    void setX2(float x) { _x2 = x; }
    void setY1(float y) { _y1 = y; }
    void setY2(float y) { _y2 = y; }
    void setZ1(float z) { _z1 = z; }
    void setZ2(float z) { _z2 = z; }

    virtual void render(QFrameView *view, bool highlighted);
    virtual void paintOverlay(DrawingView *view, QPainter *painter);
    void createHandles();
    void createSnapPoints();
    void createGeometry();
    virtual int typeId() const { return DWG_OBJECT_LINE; }
    virtual QString typeName() const { return tr("Line"); }
    virtual bool hitTest(QPoint point, QOrthoCamera* camera);
    virtual void handleMoved(int handle, float x, float y, float z);
    virtual QRect screenRect() const;
    virtual void move(float xStart, float yStart, float zStart, float xDest, float yDest, float zDest);
    virtual void scale(float xScale, float yScale, float zScale,
                       float xOrigin, float yOrigin, float zOrigin);

    void loadFromBinary(QDataStream* stream);
    void saveToBinary(QDataStream *stream);

    DrawingObject* cloneCustomObject() const { DwgLine* clone = new DwgLine(_x1, _y1, _z1, _x2, _y2, _z2, _color); return clone; }

    void createContextMenu(QMenu *menu) { menu->addAction(_doubleLength); menu->addAction(_halfLength); }

protected:
    float _x1, _x2, _y1, _y2, _z1, _z2;
    QFrameView* _view;
    QAction* _doubleLength;
    QAction* _halfLength;

public slots:
    void doubleLength();
    void halfLength();
};

#endif // DWGLINE_H
