#include "dwgellipse.h"
#include "settings.h"
#include <qmath.h>

DwgEllipse::DwgEllipse()
{

}

DwgEllipse::DwgEllipse(QVector3D center, float radius)
{
    _centerPos = center;
    _radius = 1;
    _color = Qt::white;
    _lineWidth = 1;
    _linestyle = Continuous;
    scaling.setX(radius);
    scaling.setY(radius);
    scaling.setZ(1);
    createGeometry();
}

void DwgEllipse::createGeometry()
{
    _position = centerPos();
    // Five snap points and five handles
    handleArray = new Handle;
    if (handleArray) delete handleArray;
    // Create the handle array
    handleArray = new Handle[5];
    handleArray[0].move(transformPoint(_centerPos), PointHandle);
    handleArray[1].move(transformPoint(QVector3D(_centerPos.x() - radius(),
                                                 _centerPos.y(), _centerPos.z()
                                                 )), PointHandle);
    handleArray[2].move(transformPoint(QVector3D(_centerPos.x() + radius(),
                                                 _centerPos.y(), _centerPos.z()
                                                 )), PointHandle);
    handleArray[3].move(
                transformPoint(QVector3D
                               (_centerPos.x(), _centerPos.y() - radius(),
                                _centerPos.z())), PointHandle);
    handleArray[4].move(
                transformPoint(QVector3D
                               (_centerPos.x(), _centerPos.y() + radius(),
                                _centerPos.z())), PointHandle);
    _handleCount = 5;
    // Create the snap array
    if (snapArray) delete snapArray;
    // Create the array
    snapArray = new SnapPoint[5];
    snapArray[0].move(_centerPos, SnapPoint::Center);
    snapArray[1].move(transformPoint(QVector3D(_centerPos.x() - radius(),
                                               _centerPos.y(),
                                               _centerPos.z())),
                      SnapPoint::Midpoint);
    snapArray[2].move(transformPoint(QVector3D(_centerPos.x() + radius(),
                                               _centerPos.y(),
                                               _centerPos.z())),
                      SnapPoint::Midpoint);
    snapArray[3].move(transformPoint(QVector3D(_centerPos.x(),
                                               _centerPos.y() - radius(),
                                               _centerPos.z())),
                      SnapPoint::Midpoint);
    snapArray[4].move(transformPoint(QVector3D(_centerPos.x(),
                                               _centerPos.y() + radius(),
                                               _centerPos.z())),
                      SnapPoint::Midpoint);
    _snapCount = 5;
}

void DwgEllipse::render(QFrameView *view, bool highlighted)
{
    // Save the view pointer
    _view = view;
    // Find the rectangle color and line style
    if (highlighted && (_selected == false))
    {
        view->setLineStyle(DashedDense);
        view->setLineWidth(lineWidth() * 2);
    }
    else
    {
        view->setLineStyle(((_selected == false) ? lineStyle() : DashedDense));
        view->setLineWidth(lineWidth());
    }
    QColor rendercolor = (highlighted && (_selected == false) ?
                        QColor(255, 255, 255) :
                        color());
    _view->setPolygonMode(All, Wireframe);
    _view->drawCircle(_centerPos.x(), _centerPos.y(), _radius, rendercolor);
}

void DwgEllipse::paintOverlay(DrawingView *view, QPainter *painter)
{
    // Paint the dimensions of the two axes
    // X-axis
    paintDistanceDimension(view, painter,
                           handleArray[1].vertex(), handleArray[0].vertex());
    // Y-axis
    paintDistanceDimension(view, painter,
                           handleArray[3].vertex(), handleArray[0].vertex(),
                           true);
}

bool DwgEllipse::hitTest(QPoint point, QOrthoCamera *camera)
{
    // Find the physical position of the center
    QPoint screenCenter =
            camera->physicalPoint(center().x(), center().y(), center().z());
    QPoint radiusPoint =
            camera->physicalPoint
            (center().x() + radius(), center().y(), center().z());
    int screenRadius = radiusPoint.x() - screenCenter.x();
    // Calculate the distance between the point and the screen center
    int screenDistance = sqrt(
                pow(point.x() - screenCenter.x(), 2)
                + pow(point.y() - screenCenter.y(), 2));
    // We need to reduce the pick box size four times
    // Otherwise it's oversensitive
    if (abs(screenDistance - screenRadius) < settings.crossRectSize()/4)
        return true;
    return false;
}

void DwgEllipse::move(float xStart, float yStart, float zStart, float xDest,
                      float yDest, float zDest)
{
    // Move the center
    float xDiff = xDest - xStart;
    float yDiff = yDest - yStart;
    float zDiff = zDest - zStart;

    _centerPos.setX(_centerPos.x() + xDiff);
    _centerPos.setY(_centerPos.y() + yDiff);
    _centerPos.setZ(_centerPos.z() + zDiff);

    createGeometry();
}

QRect DwgEllipse::screenRect() const
{
    QVector3D point1 = transformPoint(QVector3D(_centerPos.x() - radius(),
                                                _centerPos.y() - radius(),
                                                _centerPos.z()));
    QVector3D point4 = transformPoint(QVector3D(_centerPos.x() + radius(),
                                                _centerPos.y() + radius(),
                                                _centerPos.z()));
    QPoint screenPoint1 = _view->camera()->physicalPoint(point1.x(),
                                                         point1.y(),
                                                         point1.z());
    QPoint screenPoint2 = _view->camera()->physicalPoint(point4.x(),
                                                         point4.y(),
                                                         point4.z());
    QRect box;
    if (screenPoint1.x() < screenPoint2.x())
    {
        box.setX(screenPoint1.x());
        box.setWidth(screenPoint2.x() - screenPoint1.x());
    }
    else
    {
        box.setX(screenPoint2.x());
        box.setWidth(screenPoint1.x() - screenPoint2.x());
    }
    if (screenPoint1.y() < screenPoint2.y())
    {
        box.setY(screenPoint1.y());
        box.setHeight(screenPoint2.y() - screenPoint1.y());
    }
    else
    {
        box.setY(screenPoint2.y());
        box.setHeight(screenPoint1.y() - screenPoint2.y());
    }
    // Return the rectangle
    return box;
}

void DwgEllipse::handleMoved(int handle, float x, float y, float z)
{
    Q_UNUSED(handle);
    Q_UNUSED(x); Q_UNUSED(y); Q_UNUSED(z);
    switch (handle)
    {
    case 0:
    {
        // Center point
        _centerPos = QVector3D(x, y, z);
        createGeometry();
        break;
    }
    case 1:
    {
        // Calculate the ratio of the new and the previous radius
        QVector3D lastPos = transformPoint(QVector3D(_centerPos.x() - radius(),
                                                     _centerPos.y(),
                                                     _centerPos.z()));
        float lastDistance = sqrt(
                    pow(lastPos.x() - _centerPos.x(), 2)
                    + pow(lastPos.y() - _centerPos.y(), 2));
        float currentDistance = sqrt(
                    pow(x - _centerPos.x(), 2) + pow(y - _centerPos.y(), 2));
        float ratio = currentDistance/lastDistance;
        scaling.setX(scaling.x() * ratio);
        createGeometry();
        break;
    }
    case 2:
    {
        // Calculate the ratio of the new and the previous radius
        QVector3D lastPos = transformPoint(QVector3D(_centerPos.x() + radius(),
                                                     _centerPos.y(),
                                                     _centerPos.z()));
        float lastDistance = sqrt(
                    pow(lastPos.x() - _centerPos.x(), 2)
                    + pow(lastPos.y() - _centerPos.y(), 2));
        float currentDistance = sqrt(
                    pow(x - _centerPos.x(), 2) + pow(y - _centerPos.y(), 2));
        float ratio = currentDistance/lastDistance;
        scaling.setX(scaling.x() * ratio);
        createGeometry();
        break;
    }
    case 3:
    {
        // Calculate the ratio of the new and the previous radius
        QVector3D lastPos = transformPoint(QVector3D(_centerPos.x(),
                                                     _centerPos.y() - radius(),
                                                     _centerPos.z()));
        float lastDistance = sqrt(
                    pow(lastPos.x() - _centerPos.x(), 2)
                    + pow(lastPos.y() - _centerPos.y(), 2));
        float currentDistance = sqrt(
                    pow(x - _centerPos.x(), 2) + pow(y - _centerPos.y(), 2));
        float ratio = currentDistance/lastDistance;
        scaling.setY(scaling.y() * ratio);
        createGeometry();
        break;
    }
    case 4:
    {
        // Calculate the ratio of the new and the previous radius
        QVector3D lastPos = transformPoint(QVector3D(_centerPos.x(),
                                                     _centerPos.y() + radius(),
                                                     _centerPos.z()));
        float lastDistance = sqrt(
                    pow(lastPos.x() - _centerPos.x(), 2)
                    + pow(lastPos.y() - _centerPos.y(), 2));
        float currentDistance = sqrt(
                    pow(x - _centerPos.x(), 2) + pow(y - _centerPos.y(), 2));
        float ratio = currentDistance/lastDistance;
        scaling.setY(scaling.y() * ratio);
        createGeometry();
        break;
    }
    }
}

void DwgEllipse::createContextMenu(QMenu *menu)
{

}

void DwgEllipse::loadFromBinary(QDataStream *stream)
{
    DrawingObject::loadFromBinary(stream);
    // Center and radius
    (*stream) >> _centerPos;
    (*stream) >> _radius;
}

void DwgEllipse::saveToBinary(QDataStream *stream)
{
    DrawingObject::saveToBinary(stream);
    // Center and radius
    (*stream) << _centerPos;
    (*stream) << _radius;
}
