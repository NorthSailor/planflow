#ifndef DWGELLIPSE_H
#define DWGELLIPSE_H

#include "../drawingobject.h"

#define DWG_OBJECT_ELLIPSE 0x3

class DwgEllipse : public DrawingObject
{
    Q_OBJECT
public:
    DwgEllipse();
    DwgEllipse(QVector3D center, float radius);

    float radius() const { return _radius; }
    QVector3D centerPos() const { return _centerPos; }

    void setRadius(float radius) { _radius = radius; }
    void setCenterPos(QVector3D center) { _centerPos = center; }

    int typeId() const { return DWG_OBJECT_ELLIPSE; }
    QString typeName() const { return tr("Ellipse"); }

    void render(QFrameView *view, bool highlighted);
    void paintOverlay(DrawingView *view, QPainter *painter);
    virtual bool hitTest(QPoint point, QOrthoCamera* camera);
    virtual void move(float xStart, float yStart, float zStart, float xDest, float yDest, float zDest);

    virtual QRect screenRect() const;

    void handleMoved(int handle, float x, float y, float z);

    DrawingObject* cloneCustomObject() const { return new DwgEllipse(center(), radius()); }

    void createContextMenu(QMenu *menu);

    void loadFromBinary(QDataStream *stream);
    void saveToBinary(QDataStream *stream);

    void createGeometry();

protected:
    QVector3D _centerPos;
    float _radius;
    QFrameView* _view;
    
signals:
    
public slots:
    
};

#endif // DWGELLIPSE_H
