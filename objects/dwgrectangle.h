#ifndef DWGRECTANGLE_H
#define DWGRECTANGLE_H

#include "../drawingobject.h"

#define DWG_OBJECT_RECTANGLE 0x2

class DwgRectangle : public DrawingObject
{
    Q_OBJECT
public:
    DwgRectangle() { }
    DwgRectangle(
            float x1, float y1, float x2,
            float y2, float z, QColor c);
    
    float x1() const { return _x1; }
    float x2() const { return _x2; }
    float y1() const { return _y1; }
    float y2() const { return _y2; }
    float z() const { return _z; }

    void setX1(float x1) { _x1 = x1; }
    void setX2(float x2) { _x2 = x2; }
    void setY1(float y1) { _y1 = y1; }
    void setY2(float y2) { _y2 = y2; }
    void setZ(float z) { _z = z; }

    int typeId() const { return DWG_OBJECT_RECTANGLE; }
    QString typeName() const { return tr("Rectangle"); }

    void render(QFrameView *view, bool highlighted);
    virtual void paintOverlay(DrawingView *view, QPainter *painter);
    virtual bool hitTest(QPoint point, QOrthoCamera* camera);
    virtual void move(float xStart, float yStart, float zStart, float xDest, float yDest, float zDest);

    virtual QRect screenRect() const;

    void handleMoved(int handle, float x, float y, float z);

    DrawingObject* cloneCustomObject() const { return new DwgRectangle(x1(), y1(), x2(), y2(), z(), color()); }

    void createContextMenu(QMenu *menu);

    void createProperties(QToolBox *toolBox);

    void loadFromBinary(QDataStream *stream);
    void saveToBinary(QDataStream *stream);

protected:
    float _x1, _y1;
    float _x2, _y2, _z;
    QFrameView* _view;
    bool _topBorder;
    bool _bottomBorder;
    bool _leftBorder;
    bool _rightBorder;

public:
    void createGeometry();
protected:
    QAction* rotate90Act;

signals:
    
public slots:
    void rotate90();
    void setTopBorder(bool visible) { _topBorder = visible; _view->repaint(); }
    void setBottomBorder(bool visible) { _bottomBorder = visible; _view->repaint(); }
    void setLeftBorder(bool visible) { _leftBorder = visible; _view->repaint(); }
    void setRightBorder(bool visible) { _rightBorder = visible; _view->repaint(); }
    
};

#endif // DWGRECTANGLE_H
