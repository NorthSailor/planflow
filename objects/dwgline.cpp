#include "dwgline.h"
#include <QLine>
#include "settings.h"
#include <qmath.h>

QVector3D DwgLine::point1() const
{
    return transformPoint(_x1, _y1, _z1);
}

QVector3D DwgLine::point2() const
{
    return transformPoint(_x2, _y2, _z2);
}

void DwgLine::render(QFrameView *view, bool highlighted)
{
    // Store the pointer for later use
    _view = view;
    if (highlighted && (_selected == false))
    {
        view->setLineStyle(DashedDense);
        view->setLineWidth(lineWidth() * 2);
    }
    else
    {
        view->setLineStyle(((_selected == false) ? lineStyle() : DashedDense));
        view->setLineWidth(lineWidth());
    }
    view->drawLine(_x1, _y1, _z1, _x2, _y2, _z2,
                   (highlighted && (_selected == false) ?
                                  QColor(255, 255, 255) :
                                  color()));
}

void DwgLine::paintOverlay(DrawingView *view, QPainter *painter)
{
    // Paint the line dimension
    paintDistanceDimension(view, painter, point1(), point2(), false);
}

void DwgLine::createGeometry()
{
    createHandles();
    createSnapPoints();

    // Create the line array
    lineArray = new LineSegment(_x1, _y1, _z1, _x2, _y2, _z2);

    _position.setX((_x1 + _x2)/2);
    _position.setY((_y1 + _y2)/2);
    _position.setZ((_z1 + _z2)/2);
}

void DwgLine::createHandles()
{
    // If there were previous arrays
    // delete them to avoid memory leaks
    if (handleArray) delete handleArray;
    // Create the handle array
    handleArray = new Handle[3];
    // 3 Handles
    // 2 for the endpoints
    // and 1 for the mid point
    QVector3D firstPoint = point1();
    QVector3D secondPoint = point2();
    handleArray[0].move(firstPoint, PointHandle);
    handleArray[1].move(secondPoint, PointHandle);
    handleArray[2].move((firstPoint.x() + secondPoint.x())/2,
                        (firstPoint.y() + secondPoint.y())/2,
                        (firstPoint.z() + secondPoint.z())/2, PointHandle);
    _handleCount = 3;
}

void DwgLine::createSnapPoints()
{
    // Three snap points
    // Two for the endings and one midpoint
    if (snapArray) delete snapArray;
    // Create the handle array
    QVector3D firstPoint = point1();
    QVector3D secondPoint = point2();
    snapArray = new SnapPoint[3];
    snapArray[0].move(firstPoint, SnapPoint::Endpoint);
    snapArray[1].move(secondPoint, SnapPoint::Endpoint);
    snapArray[2].move((firstPoint.x() + secondPoint.x())/2,
                      (firstPoint.y() + secondPoint.y())/2,
                      (firstPoint.z() + secondPoint.z())/2, SnapPoint::Midpoint);
    _snapCount = 3;
}

bool DwgLine::hitTest(QPoint point, QOrthoCamera* camera)
{
    // Convert the line points to pixel coordinates
    QPoint point1 = camera->physicalPoint(x1(), y1(), z1());
    QPoint point2 = camera->physicalPoint(x2(), y2(), z2());
    // Find the line angle
    QLineF line = QLineF(QPointF(point1), QPointF(point2));
    QLineF pickLineH = QLineF(point.x(), point.y() - settings.crossRectSize(),
                    point.x(), point.y() + settings.crossRectSize());
    QLineF pickLineV = QLineF(point.x() - settings.crossRectSize(),
                              point.y(), point.x() + settings.crossRectSize(),
                              point.y());
    QPointF intersectionPoint;
    if (line.intersect(pickLineH, &intersectionPoint)
            == QLineF::BoundedIntersection)
        return true;
    if (line.intersect(pickLineV, &intersectionPoint)
            == QLineF::BoundedIntersection)
        return true;
    return false;
}

void DwgLine::handleMoved(int handle, float x, float y, float z)
{
    QVector3D untransformed = detransformPoint(QVector3D(x, y, z));
    if (handle == 0)
    {
        // Point 1 moved
        // Move point 1 to it's new position
        setX1(untransformed.x());
        setY1(untransformed.y());
        setZ1(untransformed.z());
    }
    if (handle == 1)
    {
        // Point 2 moved
        // Move point 2 to it's new position
        setX2(untransformed.x());
        setY2(untransformed.y());
        setZ2(untransformed.z());
    }
    if (handle == 2)
    {
        // Midpoint moved
        // Find the current midpoint
        float x0 = (x1() + x2())/2;
        float y0 = (y1() + y2())/2;
        float z0 = (z1() + z2())/2;
        // Find the differences
        float dx = untransformed.x() - x0;
        float dy = untransformed.y() - y0;
        float dz = untransformed.z() - z0;
        // Move the line endpoints
        setX1(x1() + dx);
        setX2(x2() + dx);
        setY1(y1() + dy);
        setY2(y2() + dy);
        setZ1(z1() + dz);
        setZ2(z2() + dz);
    }
    // Recalculate the handles
    createGeometry();
}

QRect DwgLine::screenRect() const
{
    // Find the screen coordinates of the points
    QPoint point1 = _view->camera()->physicalPoint(x1(), y1(), z1());
    QPoint point2 = _view->camera()->physicalPoint(x2(), y2(), z2());
    // Create the rectangle
    QRect box;
    if (point1.x() < point2.x())
    {
        box.setX(point1.x());
        box.setWidth(point2.x() - point1.x());
    }
    else
    {
        box.setX(point2.x());
        box.setWidth(point1.x() - point2.x());
    }
    if (point1.y() < point2.y())
    {
        box.setY(point1.y());
        box.setHeight(point2.y() - point1.y());
    }
    else
    {
        box.setY(point2.y());
        box.setHeight(point1.y() - point2.y());
    }
    // Return the rectangle
    return box;
}

// Transformations

void DwgLine::move(float xStart, float yStart, float zStart, float xDest, float yDest, float zDest)
{
    // Find the translation
    float xTrans = xDest - xStart;
    float yTrans = yDest - yStart;
    float zTrans = zDest - zStart;

    // Add the translation to the line points
    setX1(x1() + xTrans);
    setY1(y1() + yTrans);
    setZ1(z1() + zTrans);
    setX2(x2() + xTrans);
    setY2(y2() + yTrans);
    setZ2(z2() + zTrans);

    // Recreate the handles
    createGeometry();
}

void DwgLine::scale(float xScale, float yScale, float zScale,
                    float xOrigin, float yOrigin, float zOrigin)
{
    // Translate the object to the origin
    float newX1 = x1() - xOrigin;
    float newX2 = x2() - xOrigin;
    float newY1 = y1() - yOrigin;
    float newY2 = y2() - yOrigin;
    float newZ1 = z1() - zOrigin;
    float newZ2 = z2() - zOrigin;
    // Multiply the vertices with the scale factors
    float xDiff = xScale - xOrigin;
    float yDiff = yScale - yOrigin;
    float zDiff = zScale - zOrigin;
    newX1 *= xDiff;
    newX2 *= xDiff;
    newY1 *= yDiff;
    newY2 *= yDiff;
    newZ1 *= zDiff;
    newZ2 *= zDiff;
    // Translate the object back
    setX1(newX1 + xOrigin);
    setX2(newX2 + xOrigin);
    setY1(newY1 + yOrigin);
    setY2(newY2 + yOrigin);
    setZ1(newZ1 + zOrigin);
    setZ2(newZ2 + zOrigin);
    createGeometry();
}

void DwgLine::loadFromBinary(QDataStream *stream)
{
    // Load the object data
    DrawingObject::loadFromBinary(stream);
    (*stream) >> _x1;
    (*stream) >> _y1;
    (*stream) >> _z1;
    (*stream) >> _x2;
    (*stream) >> _y2;
    (*stream) >> _z2;
    createGeometry();
}

void DwgLine::saveToBinary(QDataStream *stream)
{
    // Save the object data
    DrawingObject::saveToBinary(stream);
    // Print the vertices
    (*stream) << _x1;
    (*stream) << _y1;
    (*stream) << _z1;
    (*stream) << _x2;
    (*stream) << _y2;
    (*stream) << _z2;
}

void DwgLine::halfLength()
{
    float shrinkLengthX = (x2() - x1())/4;
    float shrinkLengthY = (y2() - y1())/4;
    float shrinkLengthZ = (z2() - z1())/4;
    _x1 += shrinkLengthX;
    _x2 -= shrinkLengthX;
    _y1 += shrinkLengthY;
    _y2 -= shrinkLengthY;
    _z1 += shrinkLengthZ;
    _z2 -= shrinkLengthZ;
    createGeometry();
}

void DwgLine::doubleLength()
{
    float extensionLengthX = (x2() - x1())/2;
    float extensionLengthY = (y2() - y1())/2;
    float extensionLengthZ = (z2() - z1())/2;
    _x1 -= extensionLengthX;
    _x2 += extensionLengthX;
    _y1 -= extensionLengthY;
    _y2 += extensionLengthY;
    _z1 -= extensionLengthZ;
    _z2 += extensionLengthZ;
    createGeometry();
}
