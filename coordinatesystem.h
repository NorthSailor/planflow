#ifndef COORDINATESYTEM_H
#define COORDINATESYTEM_H

#include <QVector3D>
#include <Qt>
#include "qframeview.h"

class CoordinateSystem
{
public:
    CoordinateSystem();

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }
    float rotation() const { return _rotation; }

    void setX(float x) { _x = x; }
    void setY(float y) { _y = y; }
    void setZ(float z) { _z = z; }
    void setRotation(float rotation) { _rotation = rotation; }

    void render(QOrthoCamera &camera, QPainter &painter);

protected:
    float _x;
    float _y;
    float _z;
    float _rotation; // The rotation in degrees
};

#endif // COORDINATESYTEM_H
