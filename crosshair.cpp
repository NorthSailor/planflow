#include "crosshair.h"
#include "settings.h"

Crosshair::Crosshair(QObject *parent) :
    QObject(parent),
    _x(0),
    _y(0),
    _prompt(""),
    _state(MainSelect),
    _showPrompt(false)
{
}

void Crosshair::paintAt(QPainter &painter)
{
    // Disable Antialiasing
    painter.setRenderHint(QPainter::Antialiasing, false);
    // Create the pen
    QPen pen(settings.crosshairColor());
    painter.setPen(pen);
    // Check the state
    switch (_state)
    {
    case Crosshair::MainSelect:
        // Draw a classic crosshair
        // Draw the crosshair correctly
        if (settings.fullLengthCrosshair())
        {
            painter.drawLine(0, y(), painter.window().width(), y());
            painter.drawLine(x(), 0, x(), painter.window().height());
        }
        else
        {
            painter.drawLine(
                        x() - settings.crosshairLength()/2,
                        y(),
                        x() + settings.crosshairLength()/2,
                        y());
            painter.drawLine(
                        x(),
                        y() - settings.crosshairLength()/2,
                        x(),
                        y() + settings.crosshairLength()/2);
        }
        // Draw the crosshair square
        painter.drawRect(
                    x() - settings.crossRectSize()/2,
                    y() - settings.crossRectSize()/2,
                    settings.crossRectSize() - 1,
                    settings.crossRectSize() - 1);
        // Plot the point missing from the top-left corner
        painter.drawPoint(x() - settings.crossRectSize()/2,
                          y() - settings.crossRectSize()/2);
        break;
    case Crosshair::PickPoint:
        // Draw only the cross
        if (settings.fullLengthCrosshair())
        {
            painter.drawLine(0, y(), painter.window().width(), y());
            painter.drawLine(x(), 0, x(), painter.window().height());
        }
        else
        {
            painter.drawLine(
                        x() - settings.crosshairLength()/2,
                        y(),
                        x() + settings.crosshairLength()/2,
                        y());
            painter.drawLine(
                        x(),
                        y() - settings.crosshairLength()/2,
                        x(),
                        y() + settings.crosshairLength()/2);
        }

        break;
    case Crosshair::SelectObject:
        // Draw only the square
        painter.drawRect(
                    x() - settings.crossRectSize()/2,
                    y() - settings.crossRectSize()/2,
                    settings.crossRectSize() - 1,
                    settings.crossRectSize() - 1);
        // Plot the point missing from the top-left corner
        painter.drawPoint(x() - settings.crossRectSize()/2,
                          y() - settings.crossRectSize()/2);
        break;
    case Crosshair::Hidden:
        break; // Obviously, there's nothing to do here
    }

    // If we have to, draw the prompt
    if (showPrompt() && settings.dynamicInfo())
    {
        // Enable text antialiasing
        painter.setRenderHint(QPainter::TextAntialiasing);

        int textWidth = painter.fontMetrics().width(prompt());
        int textHeight = painter.fontMetrics().height()*(prompt().count("\n") + 1);
        QRect promptRect(
                    x() + settings.crossRectSize() + 5,
                    y() + settings.crossRectSize() + 5,
                    textWidth + 4,
                    textHeight + 4);
        // Draw the rectangle
        painter.fillRect( promptRect, settings.crosshairPromptBackground() );
        QPen borderPen(settings.crosshairPromptForeground());
        painter.setPen(borderPen);
        painter.drawRect( promptRect );
        painter.drawText(
                    promptRect.x() + 2,
                    promptRect.y() + 2,
                    textWidth, textHeight,
                    0, prompt());
    }
    // Re-enable Antialiasing
    painter.setRenderHint(QPainter::Antialiasing);
}
