#ifndef QORTHOCAMERA_H
#define QORTHOCAMERA_H

#include "qcamera.h"

class QOrthoCamera : public QCamera
{
public:
    QOrthoCamera();

public:
    void resizeScene(int width, int height);
    void zoom(int times);
    void setViewport2D(float _left, float _top, float _right, float _bottom);
    QPoint physicalPoint(float x, float y, float z);
    void logicalPoint2D(QPoint point, float& x, float& y, float& z);

    float top, left, right, bottom;
    int _width, _height;
};

#endif // QORTHOCAMERA_H
