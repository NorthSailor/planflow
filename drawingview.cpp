#include "drawingview.h"
#include <QPainter>
#include "mainwindow.h"
#include "objects/dwgline.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QDataStream>
#include <QProgressDialog>
#include "drawingobjects.h"
#include "drawingpropertiesdlg.h"
#include <qmath.h>
#include <QPluginLoader>
#include "plugincontroller.h"
#include <QMenuBar>

DrawingView::DrawingView(QMainWindow *mainWindow, QWidget *parent)
{
    Q_UNUSED(parent);
    mainWin = mainWindow;
    // Remove the mouse cursor as we're using the cross-hair
    setCursor(Qt::BlankCursor);
    // Enable mouse tracking so that we receive mouse move messages when
    // no mouse button is pressed
    setMouseTracking(true);
    // Initialize the viewport
    camera()->setViewport2D(-5, 50, 90, -5);
    camera()->zoom(1);
    // Initialize the crosshair
    crosshair.setState(Crosshair::MainSelect);
    // Initialize the status bar
    statusBar = new StatusBar(mainWindow);
    // Initialize the crosshair
    crosshair.setState(Crosshair::MainSelect);
    // Initialize the Coordinate System
    CS.setX(0);
    CS.setY(0);
    CS.setZ(0);
    CS.setRotation(30/180*PI);
    // Create the first layer
    // Create the command
    commandWindow = new CommandWindow(mainWindow);
    propertiesInspector = new PropertiesInspector(this, &objects, mainWindow);
    mainWindow->addDockWidget(Qt::RightDockWidgetArea,
                              propertiesInspector->dockWidget());
    mainWindow->addDockWidget(Qt::BottomDockWidgetArea,
                              commandWindow->dockWidget());
    // Create the design tool
    mainDesignTool =
            new MainDesignTool(this);
    designTool = mainDesignTool;
    // Emit the start signal
    designTool->start();
    // Add the commands
    addCommands();
    // Create the toolbars
    propertiesToolBar = new PropertiesToolBar(&objects);
    /*drawToolBar = new QToolBar(tr("Draw"), mainWin);
    drawToolBar->setIconSize(QSize(32, 32));
    mainWin->addToolBar(Qt::LeftToolBarArea, drawToolBar);

    modifyToolBar = new QToolBar(tr("Modify"), mainWin);
    modifyToolBar->setIconSize(QSize(32, 32));
    mainWin->addToolBar(Qt::LeftToolBarArea, modifyToolBar);*/
    createToolbars();
    connect(whatsThisAct, SIGNAL(triggered()), SLOT(enterWhatsThis()));
    // loadPlugins();
    fileModified = false;
    newDoc();
}

void DrawingView::renderObjects()
{
    // TODO : Handle layers
}

void DrawingView::paintGL()
{
    qglClearColor(settings.backgroundColor());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render the scene here

    resizeGL(width(), height());
    if (settings.showGrid())
    {
        drawGrid(1000, 1,
                 settings.baseGridColor(),
                 settings.foreGridColor(),
                 settings.xAxisColor(),
                 settings.yAxisColor(),
                 QColor(20, 50, 200),
                 settings.showAxes(), false);
    }

    // Render the objects
    renderObjects();
    // Render any Design Tool graphics
    designTool->paintGL();
}

void DrawingView::paintOverlay(QPainter &painter)
{
    painter.setPen(QColor(255,255, 255));
    QString fileLabel = fileName;
    if (fileModified) fileLabel.append(" (Modified)");
    // Check if the crosshair is above the text rectangle
    fileLabelRect.setLeft(10);
    fileLabelRect.setTop(10);
    fileLabelRect.setWidth(painter.fontMetrics().width(fileLabel));
    fileLabelRect.setHeight(painter.fontMetrics().height());
    if ((crosshair.x() > fileLabelRect.x()) &&
            (crosshair.y() > fileLabelRect.y()) &&
            (crosshair.x() < fileLabelRect.x() + fileLabelRect.width()) &&
            (crosshair.y() < fileLabelRect.y() + fileLabelRect.height()))
    {
        // Paint a light gray rectangle to highlight the label
        painter.fillRect(
                    fileLabelRect.x() - 2,
                    fileLabelRect.y() - 2,
                    fileLabelRect.x() + fileLabelRect.width(),
                    fileLabelRect.y() + fileLabelRect.height(),
                    QColor(150, 150, 150, 150));
    }
    painter.drawText(fileLabelRect, 0, fileLabel);
    painter.setPen(QColor(255, 255, 255, 200));
    int posCaptionWidth = painter.fontMetrics().width(posCaption);
    int posCaptionHeight = painter.fontMetrics().height();
    painter.drawText(this->width() - posCaptionWidth - 2,
                     this->height() - posCaptionHeight, posCaption);
    // Render the coordinate system
    CS.render(*_camera, painter);
    // Paint the design tool
    designTool->paintOverlay(&painter);
    // Finally paint the crosshair
    crosshair.paintAt(painter);
    // Set the window title properly
    mainWin->setWindowTitle((filePath.length() ? tr("...%1[*] - PlanFlow").arg(
                                this->filePath.right(32))
                                      : tr("%1[*] - PlanFlow").arg(fileName)));
}

void DrawingView::resizeEvent(QResizeEvent *)
{
    resizeGL(width(), height());
    // TODO : Improve this
    float aspectRatio = (float)width()/(float)height();
    float viewHeight = camera()->top - camera()->bottom;
    float centerX = (camera()->right + camera()->left)/2;
    float centerY = (camera()->top + camera()->bottom)/2;
    camera()->setViewport2D(centerX - (viewHeight*aspectRatio)/2,
                            centerY + (viewHeight/2),
                            centerX + (viewHeight*aspectRatio)/2,
                            centerY - (viewHeight/2));
    // TODO : Hide the crosshair till a mouse move activates it
}

void DrawingView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        designTool->mousePressEvent(event);
    }
    commandWindow->setFocus();
    repaint();
}

void DrawingView::mouseMoveEvent(QMouseEvent *event)
{
    crosshair.setX(event->x());
    crosshair.setY(event->y());
    float x, y, z;
    camera()->logicalPoint2D(QPoint(crosshair.x(), crosshair.y()), x, y, z);
    if (settings.roundCoordinates())
        posCaption = tr("%1, %2").arg((int)x + (x < 0 ? -1 : 0)).arg((int)y + (y < 0 ? -1 : 0));
    else
        posCaption = tr("%1, %2").arg(x).arg(y);
    statusBar->setPosText(posCaption);
    designTool->mouseMoveEvent(event);
    repaint();
}

void DrawingView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        // Call the design tool
        designTool->mouseReleaseEvent(event);
        repaint();
        // Check if the user clicked the file name label
        QString fileLabel = fileName;
        if (fileModified) fileLabel.append(" (Modified)");
        // Check if the crosshair is above the text rectangle
        if ((crosshair.x() > fileLabelRect.x()) &&
                (crosshair.y() > fileLabelRect.y()) &&
                (crosshair.x() < fileLabelRect.x() + fileLabelRect.width()) &&
                (crosshair.y() < fileLabelRect.y() + fileLabelRect.height()))
        {
            // Show the document properties dialog
            DrawingPropertiesDlg* drawingDlg = new DrawingPropertiesDlg(mainWin,
                                                                        fileName,
                                                                        fileAuthor,
                                                                        fileUnit);
            drawingDlg->setTitle(tr("Drawing Properties"));
            drawingDlg->exec();
            // Set the drawing title
            fileName = drawingDlg->drawingName();
            fileAuthor = drawingDlg->authorName();
            fileUnit = drawingDlg->unit();
        }
    }
    else if(event->button() == Qt::RightButton)
    {
        mainDesignTool->showContextMenu();
        // Show the context menu
        /*QMenu *contextMenu = new QMenu(mainWin);
        QAction *repeatLast, *selected;

        if (mainDesignTool->_currentTool)
        {
            if (mainDesignTool->useMainTool)
                repeatLast = contextMenu->addAction(tr("&Repeat %1").arg(mainDesignTool->_currentTool->mnemonic()));
            // Add the context menu from the current tool
            else
            {
                mainDesignTool->_currentTool->createContextMenu(contextMenu);
                contextMenu->addSeparator();
            }
        }
        else
        {
            repeatLast = contextMenu->addAction(tr("(No recent command)"));
            repeatLast->setEnabled(false);
        }
        // Add the object menu
        Q_FOREACH(DrawingObject* object, objects)
        {
            // Check if it collides with the mouse cursor and it is selected
            if (object->collides(QPoint(crosshair.x(), crosshair.y()), camera()) && object->selected())
            {
                // Add the entries to the context menu
                QMenu* objectMenu = contextMenu->addMenu(object->typeName());
                object->createContextMenu(objectMenu);
            }
        }

        selected = contextMenu->exec(this->mapToGlobal(event->pos()));
        if ((selected == repeatLast) && (mainDesignTool->_currentTool))
        {
            // Repeat the last command
            commandWindow->startCommand(mainDesignTool->_currentTool);
        }
        delete repeatLast;*/
    }
    else if (event->button() == Qt::MiddleButton)
    {
    }
    commandWindow->setFocus();
}

void DrawingView::wheelEvent(QWheelEvent *e)
{
    zoomLevel += e->delta()/8/15;
    float aspectRatio = (float)width()/(float)height();
    float viewHeight = camera()->top - camera()->bottom;
    float centerX = (camera()->right + camera()->left)/2;
    float centerY = (camera()->top + camera()->bottom)/2;
    camera()->setViewport2D(centerX - (viewHeight*aspectRatio)/2,
                            centerY + (viewHeight/2),
                            centerX + (viewHeight*aspectRatio)/2,
                            centerY - (viewHeight/2));
    camera()->zoom(e->delta()/8/15);
    repaint();
}

void DrawingView::keyPressEvent(QKeyEvent *e)
{
    // commandWindow->setFocus();
    designTool->keyPressEvent(e);
    repaint();
}

void DrawingView::keyReleaseEvent(QKeyEvent *e)
{
    // commandWindow->setFocus();
    designTool->keyReleaseEvent(e);
    repaint();
}

void DrawingView::closeEvent(QCloseEvent *e)
{
    if (promptToSave())
        mainWin->close();
    else
        e->ignore();
}

void DrawingView::commandSelected(DesignTool *tool)
{
    // Set the design tool pointer
    designTool = tool;
    // Connect the "stopped" slot and emit the start signal
    connect(tool, SIGNAL(stop()), this, SLOT(commandStopped()));
    tool->start();
}

void DrawingView::commandStopped()
{
    // Restore the main design tool and emit the start signal
    designTool = mainDesignTool;
    designTool->start();
}

void DrawingView::saveDoc()
{
    documentReset();
    // Create the file object
    QFile file(this->filePath);
    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(
                    mainWin, tr("PlanFlow"),
                    tr("Failed to open file \"%1\"\n%2")
                    .arg(file.fileName())
                    .arg(file.errorString()));
        return;
    }
    // Create the stream
    bool binary = settings.saveAsBinary();
    if (binary)
    {
        // Create a binary file
        QDataStream out(&file);
        out.setVersion(QDataStream::Qt_4_8);
        out << quint32(MagicNumber);
        QApplication::setOverrideCursor(Qt::WaitCursor);
        // Write the drawing title
        out << fileName;
        out << fileAuthor;
        out << (int)fileUnit;
        // Write the objects
        Q_FOREACH(DrawingObject* object, objects)
        {
            // Write the object type
            out << object->typeId();
            // Serialize the object
            object->saveToBinary(&out);
        }
        // Add a -1 to show that there are no more objects
        // out << -1;
    }
    else
    {
        // Create a text file
        QTextStream out(&file);
        out << "Signature : " << hex << MagicNumber << endl;
        QApplication::setOverrideCursor(Qt::WaitCursor);
        // Write the drawing title
        out << "Drawing name : " << fileName;
    }
    QApplication::restoreOverrideCursor();
    fileModified = false;
    file.close();
}

void DrawingView::openDoc(QString path)
{
    // Remove all objects
    Q_FOREACH(DrawingObject* object, objects)
    {
        objects.removeAt(objects.indexOf(object));
        delete object;
    }

    documentReset();
    filePath = path;
    QFile file(this->filePath);
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(
                    mainWin, tr("PlanFlow"),
                    tr("Failed to open file \"%1\"\n%2")
                    .arg(file.fileName())
                    .arg(file.errorString()));
        return;
    }
    // Create the stream
    bool binary = settings.saveAsBinary();
    if (binary)
    {
        // Update the status bar
        mainWin->statusBar()->showMessage(tr("Loading drawing..."));
        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_4_8);
        quint32 magicNumber = 0;
        in >> magicNumber;
        if (magicNumber != MagicNumber)
        {
            // The file is not valid
            QMessageBox::warning(
                        mainWin, tr("PlanFlow"),
                        tr("File \"%1\" is not a valid PlanFlow drawing. Invalid signature (%2)")
                        .arg(filePath).arg(magicNumber));
            // Close the file
            file.close();
            return;
        }
        // Read the document title
        in >> this->fileName;
        in >> fileAuthor;
        int unit = 0;
        in >> unit;
        fileUnit = (DistanceUnit)unit;
        while (in.atEnd() == false)
        {
            int type = -1;
            in >> type;
            switch (type)
            {
                case -1:
                    goto _cleanup;
                break;
                case DWG_OBJECT_BASE:
                    // Warn the user about the file
                    if (QMessageBox::question(mainWin, tr("PlanFlow"),
                                             tr("A base object was read from the file. This can mean that the file is corrupt. Continue?"),
                                             tr("Yes"), tr("No"), QString(), 0) == 1) goto _cleanup;
                    break;
                case DWG_OBJECT_LINE:
                {
                    // Add the line
                    DwgLine* line = new DwgLine(1, 1, 1, 5, 6, 4, Qt::white);
                    line->loadFromBinary(&in);
                    objects.insert(objects.count(), line);
                    break;
                }
                case DWG_OBJECT_RECTANGLE:
                {
                    DwgRectangle* rect = new DwgRectangle(0, 0, 1, 1, 0, Qt::white);
                    rect->loadFromBinary(&in);
                    rect->createGeometry();
                    objects.insert(objects.count(), rect);
                    break;
                }
                case DWG_OBJECT_ELLIPSE:
                {
                    DwgEllipse* ellipse = new DwgEllipse(QVector3D(0, 0, 0), 1);
                    ellipse->loadFromBinary(&in);
                    ellipse->createGeometry();
                    objects.insert(objects.count(), ellipse);
                    break;
                }
            }
        }
    }
    else
    {
        // Not implemented
    }
    _cleanup:
    file.close();
    fileModified = false;
}

void DrawingView::newDoc()
{
    if (!promptToSave())
        return;
    documentReset();
    // Delete every object
    Q_FOREACH(DrawingObject* object, objects)
    {
        objects.removeAt(objects.indexOf(object));
        delete object;
    }

    // Show a dialog so that the user can set up his drawing
    if (settings.showDrawingPropertiesAtStartup())
    {
        DrawingPropertiesDlg* drawingDlg = new DrawingPropertiesDlg(mainWin,
                                                                    tr("Drawing"),
                                                                    tr(""),
                                                                    settings.defaultDistanceUnit());
        drawingDlg->setTitle(tr("New Drawing"));
        drawingDlg->exec();
        // Set the drawing title
        fileName = drawingDlg->drawingName();
        fileAuthor = drawingDlg->authorName();
        fileUnit = drawingDlg->unit();
    }
    else
    {
        fileName = tr("Untitled Drawing");
        fileAuthor = tr("(No author specified)");
        fileUnit = Foot; // TODO : Replace this with some default unit option
    }
    filePath = tr("");
    fileModified = false;
}

bool DrawingView::promptToSave()
{
    // This prompts the user to save the drawing if it has been modified
    if (fileModified)
    {
        // Prompt the user to save the document
        int result = QMessageBox::warning(
                    mainWin,
                    tr("PlanFlow"),
                    tr("The drawing \"%1\" has been modified."
                       " Do you want to save your changes?")
                    .arg(fileName),
                    tr("Save"), tr("Discard"), tr("Cancel"), 0, 2);
        switch (result)
        {
        case 0:
            // Save the changes
            if (filePath.length())
                saveDoc();
            else
            {
                // Get a path from the user
                QString path = QFileDialog::getSaveFileName(
                            mainWin,
                            tr("Save Drawing As"),
                            QString(),
                            tr("PlanFlow Drawings(*.dwg-planflow);;All files(*)"),
                            0,
                            0);
                if (path.length())
                {
                    filePath = path;
                    saveDoc();
                }
            }
            return true;
            break;
        case 1:
            // Ignore the changes
            return true;
            break;
        case 2:
            // Cancel
            return false;
            break;
        }

    }
    return true; // We should never get here
}

QString DrawingView::unitAbbreviation() const
{
    switch (fileUnit)
    {
    case Foot:
        return tr("ft.");
    case Meter:
        return tr("m");
    case Inch:
        return tr("''");
    case Decimeter:
        return tr("dm");
    case Centimeter:
        return tr("cm");
    case Millimeter:
        return tr("mm");
    case Micrometer:
        return tr("um");
    case Nanometer:
        return tr("nm");
    case Picometer:
        return tr("pm");
    case Femtometer:
        return tr("fm");
    case Attometer:
        return tr("am");
    case Zeptometer:
        return tr("zm");
    case Yoctometer:
        return tr("ym");
    case Dekameter:
        return tr("dam");
    case Hectometer:
        return tr("hm");
    case Kilometer:
        return tr("km");
    case Megameter:
        return tr("Mm");
    case Gigameter:
        return tr("Gm");
    case Terameter:
        return tr("Tm");
    case Petameter:
        return tr("Pm");
    case Exameter:
        return tr("Em");
    case Zettameter:
        return tr("Zm");
    case Yottameter:
        return tr("Ym");
    case Mile:
        return tr("mile(s)");
    case NauticalMile:
        return tr("Sea Mile(s)");
    case Lightyear:
        return tr("ly.");
    }
    return tr(""); // This should never be executed
}

void DrawingView::loadPlugin(QObject *plugin, QString filename)
{
    DesignToolPlugin* designTool = qobject_cast<DesignToolPlugin*>(plugin);
    if (designTool)
    {
        if (designTool)
        {
            PluginController* controller = new PluginController(this);
            controller->loadPlugin(designTool);
            // Add the plugin
            mainDesignTool->addCommand(controller);
            switch (designTool->type())
            {
            case Draw:
                addToDraw(designTool->action());
                break;
            case Modify:
                addToModify(designTool->action());
                break;
            }
        }
        else
        {
            QMessageBox::warning(mainWin,
                                 tr("PlanFlow"),
                                 tr("Empty plugin loaded"));
        }
    }
}

void DrawingView::loadPlugins()
{
    QDir pluginsDir(qApp->applicationDirPath());
    pluginsDir.cd("modules");

    foreach (QString fileName, pluginsDir.entryList(QDir::Files))
    {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject* plugin = loader.instance();
        if (plugin)
            loadPlugin(plugin, fileName);
        else
        {
            QMessageBox::critical(mainWin,
                                  tr("Loading error"),
                                  tr("Invalid plugin found in plugins folder."));
        }
    }
}

void DrawingView::addToDraw(QAction *action)
{
    drawToolBar->addAction(action);
}

void DrawingView::addToModify(QAction *action)
{
    modifyToolBar->addAction(action);
}
