// Primitive Plugin SDK
// Copyright (C) 2012 Flex Computer - All rights reserved

#ifndef DESIGNTOOL_H
#define DESIGNTOOL_H

#include <QObject>
#include <QWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include "drawingviewplug.h"
#include "drawingobject.h"
#include "crosshairplug.h"

class DrawingView;

typedef QList<DrawingObject*>* ObjectList;

class DesignTool : public QObject
{
private:
    Q_OBJECT
public:
    DesignTool(DrawingView* drawingView)
    {
        // Connect the command window signal to our slot
        // connect(commandWindow, SIGNAL(inputEntered(QString)), this, SLOT(textInputEntered(QString)));
         _drawingView = drawingView;
         _objects = &drawingView->objects;
         _crosshair = &drawingView->crosshair;
         _statusBar = drawingView->statusBar;
         _CS = &drawingView->CS;
         _mainWindow = drawingView->mainWin;
         _view = (QFrameView*)drawingView;
         _commandWindow = drawingView->commandWindow;
         dragging = false;
         snapped = false;
         pickedLastPoint = false;
    }

    virtual void mousePressEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void mouseReleaseEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void mouseMoveEvent(QMouseEvent *e) { Q_UNUSED(e); }
    virtual void keyPressEvent(QKeyEvent* e) { Q_UNUSED(e); }
    virtual void keyReleaseEvent(QKeyEvent* e) { Q_UNUSED(e); }

    virtual void paintGL() { }
    virtual void paintOverlay(QPainter* painter) { Q_UNUSED(painter); }
    virtual bool highlightObjects() { return false; }
    virtual QString mnemonic() const { return _mnemonic; }
    virtual QAction* action() const { return _action; }

    QList<DrawingObject*>* objects() const { return _objects; }
    void setObjectList(QList<DrawingObject*>* list) { _objects = list; }
    void setCommandWindow(CommandWindow* commandWindow)
    { _commandWindow = commandWindow; }
    void setMainWindow(QMainWindow* mainWindow)
    { _mainWindow = mainWindow; }

    virtual void createContextMenu(QMenu* menu) { Q_UNUSED(menu); }

protected:
    QList<DrawingObject*>* _objects;
    Crosshair* _crosshair;
    StatusBar* _statusBar;
    CoordinateSystem* _CS;
    QFrameView* _view;
    CommandWindow* _commandWindow;
    QString _mnemonic;
    QAction* _action;
    QMainWindow* _mainWindow;
    DrawingView* _drawingView;

    // Helper functions for design tools
protected:
#ifdef NO_HELPERS
    // Pick point
    void pickPointMouseReleaseEvent(QMouseEvent* e) { Q_UNUSED(e); }
    void pickPointMouseMoveEvent(QMouseEvent* e) { Q_UNUSED(e); }
    void pickPointPaintOverlay(QPainter* painter) { Q_UNUSED(painter); }
    bool pickPointInputEntered(QString string) { Q_UNUSED(string); }
    void pickPointSetPrompt(QString prompt) { lastPrompt = prompt; _crosshair->setPrompt(prompt); }
    QVector3D pointFromString(QString text, bool& valid) { Q_UNUSED(text); Q_UNUSED(valid);}
#else
    // Pick point
    void pickPointMouseReleaseEvent(QMouseEvent* e);
    void pickPointMouseMoveEvent(QMouseEvent* e);
    void pickPointPaintOverlay(QPainter* painter);
    bool pickPointInputEntered(QString string);
    void pickPointSetPrompt(QString prompt) { lastPrompt = prompt; _crosshair->setPrompt(prompt); }
    QVector3D pointFromString(QString text, bool& valid);
#endif

    // variables
    bool pointPicked;
    QVector3D point;
    QVector3D lastPoint;
    QVector3D pointToPick;
    QVector3D previousPoint;
    SnapPoint* activeSnapPoint;
    QString lastPrompt;
    bool snapped;
    bool pickedLastPoint;

#ifdef NO_HELPERS
    // Select objects
    void selectMousePressEvent(QMouseEvent* e) { Q_UNUSED(e); }
    void selectMouseReleaseEvent(QMouseEvent* e) { Q_UNUSED(e); }
    void selectPaintOverlay(QPainter* painter) { Q_UNUSED(painter); }
    void selectKeyReleaseEvent(QKeyEvent* e) { Q_UNUSED(e); }
    void clearSelection();
#else
    // Select objects
    void selectMousePressEvent(QMouseEvent* e);
    void selectMouseReleaseEvent(QMouseEvent* e);
    void selectPaintOverlay(QPainter* painter);
    void selectKeyReleaseEvent(QKeyEvent* e);
    void clearSelection();
#endif

    // variables
    bool dragging;
    QPoint selectionBoxStart;
    DrawingObject* selectedObject;
    bool singleObjectSelection;
    bool objectSelected;

signals:
    void stop();

public slots:
    virtual void textInputEntered(QString text) { Q_UNUSED(text); }
    virtual void start() { emit stop(); } // Now that tools have the form of commands
                          // we need to know when a tool has started
                          // to clear the previous data
    virtual void triggered() { }
    virtual void commandStartedSlot(DesignTool* tool) { Q_UNUSED(tool); }
};

#endif
