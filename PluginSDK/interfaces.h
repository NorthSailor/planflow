#ifndef INTERFACES_H
#define INTERFACES_H

#include <QtPlugin>
#include <QObject>
#include "drawingobject.h"
#include "designtoolplug.h"

enum DesignToolType
{
    File = 0,
    Edit = 1,
    Draw = 2,
    Modify = 3,
    Help = 4,
    View = 5
};

class DesignToolPlugin
{
public:
    virtual ~DesignToolPlugin() { }
    virtual void setDrawingView(DrawingView* view) { Q_UNUSED(view); }
    virtual void start() { }
    virtual void triggered() { }
    virtual QAction* action() { return new QAction(new QObject()); }
    virtual QString mnemonic() { return "ERRORCOMMAND"; }
    virtual DesignToolType type() { return File; }
    virtual void paintGL(QFrameView* view) { Q_UNUSED(view); }
    virtual void paintOverlay(QPainter* painter) { Q_UNUSED(painter); }
    virtual bool highlightObjects() { }
    virtual void mousePressEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void mouseReleaseEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void mouseMoveEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void keyPressEvent(QKeyEvent* e) { Q_UNUSED(e); }
    virtual void keyReleaseEvent(QKeyEvent* e) { Q_UNUSED(e); }
    virtual void textInputEntered(QString text) { Q_UNUSED(text); }
    virtual void createContextMenu(QMenu* menu) { Q_UNUSED(menu); }
    virtual bool noInteraction() { return false; }
protected:
    QList<DrawingObject*>* _objects;
    Crosshair* _crosshair;
    StatusBar* _statusBar;
    CoordinateSystem* _CS;
    QFrameView* _view;
    CommandWindow* _commandWindow;
    QString _mnemonic;
    QAction* _action;
    QMainWindow* _mainWindow;
    DrawingView* _drawingView;
};

QT_BEGIN_NAMESPACE

Q_DECLARE_INTERFACE(DesignToolPlugin,
                    "com.flex.Primitive.CommandPlugin/1.0")
QT_END_NAMESPACE

#endif // INTERFACES_H
