// Primitive Plugin SDK
// Copyright (C) 2012 Flex Computer - All rights reserved

#ifndef DRAWINGVIEW_H
#define DRAWINGVIEW_H

#include <QWidget>
#include <QGLWidget>
#include <QStatusBar>
#include <QString>
#include <QObject>
#include <QToolBar>
#include <QPainter>
#include <QStack>
#include "qframeviewplug.h"
#include "crosshairplug.h"
#include "drawingobject.h"

class QDockWidget;
class QLineEdit;
class DesignTool;
class MainDesignTool;
class PropertiesToolBar;

// Distance Units
// This should be enough for any
// drawing in the galaxy
enum DistanceUnit
{
    Foot = 0,       // For Everyone
    Inch = 1,       // For Engineers
    Meter = 2,      // For Everyone
    Decimeter = 3,
    Centimeter = 4,
    Millimeter = 5, // For Engineers
    Micrometer = 6,
    Nanometer = 7,
    Picometer = 8,
    Femtometer = 9,
    Attometer = 10,
    Zeptometer = 11,
    Yoctometer = 12, // For nano technologists
    Dekameter = 13,
    Hectometer = 14,
    Kilometer = 15,
    Megameter = 16,
    Gigameter = 17, // For map creators
    Terameter = 18,
    Petameter = 19,
    Exameter = 20,
    Zettameter = 21,
    Yottameter = 22,
    Mile = 23,
    NauticalMile = 24,  // For sea map designers and limenologists
    Lightyear = 25 // For astronomists
};

// Command window declarations

class CommandPrompt;

class CommandWindow : public QObject
{
    Q_OBJECT
public:
    explicit CommandWindow(QWidget* parent) { Q_UNUSED(parent); }

    QDockWidget* dockWidget() const { return _dockWidget; }
    QWidget* parent() const { return _parent; }
    QLineEdit* lineEdit() const { return _lineEdit; }
    QLabel* promptLabel() const { return _promptLabel; }
    QString commandName() const { return _commandName; }
    QString prompt() const { return _prompt; }

    void setCommandName(QString name) { Q_UNUSED(name); }
    void setPrompt(QString prompt) { Q_UNUSED(prompt); }
    void setDefaultCaption() { }
    void parseCommand(QString text) { Q_UNUSED(text); }

    void startCommand(DesignTool* designTool) { emit commandStarted(designTool); }
    void setFocus() { }

protected:
    QDockWidget* _dockWidget;
    QWidget* _parent;
    QLineEdit* _lineEdit;
    QLabel* _promptLabel;
    QString _commandName;
    QString _prompt;
    CommandPrompt* _commandPrompt;

signals:
    void inputEntered(QString text);
    void commandStarted(DesignTool* designTool);

private slots:
    void returnPressed() { }
    void textEntered(QString text) { Q_UNUSED(text); }

};

#include <QMainWindow>
#include <QLabel>

class StatusBar : public QObject
{
    Q_OBJECT
public:
    explicit StatusBar(QMainWindow* mainWindow, QObject *parent = 0)
    { Q_UNUSED(mainWindow); Q_UNUSED(parent); }

    QMainWindow* parent() const { return _parent; }
    QString posText() const { return _posLabel->text(); }

    void setParent(QMainWindow* parent) { if (parent) _parent = parent; }
    void setPosText(QString text) { _posLabel->setText(text); }

protected:
    QMainWindow* _parent;
    QLabel* _posLabel;

};

class CoordinateSystem
{
public:
    CoordinateSystem() { }

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }
    float rotation() const { return _rotation; }

    void setX(float x) { _x = x; }
    void setY(float y) { _y = y; }
    void setZ(float z) { _z = z; }
    void setRotation(float rotation) { _rotation = rotation; }

    void render(QOrthoCamera &camera, QPainter &painter)
    { Q_UNUSED(camera); Q_UNUSED(painter); }

protected:
    float _x;
    float _y;
    float _z;
    float _rotation; // The rotation in degrees
};

class DrawingSnapshot;

class DrawingView : public QFrameView
{
public:
    DrawingView(QMainWindow* mainWindow, QWidget *parent = 0)
    { Q_UNUSED(mainWindow); Q_UNUSED(parent); }
    void resizeEvent(QResizeEvent *) { }

public:
    void closeEvent(QCloseEvent *e) { Q_UNUSED(e); }
    void paintGL() { }
    void paintOverlay(QPainter &painter) { Q_UNUSED(painter); }
    void mousePressEvent(QMouseEvent *event) { Q_UNUSED(event); }
    void mouseMoveEvent(QMouseEvent *event) { Q_UNUSED(event); }
    void mouseReleaseEvent(QMouseEvent *event) { Q_UNUSED(event); }
    void wheelEvent(QWheelEvent *e) { Q_UNUSED(e); }
    void keyPressEvent(QKeyEvent *e) { Q_UNUSED(e); }
    void keyReleaseEvent(QKeyEvent *e) { Q_UNUSED(e); }
    void renderObjects() { }
    void addCommands() { } // Implemented in commandadder.cpp cause the list is too big
    void createToolbars() { }
    void documentReset()
    {

    }

    void documentModified(QString change = QString())
    {
        Q_UNUSED(change);
    }

public: // Unsafe but practical :-)
    QMainWindow* mainWin;
    Crosshair crosshair;
    StatusBar* statusBar;
    CoordinateSystem CS;
    DesignTool* designTool;

    MainDesignTool* mainDesignTool;

    QList<DrawingObject*> objects;

    CommandWindow* commandWindow;
    PropertiesToolBar* propertiesToolBar;

    QString fileName;
    QString fileAuthor;
    DistanceUnit fileUnit;
    QString filePath;
    bool fileModified;

    float zoomLevel;

    void saveDoc() { }
    void openDoc(QString path) { Q_UNUSED(path); }
    void newDoc() { }

    void loadPlugins() { }
    void loadPlugin(QObject* plugin, QString filename)
    { Q_UNUSED(plugin); Q_UNUSED(filename); }

    void addToDraw(QAction* action) { Q_UNUSED(action); }
    void addToModify(QAction* action) { Q_UNUSED(action); }

    void undo()
    {

    }

    void redo() { }

    bool promptToSave() { }

    QRect fileLabelRect;

    QString unitAbbreviation() const { return tr(""); }

    // The Undo stack
    QStack<DrawingSnapshot*> undoStack;
    QToolBar* drawToolBar;
    QToolBar* modifyToolBar;

signals:

public slots:
    void commandSelected(DesignTool* tool) { Q_UNUSED(tool); }
    void commandStopped() { }

};

#endif // DRAWINGVIEW_H
