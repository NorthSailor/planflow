// Primitive Plugin SDK
// Copyright (C) Flex Computer 2012 - All rights reserved

#ifndef _PLUGIN_SDK_H
#define _PLUGIN_SDK_H

#include "crosshairplug.h"
#include "qframeviewplug.h"
#include "drawingviewplug.h"
#include "drawingobject.h"
#include "designtoolplug.h"

#endif 
