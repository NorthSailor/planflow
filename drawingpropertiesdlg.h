#ifndef DRAWINGPROPERTIESDLG_H
#define DRAWINGPROPERTIESDLG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include "settings.h"

class DrawingPropertiesDlg : public QDialog
{
    Q_OBJECT
public:
    explicit DrawingPropertiesDlg(QWidget *parent = 0, QString drawingName = QString(),
                                  QString authorName = QString(),
                                  int drawingUnit = 0);
    
    void setTitle(QString title) { this->setWindowTitle(title); }
    QString drawingName() const { return _drawingName; }
    QString authorName() const { return _authorName; }
    DistanceUnit unit() const { return _unit; }

    void setDrawingName(QString name) { _drawingName = name; }
    void setAuthorName(QString name) { _authorName = name; }
    void setDistanceUnit(DistanceUnit unit) { _unit = unit; }

protected:
    QString _drawingName;
    QString _authorName;
    DistanceUnit _unit;

protected:
    QLabel* nameLabel;
    QLabel* authorLabel;
    QLabel* unitLabel;
    QLineEdit* drawingNameEdit;
    QLineEdit* authorEdit;
    QComboBox* unitBox;
    QPushButton* saveButton;
    QPushButton* defaultsButton;
    QPushButton* cancelButton;

signals:
    
public slots:
    void saveClicked();
    void defaultsClicked();
    void cancelClicked();
    
};

#endif // DRAWINGPROPERTIESDLG_H
