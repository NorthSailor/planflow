#ifndef SNAPTOGRIDTOOL_H
#define SNAPTOGRIDTOOL_H

#include "../designtool.h"
#include <QAction>

class SnapToGridTool : public DesignTool
{
    Q_OBJECT
public:
    SnapToGridTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Snap To Grid"), _mainWindow);
        _action->setWhatsThis(tr("Enables or disables SNAPTOGRID, helping you pick points located on the grid (e.g (2,5))"));
        _action->setStatusTip(_action->whatsThis());
        _action->setCheckable(true);
        _action->setChecked(false);
        connect(_action, SIGNAL(changed()), SLOT(triggered()));
        _mnemonic = tr("SNAPTOGRID");
    }
    
signals:
    
public slots:
    void triggered();
    void start();
};

#endif // SNAPTOGRIDTOOL_H
