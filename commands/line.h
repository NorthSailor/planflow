#ifndef LINE_H
#define LINE_H

#include <QAction>
#include "designtool.h"
#include <QMessageBox>

const int STAGE_PICK_FIRST_POINT=0;
const int STAGE_PICK_SECOND_POINT=1;

class LineTool : public DesignTool
{
    Q_OBJECT
public:
    LineTool(DrawingView* view) :
    DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("&Line"), _mainWindow );
        _action->setStatusTip(tr("Draws one or more lines"));
        _action->setIcon(QIcon(":/Icons/Line.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));

        cancelAction = new QAction(tr("&Cancel"), _mainWindow);
        cancelAction->setStatusTip(tr("Quits the line tool"));
        connect(cancelAction, SIGNAL(triggered()), SLOT(cancel()));

        closeLineAction = new QAction(tr("&Close Line"), _mainWindow);
        connect(closeLineAction, SIGNAL(triggered()), SLOT(closeLine()));

        // Set the mnemonic
        _mnemonic = tr("LINE");
    }
    
    void paintOverlay(QPainter *painter);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void textInputEntered(QString text);
    bool highlightObjects() { return false; }

    virtual void createContextMenu(QMenu* menu)
    {
        cancelAction->setText(lineCreated ? tr("Finish") : tr("Cancel"));
        menu->addAction(cancelAction);
        if (lineCreated)
        {
            menu->addAction(closeLineAction);
        }
    }

protected:
    int stage;

    QVector3D startPoint; // For closing the lines

    void goToSecondStage();
    void addLineSegment();
    void createdLine();

    bool lineCreated;

signals:
    
public slots:
    void triggered();
    void start();
    void cancel();
    void closeLine();

protected:
    // Actions
    QAction* cancelAction;
    QAction* closeLineAction;
};

#endif // LINE_H
