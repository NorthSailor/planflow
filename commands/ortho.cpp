#include "ortho.h"
#include "settings.h"

void OrthoTool::start()
{
    if (settings.orthoEnabled())
        settings.setOrthoEnabled(false);
    else
        settings.setOrthoEnabled(true);
}

void OrthoTool::triggered()
{
    // Don't start the command as this would stop any other command
    // _commandWindow->startCommand(this);
    start();
}
