#include "open.h"
#include "drawingview.h"

void OpenTool::triggered()
{
    // Start the command
    _commandWindow->startCommand(this);
}

void OpenTool::start()
{
    if (!_drawingView->promptToSave())
    {   emit stop(); return;    }
    // Get a path from the user
    QString path = QFileDialog::getOpenFileName(
                _mainWindow,
                tr("Load Drawing"),
                QString(),
                tr("PlanFlow Drawings(*.dwg-planflow);;All files(*)"),
                0, 0);
    if (path.length())
    {
        _drawingView->openDoc(path);
    }
    // End the command
    emit stop();
    return;
}

