#include "ucs.h"
#include "drawingview.h"

void CSTool::start()
{
    _commandWindow->setCommandName(tr("Move UCS"));
    _commandWindow->setPrompt(tr("Specify a new UCS origin or [Cancel]"));
    pickPointSetPrompt(tr("Specify a new UCS origin"));
    _crosshair->setShowPrompt(true);
    _crosshair->setState(Crosshair::PickPoint);
    // Set the last point so the user can use ortho, polar and relative coordinates
    lastPoint.setX(_drawingView->CS.x());
    lastPoint.setY(_drawingView->CS.y());
    lastPoint.setZ(_drawingView->CS.z());
}

void CSTool::triggered()
{
    _commandWindow->startCommand(this);
}

void CSTool::finishCommand()
{
    emit stop();
    _crosshair->setShowPrompt(false);
}

void CSTool::mouseMoveEvent(QMouseEvent *e)
{
    pickPointMouseMoveEvent(e);
}

void CSTool::mouseReleaseEvent(QMouseEvent *e)
{
    pickPointMouseReleaseEvent(e);
    _drawingView->CS.setX(point.x());
    _drawingView->CS.setY(point.y());
    _drawingView->CS.setZ(point.z());
    finishCommand();
}

void CSTool::paintOverlay(QPainter *painter)
{
    pickPointPaintOverlay(painter);
}

void CSTool::keyReleaseEvent(QKeyEvent *e)
{
    switch (e->key())
    {
    case Qt::Key_Escape:
    case Qt::Key_Cancel:
    case Qt::Key_Return:
        finishCommand();
    }
}

void CSTool::textInputEntered(QString text)
{
    if (pickPointInputEntered(text))
    {
        // There was a point inserted, move the CS to it and finish the command
        // We subtrack the previous CS position, so that the coordinates will be absolute
        _drawingView->CS.setX(point.x() - _drawingView->CS.x());
        _drawingView->CS.setY(point.y() - _drawingView->CS.y());
        _drawingView->CS.setZ(point.z() - _drawingView->CS.z());
        finishCommand();
    }
}
