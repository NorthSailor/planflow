#include "about.h"
#include <QMessageBox>

void AboutTool::start()
{
    QMessageBox::about(
                _mainWindow,
                tr("About PlanFlow"),
                tr("<h2>PlanFlow 0.1</h2>"
                   "<p>Copyright &copy; 2012 Flex Computer - "
                   "<i>All rights reserved.</i></p>"
                   "<p>PlanFlow is a CAD system developed and maintained by <br>"
                   "Flex Computer, a software development foundation.<br>"
                   "Based on:<br>"
                   "<b>Qt 4.8</b><br>"
                   "<b>OpenGL</b><br>"
                   "<b>Flex QFrameView</b>"));
    emit stop();
}

void AboutTool::triggered()
{
    _commandWindow->startCommand(this);
}
