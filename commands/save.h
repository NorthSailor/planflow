#ifndef SAVE_H
#define SAVE_H

#include "../designtool.h"
#include <QAction>

class SaveTool : public DesignTool
{
    Q_OBJECT
public:
    SaveTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(
                    tr("&Save"), _mainWindow);
        _action->setStatusTip(tr("Saves the drawing"));
        _action->setShortcut(QKeySequence::Save);
        _action->setIcon(QIcon(":/Icons/Save.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("SAVE");
        // Add additional construction code here
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // SAVE_H
