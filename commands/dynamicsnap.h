#ifndef DYNAMICSNAP_H
#define DYNAMICSNAP_H

#include "../designtool.h"
#include <QAction>
#include "../settings.h"

class DynamicSnapTool : public DesignTool
{
    Q_OBJECT
public:
    DynamicSnapTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Dynamic Snap"), _mainWindow);
        _action->setWhatsThis(tr("Enables or disables, Dyamic Snap, helping you to pick points aligned with existing snap points."));
        _action->setStatusTip(_action->whatsThis());
        _action->setCheckable(true);
        _action->setChecked(settings.dynamicSnap());
        connect(_action, SIGNAL(changed()), this, SLOT(triggered()));
        _mnemonic = tr("DYNSNAP");
    }

signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // DYNAMICSNAP_H
