#ifndef DWGPROPS_H
#define DWGPROPS_H

#include <QAction>
#include "designtool.h"

class DwgPropsTool : public DesignTool
{
    Q_OBJECT
public:
    DwgPropsTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(
                    tr("Drawing Properties"), _mainWindow);
        _action->setStatusTip(tr("Shows the Drawing Properties dialog allowing you to"
                                 " modify the drawing properties"));
        _action->setIcon(QIcon(":/Icons/Properties.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("DWGPROPS");
        // Add additional construction code here
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // DWGPROPS_H
