#include "snaptogrid.h"
#include "settings.h"

void SnapToGridTool::start()
{
    settings.setSnapToGrid(!settings.snapToGrid());
    emit stop();
}

void SnapToGridTool::triggered()
{
    start();
    // _commandWindow->startCommand(this);
}
