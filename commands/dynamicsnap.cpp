#include "dynamicsnap.h"

void DynamicSnapTool::start()
{
    settings.setDynamicSnap(!settings.dynamicSnap());
    emit stop();
}

void DynamicSnapTool::triggered()
{
    this->start();
}
