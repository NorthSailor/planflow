#ifndef RECT_H
#define RECT_H

#include "../designtool.h"
#include <QAction>

#define RECT_STAGE_PICK_FIRST_POINT 0x1
#define RECT_STAGE_PICK_SECOND_POINT 0x2

class RectTool : public DesignTool
{
    Q_OBJECT
public:
    RectTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(QIcon(tr(":/Icons/Rectangle.png")), tr("&Rectangle"), _mainWindow);
        _action->setWhatsThis(tr("Draws one or more rectangles"));
        _action->setStatusTip(_action->whatsThis());
        connect(_action, SIGNAL(triggered()), SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("RECT");
    }

    bool highlightObjects() { return false; }

    void paintOverlay(QPainter *painter);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void textInputEntered(QString text);
    
signals:

protected:
    QVector3D firstPoint;
    bool drawnRectangle;
    int stage;
    
public slots:
    void triggered();
    void start();

    void moveToPickFirstPoint();
    void moveToPickSecondPoint();
    void addRectangle();
};

#endif // RECT_H
