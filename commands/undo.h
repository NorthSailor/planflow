#ifndef UNDO_H
#define UNDO_H

#include "../designtool.h"
#include <QAction>

class UndoTool : public DesignTool
{
    Q_OBJECT
public:
    UndoTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(
                    tr("&Undo"), _mainWindow);
        _action->setStatusTip(tr("Undoes the last action"));
        _action->setShortcut(QKeySequence::Undo);
        _action->setIcon(QIcon(":/Icons/Undo.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("UNDO");
        // Add additional construction code here
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // UNDO_H
