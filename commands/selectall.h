#ifndef SELECTALL_H
#define SELECTALL_H

#include <QAction>
#include "../designtool.h"

class SelectAllTool : public DesignTool
{
    Q_OBJECT
public:
    SelectAllTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Select All"), _mainWindow);
        _action->setWhatsThis(tr("Selects every object in the drawing"));
        _action->setStatusTip(_action->whatsThis());
        _action->setShortcut(QKeySequence::SelectAll);
        connect(_action, SIGNAL(triggered()), SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("SELECTALL");
    }
    
signals:
    
public slots:
    void triggered();
    void start();
    
};

#endif // SELECTALL_H
