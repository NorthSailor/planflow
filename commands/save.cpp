#include "save.h"
#include "drawingview.h"

void SaveTool::start()
{
    // Check if there is a path
    // If there is just call save
    if (_drawingView->filePath.length())
    {
        _drawingView->saveDoc();
    }
    // Otherwise start the Save As command
    else
    {
        // Get a path from the user
        QString path = QFileDialog::getSaveFileName(
                    _mainWindow,
                    tr("Save Drawing As"),
                    QString(),
                    tr("PlanFlow Drawings(*.dwg-planflow);;All files(*)"),
                    0,
                    0);
        if (path.length())
        {
            _drawingView->filePath = path;
            _drawingView->saveDoc();
        }
    }
    emit stop();
}

void SaveTool::triggered()
{
    _commandWindow->startCommand(this);
}
