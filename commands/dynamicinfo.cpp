#include "dynamicinfo.h"

void DynamicInfoTool::start()
{
    settings.setDynamicInfo(!settings.dynamicInfo());
    _crosshair->setShowPrompt(true);
    _crosshair->setPrompt(tr("Dynamic info has been %1").arg((settings.dynamicInfo() ? tr("enabled.") : tr("disabled."))));
    emit stop();
}

void DynamicInfoTool::triggered()
{
    // start();
    settings.setDynamicInfo(!settings.dynamicInfo());
}
