#include "ellipse.h"
#include "settings.h"
#include "drawingview.h"
#include "objects/dwgellipse.h"
#include <qmath.h> // for sqrt() used in EllipseTool::gotoSpecifyRadius()

void EllipseTool::triggered()
{
    // Notify the command window that we're in control now
    _commandWindow->startCommand(this);
}

void EllipseTool::start()
{
    // Start the ellipse tool
    gotoSpecifyFirstPoint();
    _commandWindow->setCommandName(tr("Ellipse"));
}

void EllipseTool::gotoSpecifyFirstPoint()
{
    stage = ELLIPSE_SPECIFY_FIRST_POINT;
    _crosshair->setState(Crosshair::PickPoint);
    pickPointSetPrompt(tr("Specify first point"));
    _commandWindow->setPrompt(tr("Specify first point or [Cancel]"));
}

void EllipseTool::gotoSpecifySecondPoint()
{
    // Set the first point to the point picked
    firstPoint = point;
    lastPoint = point;
    stage = ELLIPSE_SPECIFY_SECOND_POINT;
    _crosshair->setState(Crosshair::PickPoint);
    pickPointSetPrompt(tr("Specify second point"));
    _commandWindow->setPrompt(tr("Specify second point or [Cancel]"));
}

void EllipseTool::gotoSpecifyRadius()
{
    // Set the second point to the point picked
    secondPoint = point;
    stage = ELLIPSE_SPECIFY_RADIUS;
    _crosshair->setState(Crosshair::PickPoint);
    pickPointSetPrompt(tr("Specify radius"));
    _commandWindow->setPrompt(tr("Specify radius or [Cancel]"));
    // TODO : Create the ellipse
    // Find the center
    QVector3D ellipseCenter // The midpoint of the two points
            ((firstPoint.x() + secondPoint.x())/2,
             (firstPoint.y() + secondPoint.y())/2,0);
    // Find the radius = point distance / 2
    float radius = sqrt(
                pow(secondPoint.x() - firstPoint.x(), 2) +
                pow(secondPoint.y() - firstPoint.y(), 2))/2;
    tempEllipse = new DwgEllipse(ellipseCenter, radius);
    // Find the rotation
    float xDiff = secondPoint.x() - firstPoint.x();
    float yDiff = secondPoint.y() - firstPoint.y();
    float radians = atan2(xDiff, yDiff);
    float angle = (-radians) * 180 / PI + 90;
    tempEllipse->rotation.setZ(angle);
    tempEllipse->setColor(_drawingView->propertiesInspector->defaultColor());
    tempEllipse->setLineStyle(_drawingView->propertiesInspector->defaultLineStyle());
    tempEllipse->setLineWidth(_drawingView->propertiesInspector->defaultLineWidth());
    _commandWindow->setPrompt(tr("Center : (%1, %2) - Radius : %3 - Rotation : %4 degrees")
                              .arg(ellipseCenter.x()).arg(ellipseCenter.y())
                              .arg(radius).arg(angle));
    tempEllipse->createGeometry();
}

void EllipseTool::finishCommand()
{
    // Add the ellipse to the drawing
    _objects->append(tempEllipse);
    // Hide the crosshair prompt
    _crosshair->setShowPrompt(false);
    emit stop();
}

void EllipseTool::cancelCommnad()
{
    // If we are at the third stage delete the ellipse
    if (stage == ELLIPSE_SPECIFY_RADIUS)
        delete tempEllipse;
    _crosshair->setShowPrompt(false); // Nothing to display
    emit stop();
}

void EllipseTool::paintGL()
{
    // If we are on the third stage ask the ellipse to paint itself
    if (stage == ELLIPSE_SPECIFY_RADIUS)
        renderObject(tempEllipse);
}

void EllipseTool::paintOverlay(QPainter *painter)
{
    if (stage == ELLIPSE_SPECIFY_FIRST_POINT)
    {
        // Paint the point picking symbols
        pickPointPaintOverlay(painter);
    }
    else if (stage == ELLIPSE_SPECIFY_SECOND_POINT)
    {
        // Same thing here but paint a line to the first point first
        paintPointDistance(painter,
                           firstPoint, pointToPick);
        pickPointPaintOverlay(painter);
    }
    else if (stage == ELLIPSE_SPECIFY_RADIUS)
    {
        // Paint the pick point symbols above
        // the ellipse dimensions
        tempEllipse->paintOverlay(_drawingView, painter);
        pickPointPaintOverlay(painter);
    }
}

void EllipseTool::mouseMoveEvent(QMouseEvent *e)
{
    if (stage == ELLIPSE_SPECIFY_FIRST_POINT)
    {
        pickPointMouseMoveEvent(e);
    }
    else if (stage == ELLIPSE_SPECIFY_SECOND_POINT)
    {
        pickPointMouseMoveEvent(e);
    }
    else if (stage == ELLIPSE_SPECIFY_RADIUS)
    {
        pickPointMouseMoveEvent(e);
        // Update the ellipse
        // Find the new radius
        float radius = 0;
        float uselessVariable = 0;
        pointDistanceFromLine(
                    pointToPick.x(), pointToPick.y(),
                    firstPoint.x(), firstPoint.y(),
                    secondPoint.x(), secondPoint.y(), uselessVariable, radius);
        // Make the Y scaling our new radius
        tempEllipse->scaling.setY(radius);
        // Tell the ellipse to update it's geometry
        tempEllipse->createGeometry();
    }
}

void EllipseTool::mouseReleaseEvent(QMouseEvent *e)
{
    pickPointMouseReleaseEvent(e);
    if (stage == ELLIPSE_SPECIFY_FIRST_POINT)
    {
        gotoSpecifySecondPoint();
    }
    else if (stage == ELLIPSE_SPECIFY_SECOND_POINT)
    {
        gotoSpecifyRadius();
    }
    else if (stage == ELLIPSE_SPECIFY_RADIUS)
    {
        finishCommand();
    }
}

void EllipseTool::keyReleaseEvent(QKeyEvent *e)
{
    // If the key is 'Esc' or 'C' cancel the command
    if (e->key() == Qt::Key_Escape ||
            e->key() == Qt::Key_C)
        cancelCommnad();
    if (stage == ELLIPSE_SPECIFY_FIRST_POINT)
    {

    }
    else if (stage == ELLIPSE_SPECIFY_SECOND_POINT)
    {

    }
    else if (stage == ELLIPSE_SPECIFY_RADIUS)
    {

    }
}

void EllipseTool::textInputEntered(QString text)
{
    if (pickPointInputEntered(text))
    {
        if (stage == ELLIPSE_SPECIFY_FIRST_POINT)
        {
            gotoSpecifySecondPoint();
        }
        else if (stage == ELLIPSE_SPECIFY_SECOND_POINT)
        {
            gotoSpecifyRadius();
        }
        else if (stage == ELLIPSE_SPECIFY_RADIUS)
        {
            // Here we can call mouse moved and then finish command
            mouseMoveEvent(NULL); // Warning : This might cause crashes
            finishCommand();
        }
    }
    else
    {
        if (text.toUpper() == tr("CANCEL"))
            cancelCommnad();
        // Our last hope is that the text is the radius for the ellipse
        if (stage == ELLIPSE_SPECIFY_RADIUS)
        {
            // Test if the text can be converted to a number
            bool valid = false;
            float radius = text.toFloat(&valid);
            if (!valid) return;
            // Make the Y scaling our new radius
            tempEllipse->scaling.setY(radius);
            // Tell the ellipse to update it's geometry
            tempEllipse->createGeometry();
            // Finish the command
            finishCommand();
        }
    }
}
