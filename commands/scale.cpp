#include "scale.h"
#include <drawingview.h>

void ScaleTool::start()
{
    // Check if there are objects selected
    stage = STAGE_SELECT_OBJECTS;

    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected())
        {
            stage = STAGE_PICK_BASE_POINT;
        }
    }

    // Set the command window to the correct prompt
    _commandWindow->setCommandName(tr("Scale"));
    singleObjectSelection = false;
    _crosshair->setState(Crosshair::SelectObject);
    _crosshair->setPrompt(tr("Select objects to scale"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Select objects to scale or [Cancel]"));
    if (stage == STAGE_PICK_BASE_POINT)
        moveToPickBasePoint();
}

void ScaleTool::triggered()
{
    _commandWindow->startCommand(this);
}

void ScaleTool::mouseMoveEvent(QMouseEvent *e)
{
    if (stage == STAGE_SELECT_OBJECTS)
        ;
    else
    {
        pickPointMouseMoveEvent(e);
        if (stage == STAGE_SELECT_SCALING_FACTOR)
        {
            float x, y, z;
            _view->camera()->logicalPoint2D(
                        QPoint(_crosshair->x(), _crosshair->y()), x, y, z);
            QVector3D tempDestPoint(x, y, z);
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                if (object->selected())
                {
                    object->scale(
                                0, 0, 0,
                                tempDestPoint.x()/destPoint.x(), tempDestPoint.y()/destPoint.y(), tempDestPoint.z()/destPoint.z());
                }
            }
            destPoint = tempDestPoint;
            lastPoint = destPoint;
        }
    }
}

void ScaleTool::mousePressEvent(QMouseEvent *e)
{
    if (stage == STAGE_SELECT_OBJECTS)
        selectMousePressEvent(e);
}

void ScaleTool::mouseReleaseEvent(QMouseEvent *e)
{
    if (stage == STAGE_SELECT_OBJECTS)
    {
        selectMouseReleaseEvent(e);
        _crosshair->setState(Crosshair::SelectObject);
    }
    else
    {
        pickPointMouseReleaseEvent(e);
        // The user picked a point, proceed to the next step
        if (stage == STAGE_PICK_BASE_POINT)
        {
            basePoint = point;
            moveToPickScaleStart();
        }
        if (stage == STAGE_PICK_SCALE_START)
        {
            scaleStart = point;
            moveToPickScaleFactor();
        }
        else if (stage == STAGE_SELECT_SCALING_FACTOR)
        {
            destPoint = point;
            finishCommand();
        }
    }
}

void ScaleTool::keyReleaseEvent(QKeyEvent *e)
{
    Q_UNUSED(e);
}

void ScaleTool::textInputEntered(QString text)
{
    Q_UNUSED(text);
}

void ScaleTool::finishCommand()
{
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected())
        {
            object->setSelected(false);
        }
    }
    _crosshair->setShowPrompt(false);
    _drawingView->documentModified();
    emit stop();
    // Mission accomplished
}

void ScaleTool::paintOverlay(QPainter *painter)
{
    if (stage == STAGE_SELECT_OBJECTS)
        selectPaintOverlay(painter);
    else
        pickPointPaintOverlay(painter);
}

void ScaleTool::moveToPickBasePoint()
{
    stage = STAGE_PICK_BASE_POINT;
    singleObjectSelection = false;
    _crosshair->setState(Crosshair::PickPoint);
    _crosshair->setPrompt(tr("Select base point"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Select base point or [Cancel]"));
}

void ScaleTool::moveToPickScaleStart()
{
    stage = STAGE_PICK_SCALE_START;
    singleObjectSelection = false;
    _crosshair->setState(Crosshair::PickPoint);
    _crosshair->setPrompt(tr("Select scale start point"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Select scale start point or [Cancel]"));
    scaleStart = point;
}

void ScaleTool::moveToPickScaleFactor()
{
    stage = STAGE_SELECT_SCALING_FACTOR;
    singleObjectSelection = false;
    _crosshair->setState(Crosshair::PickPoint);
    _crosshair->setPrompt(tr("Select the scale factor"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Select the scale factor or [Cancel]"));
    destPoint = scaleStart;
}
