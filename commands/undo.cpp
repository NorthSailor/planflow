#include "undo.h"
#include "drawingview.h"

void UndoTool::triggered()
{
    _commandWindow->startCommand(this);
}

void UndoTool::start()
{
    // Undo
    _drawingView->undo();
    emit stop();
}
