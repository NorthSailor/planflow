#include "saveas.h"
#include "../drawingview.h"

void SaveAsTool::triggered()
{
    // Start the command
    _commandWindow->startCommand(this);
}

void SaveAsTool::start()
{
    // Get a path from the user
    QString path = QFileDialog::getSaveFileName(
                _mainWindow,
                tr("Save Drawing As"),
                QString(),
                tr("PlanFlow Drawings(*.dwg-planflow);;All files(*)"),
                0,
                0);
    if (path.length())
    {
        _drawingView->filePath = path;
        _drawingView->saveDoc();
    }
    // End the command
    emit stop();
    return;
}
