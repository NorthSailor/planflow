#ifndef ELLIPSE_H
#define ELLIPSE_H

#include "../designtool.h"
#include <QAction>

#define ELLIPSE_SPECIFY_FIRST_POINT 1
#define ELLIPSE_SPECIFY_SECOND_POINT 2
#define ELLIPSE_SPECIFY_RADIUS 3

class DwgEllipse;

class EllipseTool : public DesignTool
{
    Q_OBJECT
public:
    EllipseTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(QIcon(tr(":/Icons/ellipse.png")), tr("&Ellipse"), _mainWindow);
        _action->setWhatsThis(tr("Draws one ellipse"));
        _action->setStatusTip(_action->whatsThis());
        connect(_action, SIGNAL(triggered()), SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("ELLIPSE");
    }

    bool highlightObjects() { return false; }

    void paintGL();
    void paintOverlay(QPainter *painter);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void textInputEntered(QString text);
    
protected:
    QVector3D firstPoint;
    QVector3D secondPoint;
    float radius;
    int stage;
    DwgEllipse* tempEllipse;

    void gotoSpecifyFirstPoint();
    void gotoSpecifySecondPoint();
    void gotoSpecifyRadius();
    void finishCommand();
    void cancelCommnad();

signals:
    
public slots:
    void triggered();
    void start();
    
};

#endif // ELLIPSE_H
