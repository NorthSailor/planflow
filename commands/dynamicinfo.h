#ifndef DYNAMICINFO_H
#define DYNAMICINFO_H

#include "../designtool.h"
#include "../settings.h"
#include <QAction>

class DynamicInfoTool : public DesignTool
{
    Q_OBJECT
public:
    DynamicInfoTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Dynamic Info"), _mainWindow);
        _action->setWhatsThis(tr("Toggles Dynamic Info, displaying miscellaneous information when you are selecting objects or specifying points."));
        _action->setStatusTip(_action->whatsThis());
        _action->setCheckable(true);
        _action->setChecked(settings.dynamicInfo());
        connect(_action, SIGNAL(changed()), this, SLOT(triggered()));
        _mnemonic = tr("DYNAMICINFO");
    }

signals:

public slots:
    void triggered();
    void start();
    
};

#endif // DYNAMICINFO_H
