#include "quit.h"
#include "../drawingview.h"

void QuitTool::start()
{
    _drawingView->closeEvent(new QCloseEvent);
    emit stop();
}

void QuitTool::triggered()
{
    _commandWindow->startCommand(this);
}
