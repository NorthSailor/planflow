#include "rotate.h"
#include <qmath.h>

RotateTool::RotateTool(DrawingView *view)
    : DesignTool(view)
{
    // Create the action
    _action = new QAction(tr("Rotate"), this);
    _action->setStatusTip(tr("Rotates one or more objects"));
    _action->setIcon(QIcon(":/Icons/Rotate.png"));
    connect(_action, SIGNAL(triggered()), SLOT(triggered()));
}

void RotateTool::triggered()
{
    _commandWindow->startCommand(this);
}

void RotateTool::start()
{
    angle = 0.0f;
    initialAngle = 0.0f;
    basePoint = QVector3D(0, 0, 0);
    moveToSelectObjects();
    // Check if there are already selected objects
    bool objectsSelected = false;
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected()) objectsSelected = true;
    }
    if (objectsSelected)
        moveToSpecifyBasePoint();
}

bool RotateTool::highlightObjects()
{
    return stage == ROTATE_SELECT_OBJECTS;
}

void RotateTool::moveToSelectObjects()
{
    stage = ROTATE_SELECT_OBJECTS;
    _crosshair->setState(Crosshair::SelectObject);
    _crosshair->setPrompt(tr("Select objects to rotate"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setCommandName(tr("Rotate"));
    _commandWindow->setPrompt(tr("Select objects to rotate or [Cancel]"));
}

void RotateTool::moveToSpecifyBasePoint()
{
    stage = ROTATE_SPECIFY_BASE_POINT;
    _crosshair->setState(Crosshair::PickPoint);
    pickPointSetPrompt(tr("Specify base point"));
    _commandWindow->setPrompt(tr("Specify base point or [Center/Cancel]"));
}

void RotateTool::moveToSpecifyAngle()
{
    basePoint = point;
    stage = ROTATE_SPECIFY_ANGLE;
    _crosshair->setState(Crosshair::PickPoint);
    pickPointSetPrompt(tr("Specify rotation angle"));
    _commandWindow->setPrompt(tr("Specify rotation angle or [Cancel]"));
}

void RotateTool::mouseMoveEvent(QMouseEvent *e)
{
    if (stage == ROTATE_SELECT_OBJECTS)
        return;
    if (stage == ROTATE_SPECIFY_BASE_POINT)
        pickPointMouseMoveEvent(e);
    if (stage == ROTATE_SPECIFY_ANGLE)
    {
        pickPointMouseMoveEvent(e);
        // Find the new point
        QVector3D anglePoint = pointToPick;
        float xDiff = anglePoint.x() - basePoint.x();
        float yDiff = anglePoint.y() - basePoint.y();
        float radians = atan2(xDiff, yDiff);
        angle = (-radians) * 180 / PI + 90;
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->selected())
            {
                // Translate the object before the rotation
                // find the angle to rotate the object's position
                float angleDiff = (angle + initialAngle) - object->rotation.z();
                // Rotate the center
                QVector3D newPos(object->center().x() - basePoint.x(),
                                 object->center().y() - basePoint.y(),
                                 object->center().z() - basePoint.z());

                float radianValue = angleDiff/180*PI;
                float sinTheta = sin(radianValue);
                float cosTheta = cos(radianValue);

                float newX = newPos.x() * cosTheta - newPos.y() * sinTheta;
                float newY = newPos.x() * sinTheta + newPos.y() * cosTheta;

                newPos.setX(newX + basePoint.x());
                newPos.setY(newY + basePoint.y());
                newPos.setZ(newPos.z() + basePoint.z());

                object->move(object->center().x(), object->center().y(), object->center().z(),
                             newPos.x(), newPos.y(), newPos.z());

                object->rotation.setZ(angle + initialAngle);
                object->createGeometry();
            }
        }
    }
}

void RotateTool::mousePressEvent(QMouseEvent *e)
{
    if (stage == ROTATE_SELECT_OBJECTS)
        selectMousePressEvent(e);
}

void RotateTool::mouseReleaseEvent(QMouseEvent *e)
{
    if (stage == ROTATE_SELECT_OBJECTS)
        selectMouseReleaseEvent(e);
    if (stage == ROTATE_SPECIFY_BASE_POINT)
    {
        pickPointMouseReleaseEvent(e);
        moveToSpecifyAngle();
    }
    else if (stage == ROTATE_SPECIFY_ANGLE)
    {
        pickPointMouseReleaseEvent(e);
        // Find the new point
        QVector3D anglePoint = point;
        float xDiff = anglePoint.x() - basePoint.x();
        float yDiff = anglePoint.y() - basePoint.y();
        float radians = atan2(xDiff, yDiff);
        angle = (-radians) * 180 / PI + 90;
        finishCommand();
    }
}

void RotateTool::keyReleaseEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        // Cancel the command
        cancel();
}

void RotateTool::textInputEntered(QString text)
{
    if (text.toUpper() == tr("C") || text.toUpper() == tr("CANCEL"))
        cancel();
    if (stage == ROTATE_SELECT_OBJECTS)
        // The user pressed enter, objects selected
        moveToSpecifyBasePoint();
    else
    {
        if (pickPointInputEntered(text))
        {
            if (stage == ROTATE_SPECIFY_BASE_POINT)
                moveToSpecifyAngle();
            else if (stage == ROTATE_SPECIFY_ANGLE)
                return; // Finish the command
        }
        else
        {
            // if the user typed a number and we are on the angle mode perform the rotation
            bool valid = false;
            float value = text.toFloat(&valid);
            if (valid)
            {
                angle = value;
                finishCommand();
            }
            _crosshair->setShowPrompt(false);
            emit stop();
        }
    }
}

void RotateTool::cancel()
{
    emit stop();
}

void RotateTool::paintOverlay(QPainter *painter)
{
    if (stage == ROTATE_SELECT_OBJECTS)
    {
       selectPaintOverlay(painter);
    }
    else
    {
        if (stage == ROTATE_SPECIFY_ANGLE)
        {
            // Paint the new angle
            painter->setPen(Qt::white);
            painter->drawLine(_view->camera()->physicalPoint(basePoint.x(), basePoint.y(), basePoint.z()),
                              QPoint(_crosshair->x(), _crosshair->y()));
        }
        pickPointPaintOverlay(painter);
    }
}

void RotateTool::finishCommand()
{
    // Add the angle to every objects rotation
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected())
        {
            object->rotation.setZ(object->rotation.z() + angle);
            object->createGeometry();
        }
    }
}
