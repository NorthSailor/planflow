#include "qframeview.h"
#include <GL/glu.h>
#include "qorthocamera.h"
#include <qmath.h>

GLubyte halftone[] = {
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
    0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55};


QFrameView::QFrameView(QWidget *parent) :
    QGLWidget(parent)
{
    setFormat(QGLFormat(QGL::DoubleBuffer | QGL::DepthBuffer));
    _camera = new QOrthoCamera();
    _camera->setEye(QVector3D(-10, -40, 10));
    _camera->setUp(QVector3D(0, 0, 1));
    _camera->setCenter(QVector3D(0, 0, 0));
    _camera->setEye(QVector3D(0, 0, 1));
    _camera->setUp(QVector3D(0, 1, 0));
    _camera->setCenter(QVector3D(0, 0, 0));
    // _camera->setViewport2D(-10, 5, 10, -5);
    _camera->zoom(-5);
    setLineWidth(1);
}

void QFrameView::initializeGL()
{
    qglClearColor(QColor(33, 40, 48));
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_MULTISAMPLE);

}

void QFrameView::resizeGL(int width, int height)
{
    /*glViewport(0, 0, width, height);

    // Prevent a division by zero
    if (height == 0) height = 1;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    double aspectRatio = (double)width/(double)height;
    // gluOrtho2D(-5.0f, 5.0f, -5.0f, 5.0f);
    // gluPerspective(45.0, aspectRatio, 0.5, 100);
    glOrtho(-5.0, 5.0, -5.0, 5.0, 1.0, 1000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0);*/
    _camera->resizeScene(width, height);
}

void QFrameView::paintGL()
{
    // QPainter painter(this);
    // painter.beginNativePainting();
    // painter.setBackground(QBrush(QColor(33, 40, 48)));
    qglClearColor(QColor(20, 30, 40));
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render the scene here

    resizeGL(width(), height());


    /*drawLine(0, 0, 5, 0, 5, 0, QColor(255, 255, 255));
    drawTriangle(1, 1, 0, 0, 0, 0, 1, 0, 0, QColor(0, 255, 0));

    drawQuad(-1, 1, 1, -1, -1, 1, 1, -1, 0, 1, 1, 0, QColor(255, 0, 0));*/
    drawGrid(100, 1,
             QColor(39, 45, 56),
             QColor(50, 60, 70),
             QColor(200, 50, 20),
             QColor(20, 200, 50),
             QColor(20, 50, 200),
             true, false);

    glEnable(GL_LINE_STIPPLE);

    setLineStipple (0.1, 0x00FF); /* dashed*/
    drawLine(2, 2, 5, 0, 0, 0, QColor(255, 255, 255));
    drawLine(2, 2, 5, 0, 4, 0, QColor(255, 255, 255));
    drawLine(2, 2, 5, 4, 0, 0, QColor(255, 255, 255));
    drawLine(2, 2, 5, 4, 4, 0, QColor(255, 255, 255));
    drawLine(0, 0, 0, 4, 0, 0, QColor(255, 255, 255));
    drawLine(4, 0, 0, 4, 4, 0, QColor(255, 255, 255));
    drawLine(4, 4, 0, 0, 4, 0, QColor(255, 255, 255));
    drawLine(0, 4, 0, 0, 0, 0, QColor(255, 255, 255));

    // Draw a highlighted line
    setLineWidth(4);
    glEnable(GL_LINE_STIPPLE);
    setLineStipple(1, 0xE1E1);
    drawLine(4 , 5, 0, 6, 7, 0, QColor(255, 0, 0));


    glDisable(GL_POLYGON_STIPPLE);
    glEnable(GL_POLYGON_STIPPLE);
    setPolygonStipple(halftone);

    drawTriangle(0, 0, 0, 3, 0, 0, 0, 3, 0, QColor(0, 255, 0));
    drawQuad(-2, -2, 0, 2, -2, 0, 2, 2, 0, -2, 2, 0, QColor(255, 0, 0));
    // painter.endNativePainting();

    // Create the QPainter and render overlays
    // QPainter painter(this);
    //
    // painter.end();

}

void QFrameView::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setViewport(0, 0, width(), height());

    painter.beginNativePainting();

    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    paintGL();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();

    painter.endNativePainting();

    paintOverlay(painter);
}

void QFrameView::paintOverlay(QPainter &painter)
{
    painter.setPen(QColor(255,255, 255));
    painter.drawText(10, 10, painter.fontMetrics().width(tr("2D - Wireframe")), painter.fontMetrics().height(), 0, tr("2D - Wireframe"));
}

void QFrameView::drawPoint(float x, float y, float z, QColor color)
{
    glBegin(GL_POINTS);
        glColor4f((float)color.redF(), (float)color.greenF(), (float)color.blueF(), (float)color.alphaF());
        glVertex3f(x, y, z);
    glEnd();
}

void QFrameView::drawLine(float x1, float y1, float z1, float x2, float y2, float z2, QColor color)
{
    glBegin(GL_LINES);
        glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF());
        glVertex3f(x1, y1, z1);
        glVertex3f(x2, y2, z2);
    glEnd();
}

void QFrameView::drawTriangle(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, QColor color)
{
    glBegin(GL_TRIANGLES);
        glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF());
        glVertex3f(x1, y1, z1);
        glVertex3f(x2, y2, z2);
        glVertex3f(x3, y3, z3);
    glEnd();
}

void QFrameView::drawQuad(float x1, float y1, float z1, float x2, float y2, float z2,
                          float x3, float y3, float z3, float x4, float y4, float z4, QColor color)
{
    glBegin(GL_QUADS);
    glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF());
        glVertex3f(x1, y1, z1);
        glVertex3f(x2, y2, z2);
        glVertex3f(x3, y3, z3);
        glVertex3f(x4, y4, z4);
    glEnd();
}

void QFrameView::drawCircle(float x, float y, float radius, QColor color, int segmentCount)
{
    glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF());
    float theta = 2 * PI / float(segmentCount);
    float tangetial_factor = (float)tan(theta);//calculate the tangential factor

    float radial_factor = (float)cos(theta);//calculate the radial factor

    float _x = radius;//we start at angle = 0

    float _y = 0;

    float vertices[segmentCount*3];
    int index = 0;
    for (int i = 0; i < segmentCount; i++)
    {
        vertices[index++] = _x + x;
        vertices[index++] = _y + y;
        vertices[index++] = 0;
        //calculate the tangential vector
        //remember, the radial vector is (x, y)
        //to get the tangential vector we flip those coordinates and negate one of them

        float tx = -_y;
        float ty = _x;

        //add the tangential vector

        _x += tx * tangetial_factor;
        _y += ty * tangetial_factor;

        //correct using the radial factor

        _x *= radial_factor;
        _y *= radial_factor;
    }

    // Active the vertex array
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    // Draw the circle
    glDrawArrays(GL_POLYGON, 0, segmentCount);

    // Deactivate the vertex array
    glDisableClientState(GL_VERTEX_ARRAY);
}

void QFrameView::drawGrid(float size, float step, QColor gridColor, QColor gridHighColor, QColor xAxisColor, QColor yAxisColor, QColor zAxisColor, bool drawAxes, bool drawFullAxes)
{
    glDisable(GL_LIGHTING);

    glBegin(GL_LINES);
    glColor3f(0.3f, 0.3f, 0.3f);
    if (step)
    {
        for (float i = -size; i <= size; i += step)
        {
            if (!((i == 0) && (drawAxes)))
            {
                if (((int)i % 5) == 0) glColor3f(gridHighColor.redF(), gridHighColor.greenF(), gridHighColor.blueF());
                else
                    glColor3f(gridColor.redF(), gridColor.greenF(), gridColor.blueF());
                glVertex3f(i, -size, 0);    // Lines parallel to the Y-axis
                glVertex3f(i, size, 0);

                glVertex3f(-size, i, 0); // Lines parallel to the X-axis
                glVertex3f(size, i, 0);
            }
            if ((i == 0) && !drawFullAxes)
            {
                glColor3f(gridHighColor.redF(), gridHighColor.greenF(), gridHighColor.blueF());
                // Stop at (0, 0)
                glVertex3f(i, -size, 0);
                glVertex3f(i, 0, 0);

                glVertex3f(-size, i, 0);
                glVertex3f(0, i, 0);
            }
        }
    }

    if (drawAxes)
    {
        // X-Axis
        glColor3f(xAxisColor.redF(), xAxisColor.greenF(), xAxisColor.blueF());
        glVertex3f((drawFullAxes ? -size : 0), 0, 0);
        glVertex3f(size, 0, 0);

        // Y-Axis
        glColor3f(yAxisColor.redF(), yAxisColor.greenF(), yAxisColor.blueF());
        glVertex3f(0, (drawFullAxes ? -size : 0), 0);
        glVertex3f(0, size, 0);

        // Z-Axis
        glColor3f(zAxisColor.redF(), zAxisColor.greenF(), zAxisColor.blueF());
        glVertex3f(0, 0, (drawFullAxes ? -size : 0)); // Z-Axis remains full
        glVertex3f(0, 0, size);
    }

    glEnd();
}

void QFrameView::rotationStart(float angleX, float angleY, float angleZ, float centerX, float centerY, float centerZ)
{
    // Note that OpenGL wants the transformations in reverse order
    glPushMatrix();
    glTranslatef(centerX, centerY, centerZ);
    glRotatef(angleX, 1, 0, 0);
    glRotatef(angleY, 0, 1, 0);
    glRotatef(angleZ, 0, 0, 1);
    glTranslatef(-centerX, -centerY, -centerZ);
}

void QFrameView::rotationEnd()
{
    glPopMatrix();
}

void QFrameView::setLineStyle(LineStyle style)
{
    // Enable line stipple
    glEnable(GL_LINE_STIPPLE);
    // Set the appropriate line style
    switch(style)
    {
    case Continuous:
        glDisable(GL_LINE_STIPPLE);
        break;
    case DashedDense:
        glLineStipple(1, (unsigned short)0xF0F0F0);
        break;
    case Dashed:
        glLineStipple(1, (unsigned short)0xFF00);
        break;
    case Dotted:
        glLineStipple(1, (unsigned short)0x1010);
        break;
    case DashDotDash:
        glLineStipple(1, (unsigned short)0xf010f0);
        break;
    case DotDashDot:
        glLineStipple(1, (unsigned short)0x10f010);
    }
}

void QFrameView::setPolygonMode(PolygonFace face, PolygonMode mode)
{
    unsigned int nFace = GL_FRONT_AND_BACK;
    unsigned int nMode = GL_FILL;
    switch (face)
    {
    case Front:
        nFace = GL_FRONT;
        break;
    case Back:
        nFace = GL_BACK;
        break;
    case All:
        nFace = GL_FRONT_AND_BACK;
        break;
    }
    switch (mode)
    {
    case Point:
        nMode = GL_POINT;
        break;
    case Wireframe:
        nMode = GL_LINE;
        break;
    case Fill:
        nMode = GL_FILL;
        break;
    }
    glPolygonMode(nFace, nMode);
}

void pointDistanceFromLine(float cx, float cy, float ax, float ay ,
                      float bx, float by, float &distanceSegment,
                      float &distanceLine)
{
  float r_numerator = (cx-ax)*(bx-ax) + (cy-ay)*(by-ay);
  float r_denomenator = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
  float r = r_numerator / r_denomenator;
  float px = ax + r*(bx-ax);
  float py = ay + r*(by-ay);
  float s =  ((ay-cy)*(bx-ax)-(ax-cx)*(by-ay) ) / r_denomenator;
  distanceLine = fabs(s)*sqrt(r_denomenator);
  float xx = px;
  float yy = py;
  if ( (r >= 0) && (r <= 1) )
  {
    distanceSegment = distanceLine;
  }
  else
  {
    float dist1 = (cx-ax)*(cx-ax) + (cy-ay)*(cy-ay);
    float dist2 = (cx-bx)*(cx-bx) + (cy-by)*(cy-by);
    if (dist1 < dist2)
    {
      xx = ax;
      yy = ay;
      distanceSegment = sqrt(dist1);
    }
    else
    {
      xx = bx;
      yy = by;
      distanceSegment = sqrt(dist2);
    }
  }
  return;
}
