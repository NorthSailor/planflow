#include "coordinatesystem.h"
#include "settings.h"
#include <qmath.h>

CoordinateSystem::CoordinateSystem()
{
}

void CoordinateSystem::render(QOrthoCamera& camera, QPainter &painter)
{
    // Get the 0, 0 position
    QPoint axisStart = camera.physicalPoint(x(), y(), z());
    if (settings.alignCSIconWithTheCS() == false)
    {
        axisStart.setX(20);
        axisStart.setY(painter.window().height() - 20);
    }
    // Paint the rectangle
    painter.setPen(settings.csColor());
    painter.drawRect(
                axisStart.x() - settings.crossRectSize()/2,
                axisStart.y() - settings.crossRectSize()/2 - 1,
                settings.crossRectSize(),
                settings.crossRectSize());
    // Plot the missing pixel
    painter.drawPoint(axisStart.x() - settings.crossRectSize()/2,
                      axisStart.y() - settings.crossRectSize()/2 - 1);
    QPoint xEnd(axisStart.x() + settings.crosshairLength(), axisStart.y());
    QPoint yEnd(axisStart.x(), axisStart.y() - settings.crosshairLength());
    // Rotate the endpoints
    /*float s1 = sin(_rotation);
    float c1 = cos(_rotation);
    float s2 = sin(_rotation + 90/180*PI);
    float c2 = cos(_rotation + 90/180*PI);
    xEnd.setX(settings.crosshairLength() * c1);
    xEnd.setY(settings.crosshairLength() * s1);
    yEnd.setX(settings.crosshairLength() * c2);
    yEnd.setY(settings.crosshairLength() * s2);

    xEnd.setX(xEnd.x() + axisStart.x());
    xEnd.setY(axisStart.y() - xEnd.y());
    yEnd.setX(yEnd.x() + axisStart.x());
    yEnd.setY(axisStart.y() - yEnd.y());*/
    // Draw the X line
    painter.drawLine(axisStart.x(), axisStart.y(),
                     xEnd.x(), xEnd.y());
    // Draw the Y line
    painter.drawLine(axisStart.x(), axisStart.y(),
                     yEnd.x(), yEnd.y());
    // Draw the X label
    QPoint xStartPoint(xEnd.x() + 5, axisStart.y() - 17);
    painter.drawLine(xStartPoint.x(), xStartPoint.y(),
                     xStartPoint.x() + 15, xStartPoint.y() + 15);
    painter.drawLine(xStartPoint.x() + 15, xStartPoint.y(),
                     xStartPoint.x(), xStartPoint.y() + 15);
    // Draw the Y label
    QPoint yStartPoint(axisStart.x() + 1, yEnd.y() - 19);
    painter.drawLine(yStartPoint.x(), yStartPoint.y(),
                     yStartPoint.x() + 8, yStartPoint.y() + 8);
    painter.drawLine(yStartPoint.x() + 16, yStartPoint.y(),
                     yStartPoint.x() + 8, yStartPoint.y() + 8);
    painter.drawLine(yStartPoint.x() + 8, yStartPoint.y() + 8,
                     yStartPoint.x() + 8, yStartPoint.y() + 16);
}
