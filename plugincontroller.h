#ifndef PLUGINCONTROLLER_H
#define PLUGINCONTROLLER_H

#include "designtool.h"
#include "PluginSDK/interfaces.h"

class PluginController : public DesignTool
{
    Q_OBJECT
public:
    explicit PluginController(DrawingView* view);

    void loadPlugin(DesignToolPlugin* plugin);
    
    virtual void mousePressEvent(QMouseEvent* e);
    virtual void mouseReleaseEvent(QMouseEvent* e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void keyPressEvent(QKeyEvent* e);
    virtual void keyReleaseEvent(QKeyEvent* e);

    virtual void paintGL();
    virtual void paintOverlay(QPainter* painter);
    virtual bool highlightObjects();
    virtual QString mnemonic() const;
    virtual QAction* action() const;

    virtual void createContextMenu(QMenu *menu);

public slots:
    virtual void textInputEntered(QString text);
    virtual void start();
    virtual void triggered();

protected:
    DesignToolPlugin* _plugin;
};

#endif // PLUGINCONTROLLER_H
