#ifndef DRAWINGVIEW_H
#define DRAWINGVIEW_H

#include <QWidget>
#include <QGLWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QStack>
#include <qgl.h>
#include "settings.h"
#include "crosshair.h"
#include "statusbar.h"
#include "qframeview.h"
#include "coordinatesystem.h"
#include "drawingobject.h"
#include "designtool.h"
#include "maindesigntool.h"
#include "commandwindow.h"
#include "commands.h"
#include <QStatusBar>
#include <QWhatsThis>
#include "propertiestoolbar.h"
#include "PluginSDK/interfaces.h"
#define PROPS_INSPECTOR // WARNING : Keep this before < #include "drawingobjects.h" >
#include "propertiesinspector.h"

const quint32 MagicNumber = 0x3DD32F; // A magic number to identify PlanFlow drawings

class QPaintEvent;
class QResizeEvent;

// Undo stack objects
class DrawingSnapshot : public QObject
{
public:
    DrawingSnapshot(int n) { Q_UNUSED(n); }
    QString changeDescription;

    QList<DrawingObject*> objects;
};

class DrawingView : public QFrameView
{
public:
    DrawingView(QMainWindow* mainWindow, QWidget *parent = 0);
    void resizeEvent(QResizeEvent *);

public:
    void closeEvent(QCloseEvent *e);
    void paintGL();
    void paintOverlay(QPainter &painter);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *e);
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void renderObjects();
    void addCommands(); // Implemented in commandadder.cpp cause the list is too big
    void createToolbars();
    void documentReset()
    {
        fileModified = false;
        mainWin->setWindowModified(false);
    }

    void documentModified(QString change = QString())
    {
        fileModified = true;
        mainWin->setWindowModified(true);
        // Create the document snapshot
        undoStack.push(new DrawingSnapshot(3));
        Q_FOREACH(DrawingObject* object, objects)
        {
            // Add the object to the snapshot
            DrawingObject* objectClone = object->cloneObject();
            undoStack.top()->objects.append(objectClone);
        }
        undoStack.top()->changeDescription = change;
    }

    // slots
#include "commandactions.h"

public: // Unsafe but practical :-)
    QMainWindow* mainWin;
    Crosshair crosshair;
    StatusBar* statusBar;
    CoordinateSystem CS;
    DesignTool* designTool;

    MainDesignTool* mainDesignTool;

    QAction* whatsThisAct;

    QList<DrawingObject*> objects;

    CommandWindow* commandWindow;
    PropertiesToolBar* propertiesToolBar;
    PropertiesInspector* propertiesInspector;

    QString fileName;
    QString fileAuthor;
    DistanceUnit fileUnit;
    QString filePath;
    bool fileModified;

    float zoomLevel;

    void saveDoc();
    void openDoc(QString path);
    void newDoc();

    void loadPlugins();
    void loadPlugin(QObject* plugin, QString filename);

    void addToDraw(QAction* action);
    void addToModify(QAction* action);

    void undo()
    {
        if (undoStack.isEmpty()) return;
        // Restore the previous snapshot of the drawing
        Q_FOREACH(DrawingObject* object, objects)
        {
            delete object;
            objects.removeAt(objects.indexOf(object));
        }

        objects.clear(); // Looks insane but it's OK
        objects.append(undoStack.top()->objects);
        mainWin->statusBar()->showMessage(tr("Action \"%1\" successfully undone.").arg(
                                              undoStack.top()->changeDescription));
        // Delete the last snapshot
    }

    void redo() { }

    bool promptToSave();

    QRect fileLabelRect;
    QString posCaption;

    QString unitAbbreviation() const;

    // The Undo stack
    QStack<DrawingSnapshot*> undoStack;
    QToolBar* drawToolBar;
    QToolBar* modifyToolBar;

signals:

public slots:
    void commandSelected(DesignTool* tool);
    void commandStopped();

    void enterWhatsThis()
    {
        QWhatsThis::enterWhatsThisMode();
    }
    
};

#endif // DRAWINGVIEW_H
