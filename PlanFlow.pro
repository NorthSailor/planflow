#-------------------------------------------------
#
# Project created by QtCreator 2012-11-04T20:48:22
#
#-------------------------------------------------

QT       += core gui opengl

LIBS += -lGLU

TARGET = ../Primitive
TEMPLATE = app

SOURCES += \
    crosshair.cpp \
    coordinatesystem.cpp \
    commandwindow.cpp \
    commandadder.cpp \
    statusbar.cpp \
    settings.cpp \
    qorthocamera.cpp \
    qframeview.cpp \
    propertiestoolbar.cpp \
    mainwindow.cpp \
    maindesigntool.cpp \
    main.cpp \
    layer.cpp \
    drawingview.cpp \
    drawingpropertiesdlg.cpp \
    drawingobject.cpp \
    designtool.cpp \
    commands/undo.cpp \
    commands/ucs.cpp \
    commands/snaptogrid.cpp \
    commands/selectall.cpp \
    commands/scale.cpp \
    commands/saveas.cpp \
    commands/save.cpp \
    commands/rect.cpp \
    commands/quit.cpp \
    commands/osnap.cpp \
    commands/ortho.cpp \
    commands/open.cpp \
    commands/new.cpp \
    commands/move.cpp \
    commands/line.cpp \
    commands/grid.cpp \
    commands/erase.cpp \
    commands/dwgprops.cpp \
    commands/about.cpp \
    objects/dwgrectangle.cpp \
    objects/dwgline.cpp \
    plugincontroller.cpp \
    commands/dynamicinfo.cpp \
    commands/rotate.cpp \
    objects/dwgellipse.cpp \
    commands/circle.cpp \
    propertiesinspector.cpp \
    propwidgets/distance.cpp \
    propwidgets/checkproperty.cpp \
    propwidgets/button.cpp \
    propwidgets/list.cpp \
    propwidgets/labelproperty.cpp \
    commands/dynamicsnap.cpp \
    commands/ellipse.cpp

HEADERS  += \
    coordinatesystem.h \
    commandwindow.h \
    commands.h \
    commandactions.h \
    statusbar.h \
    settings.h \
    qorthocamera.h \
    qframeview.h \
    qcamera.h \
    propertiestoolbar.h \
    mainwindow.h \
    maindesigntool.h \
    layer.h \
    drawingview.h \
    drawingpropertiesdlg.h \
    drawingobjects.h \
    drawingobject.h \
    designtool.h \
    crosshair.h \
    commands/undo.h \
    commands/ucs.h \
    commands/snaptogrid.h \
    commands/selectall.h \
    commands/scale.h \
    commands/saveas.h \
    commands/save.h \
    commands/rect.h \
    commands/quit.h \
    commands/osnap.h \
    commands/ortho.h \
    commands/open.h \
    commands/new.h \
    commands/move.h \
    commands/line.h \
    commands/grid.h \
    commands/erase.h \
    commands/dwgprops.h \
    commands/about.h \
    objects/dwgrectangle.h \
    objects/dwgline.h \
    plugincontroller.h \
    commands/dynamicinfo.h \
    commands/rotate.h \
    objects/dwgellipse.h \
    commands/circle.h \
    propertiesinspector.h \
    propwidgets/distance.h \
    propwidgets/checkproperty.h \
    propwidgets/button.h \
    propwidgets/list.h \
    propertymodel.h \
    propwidgets/labelproperty.h \
    commands/dynamicsnap.h \
    commands/ellipse.h

OTHER_FILES += \
    README.md \
    Primitive.pro.user

RESOURCES += \
    Resources.qrc
