#ifndef QFRAMEVIEW_H
#define QFRAMEVIEW_H

#include <QGLWidget>
#include <QVector3D>
#include "qcamera.h"
#include "qorthocamera.h"

#define PI 3.1415926535897 // This should be precise enough

// Projection types
enum ProjectionType
{
    Orthographic2D = 0,
    Orthographic3D = 0,
    Perspective = 0
};

enum LineStyle
{
    Continuous,
    Dashed,
    DashedDense,
    Dotted,
    DashDotDash,
    DotDashDot
};

enum PolygonMode
{
    Point,
    Fill,
    Wireframe
};

enum PolygonFace
{
    Front,
    Back,
    All
};

class QFrameView : public QGLWidget
{
    Q_OBJECT
public:
    explicit QFrameView(QWidget *parent = 0);
    
    virtual void initializeGL();
    virtual void resizeGL(int width, int height);
    virtual void paintEvent(QPaintEvent *);
    virtual void paintGL();
    virtual void paintOverlay(QPainter& painter);

    // QFrame API
    // Drawing functions
    void drawPoint(float x, float y, float z, QColor color);
    void drawLine(float x1, float y1, float z1, float x2, float y2, float z2, QColor color);
    void drawTriangle(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, QColor color);
    void drawQuad(float x1, float y1, float z1,
                  float x2, float y2, float z2,
                  float x3, float y3, float z3,
                  float x4, float y4, float z4,
                  QColor color);
    void drawCircle(float x, float y, float radius, QColor color = Qt::white, int segmentCount = 100);

    void drawGrid(float size, float step, QColor gridColor, QColor gridHighColor,
                  QColor xAxisColor, QColor yAxisColor, QColor zAxisColor,
                  bool drawAxes, bool drawFullAxes);

    void rotationStart(float angleX, float angleY, float angleZ, float centerX, float centerY, float centerZ);
    void rotationEnd();

    // Get/Set functions
    QOrthoCamera* camera() const { return _camera; }

    void setCamera(QOrthoCamera* camera)
    {
        if (_camera) delete _camera;
        _camera = camera;
    }
    void setLineWidth(float width) { glLineWidth(width); }

    void setLineStyle(LineStyle style);

    void setPolygonMode(PolygonFace face, PolygonMode mode);

    void setLineStipple(int factor, unsigned short pattern) { glLineStipple(factor, pattern); }

    void setPolygonStipple(const unsigned char* pattern) { glPolygonStipple(pattern); }

    void setFillPolygons(bool fill = true) { glPolygonMode(GL_FRONT_AND_BACK, (fill ? GL_FILL : GL_LINE)); }

protected:
    QOrthoCamera* _camera;

signals:
    
public slots:
    
};

// Some nice helper functions
void pointDistanceFromLine(float cx, float cy, float ax, float ay ,
                      float bx, float by, float &distanceSegment,
                      float &distanceLine);

#endif // QFRAMEVIEW_H
