#define _NO_EXTERN // Don't define the extern
#include "settings.h"

// The one and only settings object
Settings settings;

Settings::Settings(QObject *parent) :
    QObject(parent),
    // Default settings
    _backgroundColor(QColor(33, 40, 49)),
    // _backgroundColor(QColor(0, 0, 0)),
    // _crosshairColor(QColor(0, 150, 255)),
    _crosshairColor(QColor(255, 255, 255)),
    _fullLengthCrosshair(false),
    _crosshairLength(100),
    _crossRectSize(9),
    _handleRectSize(9),
    _snapRectSize(12),
    _hitTestTolerance(0.1),
    _crosshairPromptBackground(QColor(10, 11, 64, 150)),
    _crosshairPromptForeground(QColor(150, 180, 200)),
    _antialiasing(true), // Antialiasing is not a default for CAD systems
    _dynamicAntialiasing(true), // Dynamic antialiasing is really nice and it's not a performance issue
    _alignCSIconWithTheCS(true), // Some users don't want to bother with UCS's
    _showGrid(true),
    _showAxes(true),
    _gridSpacing(1.0f),
    _showAsDots(true), // I prefer a solid semi-transparent grid but dots are faster
    _baseGridColor(QColor(39, 45, 57)), // Dark gray
    _foreGridColor(QColor(50, 56, 72)),
    _dotGridColor(QColor(90, 110, 150)), // Light gray
    _xAxisColor(QColor(150, 50, 10)),     // Red
    _yAxisColor(QColor(10, 150, 50)),     // Green
    _csColor(QColor(255, 255, 255)),
    _handleColor(QColor(0, 200, 255)),
    _defaultDistanceUnit(Foot), // This should be different for the Metric version
    _roundCoordinates(false),
    _saveAsBinary(true),
    _showDrawingPropertiesOnStartup(false),
    _snapEnabled(true),
    _snapEndpoints(true),
    _snapMidpoints(true),
    _snapCenters(true),
    _snapToGrid(false),
    _dynamicSnap(true),
    _dynamicInfo(true),
    _orthoEnabled(false),
    _dynamicInfoDetailed(false)
{
    // TODO : Load the settings from a file
}
