#ifndef PROPERTYMODEL_H
#define PROPERTYMODEL_H

#include <QObject>
#include <QWidget>
#include <QAction>

// Controls
class QLabel;
class QVBoxLayout;
class QPushButton;
class QToolBox;

enum PropertyType
{
    Distance,
    Vector,
    Angle,
    ComboBox,
    NumberBox,
    TextBox,
    CheckBox
};

class PropertyWidget : public QWidget
{
    Q_OBJECT
public:
    PropertyWidget();

    void setText(QString text) { Q_UNUSED(text); }
    QString text() const { return QString(); }

protected:
    QLabel* _captionLabel;
};

class PropertyGroupWidget : public QWidget
{
    Q_OBJECT
public:
    PropertyGroupWidget();

    void addWidget(PropertyWidget* widget);
    void clearWidgets();
    void endWidgets();

public slots:

protected:

    // Widgets
    QVBoxLayout* _propertiesLayout;
};

#endif // PROPERTYMODEL_H
