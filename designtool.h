#ifndef DESIGNTOOL_H
#define DESIGNTOOL_H

#include <QObject>
#include <QWidget>
#include <QMouseEvent>
#include "drawingobject.h"
#include "qframeview.h"
#include "crosshair.h"
#include "statusbar.h"
#include "coordinatesystem.h"
#include <QPainter>
#include "commandwindow.h"

class DrawingView;

typedef QList<DrawingObject*>* ObjectList;

class DesignTool : public QObject
{
private:
    Q_OBJECT
public:
    DesignTool(DrawingView* drawingView);

    virtual void mousePressEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void mouseReleaseEvent(QMouseEvent* e) { Q_UNUSED(e); }
    virtual void mouseMoveEvent(QMouseEvent *e) { Q_UNUSED(e); }
    virtual void keyPressEvent(QKeyEvent* e) { Q_UNUSED(e); }
    virtual void keyReleaseEvent(QKeyEvent* e) { Q_UNUSED(e); }

    virtual void paintGL() { }
    virtual void paintOverlay(QPainter* painter) { Q_UNUSED(painter); }
    virtual bool highlightObjects() { return false; }
    virtual QString mnemonic() const { return _mnemonic; }
    virtual QAction* action() const { return _action; }

    QList<DrawingObject*>* objects() const { return _objects; }
    void setObjectList(QList<DrawingObject*>* list) { _objects = list; }
    void setCommandWindow(CommandWindow* commandWindow)
    { _commandWindow = commandWindow; }
    void setMainWindow(QMainWindow* mainWindow)
    { _mainWindow = mainWindow; }

    virtual void createContextMenu(QMenu* menu) { Q_UNUSED(menu); }

protected:
    QList<DrawingObject*>* _objects;
    Crosshair* _crosshair;
    StatusBar* _statusBar;
    CoordinateSystem* _CS;
    QFrameView* _view;
    CommandWindow* _commandWindow;
    QString _mnemonic;
    QAction* _action;
    QMainWindow* _mainWindow;
    DrawingView* _drawingView;

    // Helper functions for design tools
protected:
    // Pick point
    void pickPointMouseReleaseEvent(QMouseEvent* e);
    void pickPointMouseMoveEvent(QMouseEvent* e);
    void pickPointPaintOverlay(QPainter* painter);
    bool pickPointInputEntered(QString string);
    void pickPointSetPrompt(QString prompt);
    QVector3D pointFromString(QString text, bool& valid);
    void paintSnapPoint(SnapPoint point, QPainter* painter);
    void paintPointDistance(QPainter *painter, QVector3D point1, QVector3D point2, bool invertDirection = false);

    // variables
    bool pointPicked;
    QVector3D point;
    QVector3D lastPoint;
    QVector3D pointToPick;
    QVector3D previousPoint;
    SnapPoint* activeSnapPoint;
    QString lastPrompt;
    bool snapped;
    bool pickedLastPoint;
    QList<SnapPoint*> dynSnapPoints;

    // Select objects
    void selectMousePressEvent(QMouseEvent* e);
    void selectMouseReleaseEvent(QMouseEvent* e);
    void selectPaintOverlay(QPainter* painter);
    void selectKeyReleaseEvent(QKeyEvent* e);
    void clearSelection();

    // variables
    bool dragging;
    QPoint selectionBoxStart;
    DrawingObject* selectedObject;
    bool singleObjectSelection;
    bool objectSelected;

    void renderObject(DrawingObject* object);

signals:
    void stop();

public slots:
    virtual void textInputEntered(QString text) { Q_UNUSED(text); }
    virtual void start() { emit stop(); } // Now that tools have the form of commands
                          // we need to know when a tool has start()ed
                          // so that we can clear the previous data
    virtual void triggered() { }
    virtual void commandStartedSlot(DesignTool* tool) { Q_UNUSED(tool); }
};

#endif // DESIGNTOOL_H
