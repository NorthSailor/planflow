#include "statusbar.h"
#include <QStatusBar>

StatusBar::StatusBar(QMainWindow *mainWindow, QObject *parent) :
    QObject(parent),
    _parent(mainWindow)
{
    _posLabel = new QLabel(_parent);
    _parent->statusBar()->addWidget(_posLabel);
}
