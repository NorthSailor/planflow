#include "designtool.h"
#include "drawingview.h"
#include <qmath.h>

DesignTool::DesignTool(DrawingView *drawingView)
{
   // Connect the command window signal to our slot
   // connect(commandWindow, SIGNAL(inputEntered(QString)), this, SLOT(textInputEntered(QString)));
    _drawingView = drawingView;
    _objects = &drawingView->objects;
    _crosshair = &drawingView->crosshair;
    _statusBar = drawingView->statusBar;
    _CS = &drawingView->CS;
    _mainWindow = drawingView->mainWin;
    _view = (QFrameView*)drawingView;
    _commandWindow = drawingView->commandWindow;
    dragging = false;
    snapped = false;
    pickedLastPoint = false;
}

// Since this is a base class most functions are implemented in the header

// Only the design tool helpers are implemented here

void DesignTool::clearSelection()
{
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        object->setSelected(false);
    }
}

void DesignTool::selectKeyReleaseEvent(QKeyEvent *e)
{
    // For the Escape key
    if (e->key() == Qt::Key_Escape)
    {
        // If we are not dragging clear the selection
        if (!dragging)
        {
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                object->setSelected(false);
            }
            _drawingView->propertiesToolBar->selectionChanged();
            _drawingView->propertiesInspector->selectionChanged();
        }
        // Otherwise, cancel the drag
        if (dragging)
        {
            dragging = false;
            _crosshair->setState(Crosshair::SelectObject);
        }
    }
}

void DesignTool::selectMousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    if (dragging == false)
    {
        // If we are not above an object, start dragging the selection box
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->collides(QPoint(_crosshair->x(), _crosshair->y()),
                                 _view->camera()))
            {
                // Check if the object is selected
                if (!object->selected())
                {
                    // Select the object
                    object->setSelected();
                    // If we are in single selection mode
                    if (singleObjectSelection)
                    {
                        selectedObject = object;
                        objectSelected = true;
                        break;
                    }
                }
                // If the object is selected check for it's handles
                if (object->selected())
                {
                    // Do nothing for now
                }
            }
        }
    }
    // We are still here so there we no selections, start a selection box
    if ((dragging == false) && (singleObjectSelection == false))
    {
        dragging = true;
        selectionBoxStart.setX(_crosshair->x());
        selectionBoxStart.setY(_crosshair->y());
        _crosshair->setState(Crosshair::Hidden);
    }
}

void DesignTool::selectMouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    if (dragging)
    {
        dragging = false;
        // Determine the type of the selection box
        QRect selectionRect;
        if (_crosshair->x() < selectionBoxStart.x())
        {
            selectionRect.setX(_crosshair->x());
            selectionRect.setWidth(selectionBoxStart.x() - _crosshair->x());
        }
        else
        {
            selectionRect.setX(selectionBoxStart.x());
            selectionRect.setWidth(_crosshair->x() - selectionBoxStart.x());
        }
        if (_crosshair->y() < selectionBoxStart.y())
        {
            selectionRect.setY(_crosshair->y());
            selectionRect.setHeight(selectionBoxStart.y() - _crosshair->y());
        }
        else
        {
            selectionRect.setY(selectionBoxStart.y());
            selectionRect.setHeight(_crosshair->y() - selectionBoxStart.y());
        }
        if (_crosshair->x() > selectionBoxStart.x())
        {
            // Blue box
            // Parse all of the objects and check if they
            // are in the blue box
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                QRect objectRect = object->screenRect();
                if (
                        (objectRect.x() > selectionRect.x()) &&
                        (objectRect.y() > selectionRect.y()) &&
                        ((objectRect.x() + objectRect.width()) <
                         (selectionRect.x() + selectionRect.width())) &&
                        ((objectRect.y() + objectRect.height()) <
                         (selectionRect.y() + selectionRect.height())))
                    object->setSelected();
            }
        }
        else
        {
            // Green box
            // Parse all of the objects and check if they
            // are in or intersect with the green box
            // TODO : Improve this
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                QRect objectRect = object->screenRect();
                if (selectionRect.intersects(objectRect))
                    object->setSelected();
            }
        }
    }
    _drawingView->propertiesToolBar->selectionChanged();
    _drawingView->propertiesInspector->selectionChanged();
}

void DesignTool::selectPaintOverlay(QPainter *painter)
{
    // If we are draggin draw the selection box
    if (dragging)
    {
        QRect selectionRect(
                    selectionBoxStart.x(), selectionBoxStart.y(),
                    (_crosshair->x() - selectionBoxStart.x()),
                    (_crosshair->y() - selectionBoxStart.y()));
        QPen pen(QColor(255, 255, 255));
        if (_crosshair->x() < selectionBoxStart.x())
            pen.setStyle(Qt::DashLine);
        else
            pen.setStyle(Qt::SolidLine);
        painter->setPen(pen);
        painter->fillRect(
                    selectionRect,
                    ((_crosshair->x() > selectionBoxStart.x()) ?
                        QColor(10, 50, 200, 70) :
                        QColor(10, 200, 50, 70))
                    );
        painter->drawRect(selectionRect);
    }
}

// This function is not in the DesignTool class

void splitString(QString string, QString& string1, QString& string2, QChar splitter)
{
    // Find the length of the string
    int length = string.length();
    bool finishedString1 = false;
    for (int i = 0; i < length; i++)
    {
        QChar character = string.at(i);
        if (!finishedString1)
        {
            if (character == splitter)
            {
                finishedString1 = true;
                continue;
            }
            else
            {
                string1.append(character);
            }
        }
        else
            string2.append(character);
    }
}

QVector3D DesignTool::pointFromString(QString text, bool &valid)
{
    QVector3D point(_CS->x(), _CS->y(), _CS->y());
    if (text.startsWith("@"))
    {
        // Add the last point to point
        point.setX(lastPoint.x());
        point.setY(lastPoint.y());
        point.setZ(lastPoint.z());
        text = text.right(text.length() - 1);
    }
    // Remove all spaces
    text.remove(' ');
    if (text.contains(","))
    {
        // Check if text contains "<" or "," or both
        QString xCoord = "";
        QString yCoord = "";
        splitString(text, xCoord, yCoord, ',');
        bool xOk, yOk;
        point.setX(point.x() + xCoord.toFloat(&xOk));
        point.setY(point.y() + yCoord.toFloat(&yOk));
        if (yOk == false)
        {
            // We have a 3D coordinate, split the second string
            QString zCoord, newYCoord;
            splitString(yCoord, newYCoord, zCoord, ',');
            point.setY(point.y() + newYCoord.toFloat(&yOk));
            point.setZ(point.z() + zCoord.toFloat(&xOk));
        }
        valid = true;
    }
    else if (text.contains("<"))
    {
        // We have a polar coordinate
        QString distance, angle;
        splitString(text, distance, angle, '<');
        bool distanceOk, angleOk;
        float r = distance.toFloat(&distanceOk);
        float theta = angle.toFloat(&angleOk)*PI/180;
        point.setX(point.x() + r * cos(theta));
        point.setY(point.y() + r * sin(theta));
        valid = true;
    }
    return point;
}

void DesignTool::pickPointMouseMoveEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    float x, y, z;
    _view->camera()->logicalPoint2D(
                QPoint(_crosshair->x(), _crosshair->y()),
                x, y, z);
    // Parse the object list
    // If the distance from a snap point is less than the picking tolerance
    // Move the crosshair to that snap point
    // Clear the snap points list
    Q_FOREACH(SnapPoint* snapPoint, dynSnapPoints)
    {
        delete snapPoint;
    }

    dynSnapPoints.clear();

    if (settings.snapEnabled())
    {
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            // Get the snap points
            int snapCount = 0;
            SnapPoint* snapPoints = object->snapPoints(snapCount);
            for (int i = 0; i < snapCount; i++)
            {
                // Check if the snap point is enabled based on its type
                if (!settings.snapEndpoints())
                    if (snapPoints[i].type() == SnapPoint::Endpoint)
                        continue;
                if (!settings.snapMidpoints())
                    if (snapPoints[i].type() == SnapPoint::Midpoint)
                        continue;
                if (!settings.snapCenters())
                    if (snapPoints[i].type() == SnapPoint::Center)
                        continue;
                // Find the screen coordinates for the snap point
                QPoint screenPos = _view->camera()->physicalPoint(
                            snapPoints[i].x(), snapPoints[i].y(), snapPoints[i].z());
                // Distance = sqrt(Dx^2 + Dy^2)
                int distance = sqrt(pow(screenPos.x() - _crosshair->x(), 2) +
                         pow(screenPos.y() - _crosshair->y(), 2));
                if ((distance < settings.crossRectSize()))
                {
                    // Move the cursor to the snap point and exit the loop
                    pointToPick.setX(snapPoints[i].x());
                    pointToPick.setY(snapPoints[i].y());
                    pointToPick.setZ(snapPoints[i].z());
                    _crosshair->setX(screenPos.x());
                    _crosshair->setY(screenPos.y());
                    activeSnapPoint = &snapPoints[i];
                    snapped = true;
                    goto _end;
                }
                else
                {
                    snapped = false;
                }
                // Check for a dynamic snap
                if (settings.dynamicSnap())
                {
                    // Check for a horizontal snap
                    if (abs(screenPos.y() - _crosshair->y()) <= settings.crossRectSize())
                    {
                        // Snap to the line
                        y = snapPoints[i].y();
                        // Add the point to the snapPoints list
                        SnapPoint* replica = new SnapPoint;
                        replica->move(QVector3D(snapPoints[i].x(), snapPoints[i].y(), snapPoints[i].z()),
                                      snapPoints[i].type());
                        dynSnapPoints.append(replica);
                    }
                    // Check for a vertical snap
                    if (abs(screenPos.x() - _crosshair->x()) <= settings.crossRectSize())
                    {
                        // Snap to the vertical line
                        x = snapPoints[i].x();
                        // Add the point to the snapPoints list
                        SnapPoint* replica = new SnapPoint;
                        replica->move(QVector3D(snapPoints[i].x(), snapPoints[i].y(), snapPoints[i].z()),
                                      snapPoints[i].type());
                        dynSnapPoints.append(replica);
                    }
                }
            }
        }
    }
    if (settings.orthoEnabled() && pickedLastPoint)
    {
        // Find what axis is the crosshair closest to
        float dx = lastPoint.x() - x;
        float dy = lastPoint.y() - y;
        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;
        if (dx < dy)
        {
            x = lastPoint.x();
        }
        else
        {
            y = lastPoint.y();
        }
        pointToPick.setX(x);
        pointToPick.setY(y);
    }
    if (settings.snapToGrid())
    {
        x = round(x);
        y = round(y);
        z = round(z);
        pointToPick.setX(x);
        pointToPick.setY(y);
        pointToPick.setZ(z);
        QPoint newCrosshairPoint = _view->camera()->physicalPoint
                (pointToPick.x(), pointToPick.y(), pointToPick.z());
        _crosshair->setX(newCrosshairPoint.x());
        _crosshair->setY(newCrosshairPoint.y());
    }

    pointToPick.setX(x);
    pointToPick.setY(y);
    pointToPick.setZ(z);
    _end:
    return;
}

void DesignTool::pickPointSetPrompt(QString prompt)
{
    lastPrompt = prompt;
    _crosshair->setShowPrompt();
    _crosshair->setPrompt(prompt);
}

bool DesignTool::pickPointInputEntered(QString string)
{
    // If string is a point then return true
    // Otherwise return false
    bool valid = false;
    QVector3D pointEntered = pointFromString(string, valid);
    if (valid)
    {
        lastPoint = point;
        // Set the last point to the point tnered
        point = pointEntered;
    }
    return valid;
}

void DesignTool::pickPointMouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    // Get the point position and end the point picking
    this->pointPicked = true;
    point = pointToPick;
}

void DesignTool::paintSnapPoint(SnapPoint point, QPainter* painter)
{
    QPen pen(Qt::green);
    pen.setWidth(2);
    painter->setPen(pen);
    QPoint snapPos = _view->camera()->physicalPoint(
                activeSnapPoint->x(), activeSnapPoint->y(),
                activeSnapPoint->z());
    if (activeSnapPoint->type() == SnapPoint::Endpoint)
    { // Endpoint, draw a rectangle
        painter->drawRect(
                    snapPos.x() - settings.snapRectSize()/2,
                    snapPos.y() - settings.snapRectSize()/2,
                    settings.snapRectSize(),
                    settings.snapRectSize());
    }
    else if (activeSnapPoint->type() == SnapPoint::Midpoint)
    { // Midpoint, draw a triangle
        QPoint top(snapPos.x(), snapPos.y() - settings.snapRectSize()/2);
        QPoint bottomLeft(snapPos.x() - settings.snapRectSize()/2,
                          snapPos.y() + settings.snapRectSize()/2);
        QPoint bottomRight(snapPos.x() + settings.snapRectSize()/2,
                           snapPos.y() + settings.snapRectSize()/2);
        painter->drawLine(top, bottomLeft);
        painter->drawLine(top, bottomRight);
        painter->drawLine(bottomLeft, bottomRight);
    }
    else if (activeSnapPoint->type() == SnapPoint::Center)
    {
        // Center, draw a circle
        painter->drawEllipse(
                    snapPos.x() - settings.snapRectSize()/2,
                    snapPos.y() - settings.snapRectSize()/2,
                    settings.snapRectSize(),
                    settings.snapRectSize());
    }
}

void DesignTool::pickPointPaintOverlay(QPainter *painter)
{
    // Render the dynamic snap points
    Q_FOREACH(SnapPoint* snapPoint, dynSnapPoints)
    {
        // Paint the alignment line
        QPen dynamicInfoPen(settings.crosshairPromptForeground());
        dynamicInfoPen.setStyle(Qt::DotLine);
        painter->setPen(dynamicInfoPen);
        QPoint screenSnapPoint = _view->camera()->physicalPoint(snapPoint->x(), snapPoint->y(), snapPoint->z());
        QPoint screenDestPoint = _view->camera()->physicalPoint(pointToPick.x(), pointToPick.y(), pointToPick.z());
        painter->drawLine(screenSnapPoint, screenDestPoint);
        // Render the snap point
        // paintSnapPoint(*snapPoint, painter);
    }

    if (dynSnapPoints.count())
    {
        // Paint an 'x' to show the point to be picked
        QPen solidPen(settings.crosshairPromptForeground());
        painter->setPen(solidPen);
        QPoint screenDestPoint = _view->camera()->physicalPoint(pointToPick.x(), pointToPick.y(), pointToPick.z());
        painter->drawLine(screenDestPoint.x() - 4, screenDestPoint.y() - 4, screenDestPoint.x() + 4, screenDestPoint.y() + 4);
        painter->drawLine(screenDestPoint.x() - 4, screenDestPoint.y() + 4, screenDestPoint.x() + 4, screenDestPoint.y() - 4);
    }

    // If there is a snap, draw the snap point indicator
    if (snapped)
    {
        paintSnapPoint(*activeSnapPoint, painter);
        if (activeSnapPoint->type() == SnapPoint::Endpoint)
        {
            _crosshair->setPrompt(tr("Endpoint"));
        }
        else if (activeSnapPoint->type() == SnapPoint::Midpoint)
        {
            _crosshair->setPrompt(tr("Midpoint"));
        }
        else if (activeSnapPoint->type() == SnapPoint::Center)
        {
            _crosshair->setPrompt(tr("Center"));
        }
    }
    else
    {
        _crosshair->setPrompt(lastPrompt);
    }
    // Regardless the snap status, draw the dynamic info
    if (settings.dynamicInfo())
    {
        if (pickedLastPoint)
        {
            // Calculate the distance between the two points
            paintPointDistance(painter, lastPoint, pointToPick);

            // Draw the angle indicator
            // QPoint previousScreenPoint = _view->camera()->physicalPoint(previousPoint.x(), previousPoint.y(), previousPoint.z());
            /*QRect arcRect(
                        previousScreenPoint.x(),
                        currentScreenPoint.y(),
                        currentScreenPoint.x() - previousScreenPoint.x(),
                        lastScreenPoint.y() - currentScreenPoint.y());
            // painter->drawRect(arcRect);
            painter->drawArc(arcRect, 0, 16 * 360);*/
        }
    }
}

void DesignTool::paintPointDistance(QPainter* painter, QVector3D point1, QVector3D point2, bool invertDirection)
{
    // Calculate the distance between the two points
    float distance =
            sqrt(
                pow(point2.x() - point1.x(), 2) +
                pow(point2.y() - point1.y(), 2) +
                pow(point2.z() - point1.z(), 2));
    // Find the last point in screen coordinates
    QPoint lastScreenPoint = _view->camera()->physicalPoint(
                point1.x(), point1.y(), point1.z());
    QPoint currentScreenPoint =
            _view->camera()->physicalPoint(point2.x(), point2.y(), point2.z());
    QPen dynamicInfoPen(settings.crosshairPromptForeground());
    dynamicInfoPen.setStyle(Qt::DotLine);
    painter->setPen(dynamicInfoPen);
    int t = 30;
    int d;
    int Dx = currentScreenPoint.x() - lastScreenPoint.x();
    int Dy = currentScreenPoint.y() - lastScreenPoint.y();
    d = sqrt(pow(currentScreenPoint.x() - lastScreenPoint.x(), 2) + pow(currentScreenPoint.y() - lastScreenPoint.y(), 2));
    if (d == 0) return;
    int invertingFactor = (invertDirection ? -1 : 1);
    int dy = (Dx * t)/d * ((currentScreenPoint.x() < lastScreenPoint.x() ? 1 * invertingFactor : -1 * invertingFactor));
    int dx = (Dy * t)/d * ((currentScreenPoint.x() < lastScreenPoint.x() ? 1 * invertingFactor : -1 * invertingFactor));

    QPoint dimStart(lastScreenPoint.x() - dx, lastScreenPoint.y() + dy);
    QPoint dimEnd(dimStart.x() + Dx, dimStart.y() + Dy);
    painter->drawLine(
                dimStart, dimEnd);
    painter->drawLine(
                dimStart, lastScreenPoint);
    painter->drawLine(dimEnd, currentScreenPoint);
    painter->setPen(QColor(255, 255, 255, 200));
    painter->drawText((dimStart.x() + dimEnd.x())/2, (dimStart.y() + dimEnd.y())/2,
                      tr("%1 %2").arg(distance).arg(_drawingView->unitAbbreviation()));
    painter->drawEllipse(dimStart, 2, 2);
    painter->drawEllipse(dimEnd, 2, 2);
}

void DesignTool::renderObject(DrawingObject *object)
{
    // Apply the object transformations
    glPushMatrix();
    glTranslatef(object->center().x(), object->center().y(), object->center().z());

    glRotatef(object->rotation.x(), 1, 0, 0);
    glRotatef(object->rotation.y(), 0, 1, 0);
    glRotatef(object->rotation.z(), 0, 0, 1);
    glScalef(object->scaling.x(), object->scaling.y(), object->scaling.z());
    glTranslatef(-object->center().x(), -object->center().y(),
                 -object->center().z());
    object->render(_view, false);
    glPopMatrix();
}
