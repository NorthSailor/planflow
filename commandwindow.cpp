#include "commandwindow.h"
#include <QLayout>
#include <QMessageBox>

CommandPrompt::CommandPrompt()
{
    // Create the label and the input box
    promptLabel = new QLabel(tr("<b>Command</b>"), this);
    inputBox = new QLineEdit(this);
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->addWidget(promptLabel);
    layout->addWidget(inputBox);
    this->setLayout(layout);

    connect(inputBox, SIGNAL(returnPressed()), SLOT(returnPressed()));
}

void CommandPrompt::setFocus()
{
    inputBox->setFocus();
}

void CommandPrompt::setDefaultCaption()
{
    promptLabel->setText(tr("<b>Command</b>"));
}

void CommandPrompt::setCommandName(QString name)
{
    _commandName = name;
    promptLabel->setText(tr("<b>%1</b> %2").arg(_commandName.toUpper()).arg(_prompt));
}

void CommandPrompt::setPrompt(QString prompt)
{
    _prompt = prompt;
    promptLabel->setText(tr("<b>%1</b> %2").arg(_commandName.toUpper()).arg(_prompt));
}

void CommandPrompt::returnPressed()
{
    // Emit the input entered signal
    emit inputEntered(inputBox->text());
    inputBox->setText(tr(""));
}


CommandWindow::CommandWindow(QWidget *parent) :
    _parent(parent)
{
    _dockWidget = new QDockWidget(tr("Command Window"), _parent);
    _dockWidget->setAllowedAreas(Qt::BottomDockWidgetArea |
                                 Qt::TopDockWidgetArea);
    _commandPrompt = new CommandPrompt();
    _dockWidget->setTitleBarWidget(_commandPrompt);
    connect(_commandPrompt, SIGNAL(inputEntered(QString)), SLOT(textEntered(QString)));
}

void CommandWindow::setFocus()
{
    _commandPrompt->setFocus();
}

void CommandWindow::setCommandName(QString name)
{
    _commandPrompt->setCommandName(name);
}

void CommandWindow::setPrompt(QString prompt)
{
    _commandPrompt->setPrompt(prompt);
}

void CommandWindow::setDefaultCaption()
{
    _commandPrompt->setDefaultCaption();
}

void CommandWindow::returnPressed()
{
}

void CommandWindow::textEntered(QString text)
{
    emit inputEntered(text);
}

void CommandWindow::parseCommand(QString text)
{
    // Check if the string is a command name
    if (text.contains(' ')) return; // Command names can't contain spaces
    if (text.length() == 0) return; // Command names must be at least one
                                    // character long
    // Add additional criteria here
    // Emit the command signal
}
