#ifndef PROPERTIESTOOLBAR_H
#define PROPERTIESTOOLBAR_H

#include <QObject>
#include <QToolBar>
#include <QAction>
#include "drawingobject.h"
#include <QComboBox>

class PropertiesToolBar : public QObject
{
    Q_OBJECT
public:
    explicit PropertiesToolBar(ObjectList objectList, QObject *parent = 0);
    
    QToolBar* toolBar() const { return _toolBar; }
    QColor defaultColor() const { return _defaultColor; }
    LineStyle defaultLineStyle() const { return _defaultLineStyle; }
    int defaultLineWidth() const { return _defaultLineWidth; }

protected:
    QToolBar* _toolBar;
    ObjectList _objectList;
    QComboBox* _colorCombo;
    QComboBox* _lineWidthCombo;
    QComboBox* _lineStyleCombo;

    QString colorText(QColor color);
    QString lineStyleText(LineStyle lineStyle);
    QString lineWidthText(int width);

    QColor _defaultColor;
    LineStyle _defaultLineStyle;
    int _defaultLineWidth;

signals:
    
public slots:
    void selectionChanged();
    void colorChanged(QString newColor);
    void lineWidthChanged(int newWidth);
    void lineStyleChanged(QString newLineStyle);
    
};

#endif // PROPERTIESTOOLBAR_H
