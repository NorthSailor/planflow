#ifndef PROPERTIESINSPECTOR_H
#define PROPERTIESINSPECTOR_H

#include <QObject>
#include <QWidget>
#include <QAction>
#include <QDockWidget>

#include "propertymodel.h"
#include "drawingobject.h"

class DrawingView;

class HoverButton : public QWidget
{
    Q_OBJECT
public:
    HoverButton(QString text, QWidget* parent = 0);

    void setText(QString text);

    QString text() const;

    virtual void enterEvent(QEvent *);
    virtual void leaveEvent(QEvent *);

protected:
    QLabel* _label;

signals:
    void enter();
    void leave();

};

class InspectorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit InspectorWidget(QWidget* parent = 0);

    void addWidget(QString label, QWidget* widget, PropertyType type);
    void clearProperties();
    void createEssentialProperties();
    void setSelectionDescription(QString text);

    QToolBox* toolBox() const { return _toolBox; }

protected:
    QLabel* _selectionDescription;
    HoverButton* _showDims;
    QLabel* _generalPropsLabel;
    QLabel* _customPropsLabel;

    QToolBox* _toolBox;

signals:
    void showDims();
    void hideDims();

public slots:

};

class ListProperty;
class CheckProperty;

class PropertiesInspector : public QObject
{
    Q_OBJECT
public:
    explicit PropertiesInspector(DrawingView* view, ObjectList list, QWidget *parent = 0);
    
    QDockWidget* dockWidget() const { return _dockWidget; }
    InspectorWidget* propsWidget() const { return _widget; }

    // Defaults
    QColor defaultColor() const { return _defaultColor; }
    LineStyle defaultLineStyle() const { return _defaultLineStyle; }
    int defaultLineWidth() const { return _defaultLineWidth; }

    bool showDimensions() const { return _showDimensions; }

protected:
    QDockWidget* _dockWidget;
    InspectorWidget* _widget;

    DrawingView* _view;

    void createProperties();
    void createEssentialProperties();

    // Essential Widgets
    // Line Color combo box
    ListProperty* lineColorList;
    // Line Style combo box
    ListProperty* lineStyleList;
    // Line Width combo box
    ListProperty* lineWidthList;

    QString colorText(QColor color);
    QString lineStyleText(LineStyle lineStyle);
    QString lineWidthText(int width);

    QColor _defaultColor;
    LineStyle _defaultLineStyle;
    int _defaultLineWidth;

    bool _showDimensions;

    ObjectList objectList;
signals:
    
public slots:
    void selectionChanged();
    void colorChanged(QString newColor);
    void lineWidthChanged(int newWidth);
    void lineStyleChanged(QString newLineStyle);

    void showDims();
    void hideDims();
};

#endif // PROPERTIESINSPECTOR_H
