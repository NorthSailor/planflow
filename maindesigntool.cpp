#include "maindesigntool.h"
#include "settings.h"
#include <QMessageBox>
#include <QTranslator>
#include <QString>
#include <QWidget>
#include <QObject>
#include "drawingview.h"

void MainDesignTool::paintOverlay(QPainter *painter)
{
    // Before drawing anything else deal with the "Show Dimensions" button
    if (_drawingView->propertiesInspector->showDimensions())
    {
        bool objectsSelected = false;
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->selected()) objectsSelected = true;
        }
        // If there were no objects selected paint the dimensions for every object
        if (!objectsSelected)
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                object->paintOverlay(_drawingView, painter);
            }
        // Else if there were objects selected paint the dimensions only for the selected objects
        else
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                if (object->selected())
                    object->paintOverlay(_drawingView, painter);
            }
    }
    if (!useMainTool)
    {
        this->_currentTool->paintOverlay(painter);
        return;
    }
    // Render the object handles
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        // If the object is selected paint the handles
        if (object->selected())
        {

            int handleCount = 0;
            Handle* handles = object->handles(handleCount);
            for (int i = 0; i < handleCount; i++)
            {
                QPoint handleCenter = _view->camera()->physicalPoint(
                            handles[i].x(), handles[i].y(), handles[i].z());
                painter->fillRect(
                            handleCenter.x() - settings.handleRectSize()/2,
                            handleCenter.y() - settings.handleRectSize()/2,
                            settings.handleRectSize(),
                            settings.handleRectSize(),
                            settings.handleColor());
            }
        }
    }
    // If we are draggin draw the selection box
    if (dragging)
    {
        selectPaintOverlay(painter);
    }
    // If we are dragging a handle, render the handle as red
    // and a white line from the start point to the crosshair
    if (draggingHandle)
    {
        painter->fillRect(handleDragStart.x() - settings.handleRectSize()/2,
                          handleDragStart.y() - settings.handleRectSize()/2,
                          settings.handleRectSize(), settings.handleRectSize(),
                          QColor(255, 0, 0));
        QPen pen(QColor(255, 255, 255));
        painter->setPen(pen);
        painter->drawLine(
                    handleDragStart,
                    QPoint(_crosshair->x(), _crosshair->y()));
        // Draw the overlay
        handleObject->paintOverlay(_drawingView, painter);
    }
}

void MainDesignTool::paintGL()
{
    bool highlightObjects = (useMainTool ? true : _currentTool->highlightObjects());
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        // Apply the object transformations
        glPushMatrix();
        glTranslatef(object->center().x(), object->center().y(), object->center().z());

        glRotatef(object->rotation.x(), 1, 0, 0);
        glRotatef(object->rotation.y(), 0, 1, 0);
        glRotatef(object->rotation.z(), 0, 0, 1);
        glScalef(object->scaling.x(), object->scaling.y(), object->scaling.z());
        glTranslatef(-object->center().x(), -object->center().y(), -object->center().z());
        object->render(_view, (object->collides(
                           QPoint(_crosshair->x(), _crosshair->y()),
                                  _view->camera())) && highlightObjects);
        glPopMatrix();
    }
    if (!useMainTool)
        _currentTool->paintGL();
}

void MainDesignTool::mouseMoveEvent(QMouseEvent *e)
{
    if (!useMainTool)
    {
        _currentTool->mouseMoveEvent(e);
        return;
    }
    Q_UNUSED(e); // We don't need e as we have the crosshair object
    // If we are dragging a handle, move the handle
    if (draggingHandle)
    {
        float x, y, z;
        _view->camera()->logicalPoint2D(QPoint(_crosshair->x(), _crosshair->y()), x, y, z);
        handleObject->handleMoved(handleId,
                                  x, y, z);
    }
}

void MainDesignTool::mousePressEvent(QMouseEvent *e)
{
    if (!useMainTool)
    {
        _currentTool->mousePressEvent(e);
        return;
    }
    Q_UNUSED(e);
    if (dragging == false && draggingHandle == false)
    {
        // If we are not above an object, start dragging the selection box
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->collides(QPoint(_crosshair->x(), _crosshair->y()),
                                 _view->camera()))
            {
                // Check if the object is selected
                if (!object->selected())
                {
                    // Select the object
                    object->setSelected();
                    _drawingView->propertiesToolBar->selectionChanged();
                }                
            }
            // Check if the crosshair is in a handle
            // Get the handle array
            int handleCount = 0;
            Handle* handles = object->handles(handleCount);
            for (int i = 0; i < handleCount; i++)
            {
                QPoint handlePos = _view->camera()->physicalPoint(
                     handles[i].x(), handles[i].y(), handles[i].z());
                // Check if the crosshair is in the handle
                if (((_crosshair->x() > (handlePos.x() -
                     settings.handleRectSize()/2)) &&
                        (_crosshair->y() > (handlePos.y() -
                        settings.handleRectSize()/2))) &&
                        (_crosshair->x() < (handlePos.x() +
                          settings.handleRectSize())) &&
                        (_crosshair->y() < (handlePos.y() +
                                            settings.handleRectSize())) && object->selected())
                {
                    // Start the drag
                    draggingHandle = true;
                    handleDragStart = handlePos;
                    handleObject = object;
                    handleId = i;
                    _crosshair->setState(Crosshair::PickPoint);
                }
            }
        }
    }
    // We are still here so there we no selections, start a selection box
    if (draggingHandle == false)
    {
        dragging = true;
        selectionBoxStart = e->pos();
        _crosshair->setState(Crosshair::Hidden);
        selectMousePressEvent(e);
    }
}

void MainDesignTool::mouseReleaseEvent(QMouseEvent *e)
{
    if (!useMainTool)
    {
        _currentTool->mouseReleaseEvent(e);
        return;
    }
    Q_UNUSED(e);
    selectMouseReleaseEvent(e);

    if (draggingHandle)
    {
        // Terminate the drag
        draggingHandle = false;
        // Set the modified flag
        _drawingView->documentModified();
    }
    _crosshair->setState(Crosshair::MainSelect);
    _crosshair->setShowPrompt(false);
    _drawingView->propertiesToolBar->selectionChanged();
    _drawingView->propertiesInspector->selectionChanged();
}

void MainDesignTool::showContextMenu()
{
    if (!useMainTool)
    {
        QMenu* menu = new QMenu(_mainWindow);
        _currentTool->createContextMenu(menu);
        menu->exec(_drawingView->mapToGlobal(QPoint(_crosshair->x(), _crosshair->y())));
    }
    else
    {
        QMenu* menu = new QMenu(_mainWindow);
        QAction* repeatLast = new QAction(tr("Repeat %1").arg(_currentTool->mnemonic()), _mainWindow);
        menu->addAction(repeatLast);
        QAction* selectAll = menu->addAction(tr("Select All"));
        menu->addSeparator();
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->selected())
            {
                QMenu* submenu = menu->addMenu(object->typeName());
                object->createContextMenu(submenu);
            }
        }
        QAction* selected = menu->exec(_drawingView->mapToGlobal(QPoint(_crosshair->x(), _crosshair->y())));
        if (selected == repeatLast)
        {
            startCommand(_currentTool);
        }
        else if (selected == selectAll)
        {
            // Select all objects
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                object->setSelected(true);
            }
        }
    }
}

void MainDesignTool::keyPressEvent(QKeyEvent *e)
{
    if (!useMainTool)
    {
        _currentTool->keyPressEvent(e);
        return;
    }
    Q_UNUSED(e);
}

void MainDesignTool::keyReleaseEvent(QKeyEvent *e)
{
    if (!useMainTool)
    {
        _currentTool->keyReleaseEvent(e);
        return;
    }
    // For the Escape key
    if (e->key() == Qt::Key_Escape)
    {
        // If we are not dragging clear the selection
        if (!dragging)
        {
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                object->setSelected(false);
            }
            _drawingView->propertiesToolBar->selectionChanged();
            _drawingView->propertiesInspector->selectionChanged();
        }

        // Otherwise, cancel the drag
        if (dragging)
        {
            dragging = false;
            _crosshair->setState(Crosshair::MainSelect);
        }
        // Also, cancel the handle drag
        if (draggingHandle)
        {
            draggingHandle = false;
            _crosshair->setState(Crosshair::MainSelect);
        }
    }
    // For the delete key, delete the objects
    if (e->key() == Qt::Key_Delete)
    {
        /*
        // If we are dragging ignore the delete key
        if (dragging || draggingHandle)
            return;
        // Otherwise delete the selection
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->selected())
            {
                this->objects()->removeAt(
                            objects()->indexOf(object));
                delete object;
            }
        } */
        // Start the erase command
        textInputEntered(tr("ERASE"));
    }
    if ((e->key() == Qt::Key_Enter) ||
            (e->key() == Qt::Key_Return))
    {
        // Repeat the last command
        startCommand(_currentTool);
    }
}

void MainDesignTool::textInputEntered(QString text)
{
    if (!useMainTool)
    {
        _currentTool->textInputEntered(text);
        return;
    }
    // Check if the string is a command
    if (text.contains(" ")) return; // Command names can't contains spaces and we
                                    // don't have arguments
    // Capitalize the string
    text = text.toUpper();
    // Parse the command list
    Q_FOREACH(DesignTool* tool, _commandList)
    {
        if (tool->mnemonic() == text)
        {
            // Start the command and return
            tool->triggered();
            return;
        }
    }
    // No command was found
    _commandWindow->setCommandName(tr("Command"));
    _commandWindow->setPrompt(tr("Unknown command \"%1\"").arg(text));
}

void MainDesignTool::start()
{
    // Set the command window prompt
    _commandWindow->setCommandName(tr("Command"));
    _commandWindow->setDefaultCaption();
    // Set the crosshair
    _crosshair->setState(Crosshair::MainSelect);
    // _crosshair->setShowPrompt(false);
    // connect the command window signal to our slot
    // connect(_commandWindow, SIGNAL(inputEntered(QString)), this, SLOT(textInputEntered(QString)));
    /* if (useMainTool == false)
        disconnect(_currentTool, SIGNAL(stop())); */
    useMainTool = true;
}
